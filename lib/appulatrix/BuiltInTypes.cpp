#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/Type.h"
#include "appulatrix/TypeManager.h"
#include "appulatrix/TypeBuilder.h"


#define IMPLEMENT_BUILTIN_TYPE_BUILDER(name, type) namespace appulatrix { \
  TypeBuilder TypeFinder<name>::create(TypeManager &manager) { \
    TypeBuilder builder(#name, sizeof(name), type, boost::none, manager);\
    fill_description(builder); \
    return builder; } }\
  void appulatrix::TypeFinder<name>::fill_description(appulatrix::TypeBuilder &) {}

IMPLEMENT_BUILTIN_TYPE_BUILDER(std::uint8_t, appulatrix::Type::DataType::UnsignedInteger)
IMPLEMENT_BUILTIN_TYPE_BUILDER(std::uint16_t, appulatrix::Type::DataType::UnsignedInteger)
IMPLEMENT_BUILTIN_TYPE_BUILDER(std::uint32_t, appulatrix::Type::DataType::UnsignedInteger)
IMPLEMENT_BUILTIN_TYPE_BUILDER(std::uint64_t, appulatrix::Type::DataType::UnsignedInteger)

IMPLEMENT_BUILTIN_TYPE_BUILDER(std::int8_t, appulatrix::Type::DataType::Integer)
IMPLEMENT_BUILTIN_TYPE_BUILDER(std::int16_t, appulatrix::Type::DataType::Integer)
IMPLEMENT_BUILTIN_TYPE_BUILDER(std::int32_t, appulatrix::Type::DataType::Integer)
IMPLEMENT_BUILTIN_TYPE_BUILDER(std::int64_t, appulatrix::Type::DataType::Integer)

IMPLEMENT_BUILTIN_TYPE_BUILDER(bool, appulatrix::Type::DataType::Boolean)
IMPLEMENT_BUILTIN_TYPE_BUILDER(char, appulatrix::Type::DataType::Integer)

IMPLEMENT_BUILTIN_TYPE_BUILDER(float, appulatrix::Type::DataType::Real)
IMPLEMENT_BUILTIN_TYPE_BUILDER(double, appulatrix::Type::DataType::Real)

IMPLEMENT_BUILTIN_TYPE_BUILDER(void *, appulatrix::Type::DataType::UnsignedInteger)

IMPLEMENT_TYPE_BUILDER(appulatrix::String, )
{
}
