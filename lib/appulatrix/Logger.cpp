#include "appulatrix/Logger.h"
#include "appulatrix/detail/LoggerDetails.h"

#include <boost/filesystem/operations.hpp>

#include <iostream>

namespace appulatrix
{

Logger::Logger(
  const boost::filesystem::path &path,
  std::size_t starting_file_size,
  std::size_t max_file_size,
  double growth_factor)
: m_path(path)
, m_logged_event_count_previous_files(0)
, m_min_allocation_size(0)
, m_next_file_size(starting_file_size)
, m_max_file_size(max_file_size)
, m_growth_factor(growth_factor)
{
  std::cout << "Logging data to " << path << std::endl;
  std::lock_guard<std::mutex> l(m_active_file_mutex);
  boost::filesystem::remove_all(path);
  boost::filesystem::create_directories(path);
  m_header = detail::MappedObject<detail::Header>::create(detail::header_file_path(path));
  create_active_file();


  auto event_min = m_active_file->get().m_events.min_allocation_size();
  auto type_min = m_type_manager.min_allocation_size();
  auto stack_min = m_stack_manager.min_allocation_size();
  m_min_allocation_size = std::max(type_min, std::max(event_min, stack_min));
}

void Logger::next_active_file()
{
  std::lock_guard<std::mutex> l(m_active_file_mutex);
  m_header->get().next_active_file();
  create_active_file();
}

void Logger::create_active_file()
{
  auto index = m_header->get().active_file();
  m_old_files.push_back(m_active_file);
  m_active_file = detail::MappedObject<detail::DataFile>::create(
    detail::data_file_path(m_path, index),
    m_next_file_size
  );

  m_stack_manager.set_current_stack_list(m_active_file->get().m_stacks);
  m_type_manager.set_current_type_list(m_active_file->get().m_types);

  m_next_file_size = std::min(
    m_max_file_size,
    std::size_t(m_next_file_size * m_growth_factor)
  );
}


void Logger::verify_file_space()
{
  if (m_active_file->get().m_allocator.available_size() < m_min_allocation_size)
  {
    next_active_file();
  }
}

EventManager &Logger::active_event_manager()
{
  return m_active_file->get().m_events;
}

std::size_t Logger::logged_event_count() const
{
  return m_logged_event_count_previous_files + m_active_file->get().m_events.events().size();
}

}
