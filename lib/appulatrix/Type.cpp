#include "appulatrix/Type.h"
#include "appulatrix/TypeManager.h"
#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/TypeBuilder.h"

#include <boost/variant.hpp>
#include <boost/optional.hpp>

#include <iomanip>

namespace appulatrix
{

const void *Type::Field::get_data_ptr(const void *parent) const
{
  auto parent_ptr = reinterpret_cast<const char *>(parent);
  return parent_ptr + m_offset;
}

void Type::Field::dump(std::ostream &str, TypeLookup &lookup, const void *data, std::size_t indent) const
{   
  str << name().get() << ": ";

  auto type_val = type();
  if (type_val != Type::invalid_index())
  {
    lookup.find_type(type_val).dump(str, lookup, data, false, indent);
  }
  str << "\n";
}

Type::Type(
  Id index,
  const TypeBuilder &builder,
  AllocatableBlock &alloc)
: m_size(builder.size())
, m_field_count(builder.fields().size())
, m_fields(nullptr)
, m_name(builder.name().c_str(), alloc)
, m_data_type(builder.data_type())
, m_id(index)
{
  auto &children_src = builder.fields();
  auto children = static_cast<Field*>(alloc
    .allocate(children_src.size() * sizeof(Field)));
  m_fields = children;
  
  for (std::size_t i = 0; i < children_src.size(); ++i)
  {
    auto &child_src = children_src[i];
    
    new(children + i) Field(
      child_src.name.c_str(),
      child_src.type,
      child_src.offset,
      child_src.parent,
      child_src.flags,
      alloc
    );
  }
}

boost::optional<const Type::Field &> Type::find_field(const char *str) const
{
  for (std::size_t i = 0; i < m_field_count; ++i)
  {
    auto &field = m_fields.get()[i];
    if (field.name() == str)
    {
      return field;
    }
  }

  return boost::none;
}

boost::optional<const Type::Field &> Type::find_field(const std::string &str) const
{
  return find_field(str.data());
}

boost::optional<Type::DataVariant> Type::get_field_value(TypeLookup &lookup, const char *str, const void *data) const
{
  auto field = find_field(str);
  if (!field || field->type() == Type::invalid_index())
  {
    return boost::none;
  }

  auto field_data = field->get_data_ptr(data);
  return lookup.find_type(field->type()).get_value(field_data);
}

boost::optional<Type::DataVariant> Type::get_field_value(TypeLookup &lookup, const std::string &str, const void *data) const
{
  return get_field_value(lookup, str.data(), data);
}

boost::optional<const void *> Type::find_interface(const void *data, TypeId type) const
{
  // Check if this is the correct type
  if (m_id == type)
  {
    return data;
  }

  // Otherwise check the fields
  for (std::size_t i = 0; i < m_field_count; ++i)
  {
    auto &field = m_fields.get()[i];
    if (field.has_flag(Field::Flag::Interface) && field.type() == type)
    {
      return field.get_data_ptr(data);
    }
  }

  return boost::none;
}

boost::optional<Type::DataVariant> Type::get_value(const void *data) const
{
  using Result = boost::optional<Type::DataVariant>;
  auto data_val = reinterpret_cast<const char *>(data);
  if (m_data_type == DataType::UnsignedInteger)
  {
    switch (m_size)
    {
    case 8: return Result(*(std::uint64_t*)data_val);
    case 4: return Result(*(std::uint32_t*)data_val);
    case 2: return Result(*(std::uint16_t*)data_val);
    case 1: return Result(*(std::uint8_t*)data_val);
    default: break;
    }
  }
  else if (m_data_type == DataType::Integer)
  {
    switch (m_size)
    {
    case 8: return Result(*(std::int64_t*)data_val);
    case 4: return Result(*(std::int32_t*)data_val);
    case 2: return Result(*(std::int16_t*)data_val);
    case 1: return Result(*(std::int8_t*)data_val);
    }
  }
  else if (m_data_type == DataType::Real)
  {
    switch (m_size)
    {
    case 8: return Result(*(double*)data_val);
    case 4: return Result(*(float*)data_val);
    }
  }
  else if (m_data_type == DataType::Boolean)
  {
    std::uint8_t value = 0;
    for (std::size_t i = 0; i < m_size; ++i)
    {
      value += data_val[i];
    }

    return Result(value != 0);
  }

  return {};
}

void Type::dump(std::ostream &str, TypeLookup &lookup, const void *data, bool leading_indent, std::size_t indent) const
{
  struct Utils
  {
    static void dump_indent(
      std::ostream &str,
      std::size_t indent)
    {
      for (std::size_t i = 0; i < indent; ++i)
      {
        str << "  ";
      }
    }

    static void dump_fields(
      std::ostream &str,
      TypeLookup &lookup,
      const Type &ths,
      const void *data,
      std::size_t parent,
      std::size_t indent)
    {
      for (std::size_t i = 0; i < ths.m_field_count; ++i)
      {
        auto &field = ths.m_fields.get()[i];
        if (field.parent() == parent)
        {
          auto field_data = field.get_data_ptr(data);

          Utils::dump_indent(str, indent);
          field.dump(str, lookup, field_data, indent);

          Utils::dump_fields(
            str,
            lookup,
            ths,
            data,
            i,
            indent + 1
          );
        }
      }
    };
  };

  if (m_field_count > 0)
  {
    if (leading_indent)
    {
      Utils::dump_indent(str, indent);
    }
    str << m_name.get() << " {\n";

    Utils::dump_fields(
      str,
      lookup,
      *this,
      data,
      Field::null_parent(),
      indent + 1
    );

    Utils::dump_indent(str, indent);
    str << "}\n";
  }
  else
  {
    // Try to get the data as a known type
    auto value = get_value(data);
    if (value)
    {
      str << *value;
    }
    // If we fail, print the byte values
    else
    {
      std::ios::fmtflags f(str.flags());
      auto data_val = reinterpret_cast<const char *>(data);
      for (std::size_t i = 0; i < m_size; ++i)
      {
        str << std::setw(2) << std::hex << std::setfill('0') << (int)data_val[i];
      }
      str.flags(f);
    }
  }
}

}
