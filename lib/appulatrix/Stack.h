#pragma once

#include "appulatrix_export.h"
#include "appulatrix/StackProviders/Utils.h"
#include "appulatrix/types.h"

#include <boost/optional.hpp>

#include <array>
#include <unordered_map>

namespace appulatrix
{
class Stack;
}

namespace std
{
template <> struct hash<appulatrix::Stack>
{
public:
  std::size_t operator()(const appulatrix::Stack &a) const;
};
}

namespace appulatrix
{

class APPULATRIX_EXPORT Stack
{
public:
  using Data = std::array<char, 320>;

  Stack() { }
  static Stack current_stack_thread(std::size_t skip_count);

  bool operator!=(const Stack &oth) const
  {
    return !(*this == oth);
  }
  bool operator==(const Stack &oth) const;

  FormattedStackContainer format() const;
  
  char *data() { return m_data.data(); }
  const char *data() const { return m_data.data(); }

private:
  Data m_data;
};

class APPULATRIX_EXPORT DetailedStack
{
public:
  static DetailedStack current_stack_thread(std::size_t skip_count);
  static DetailedStack from_stack_string(const std::string &stack);

  const std::string &to_string() const { return m_stack.to_string(); }

  std::vector<FormattedStackContainer::StackEntry> entries() const { return m_stack.entries(); }

private:
  FormattedStackContainer m_stack;
};


class APPULATRIX_EXPORT StackHash
{
public:
  StackId add(const Stack &stk);
  boost::optional<StackId> find(const Stack &stk);

private:
  std::unordered_map<Stack, StackId> m_stacks;
};

}
