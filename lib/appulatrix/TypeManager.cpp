#include "appulatrix/TypeManager.h"
#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/List.h"
#include "appulatrix/TypeBuilder.h"

namespace appulatrix
{
  
TypeList::TypeList(AllocatableBlock *allocatable)
: m_allocator(allocatable)
, m_types(m_allocator->create<List<Type>>(m_allocator.get()))
{
}

const Type *TypeList::add_type(const Type::Id index, const TypeBuilder &type)
{
  auto val = m_types->emplace_back(index, type, *m_allocator);
  assert(val);
  return val;
}
  
TypeManager::TypeManager()
: m_next_type_id(std::size_t(BuiltInTypes::Indices::FirstUserType))
, m_type_count(0)
, m_current_type_list(nullptr)
{
}

TypeManager::~TypeManager()
{
  for (auto &&t : m_reset)
  {
    t();
  }
}

void TypeManager::set_current_type_list(appulatrix::TypeList &list)
{
  m_current_type_list = &list;
}
  
const Type *TypeManager::add_type(const TypeBuilder &type, std::function<void()> const& reset)
{
  TypeId this_id = 0;
  if (type.builtin_id())
  {
    this_id = *type.builtin_id();
  }
  else
  {
    this_id = m_next_type_id++;
  }
  m_type_count++;
  
  assert(m_current_type_list);
  auto index = Type::Id(this_id);
  auto type_ptr = m_current_type_list->add_type(index, type);
  m_types.push_back(type_ptr);
  m_types_by_id.resize(std::max(m_types_by_id.size(), std::size_t(this_id + 1)));
  assert(!m_types_by_id[this_id]);
  m_types_by_id[this_id] = type_ptr;
  m_reset.emplace_back(reset);
  return type_ptr;
}

const Type &TypeManager::find_type(Type::Id index) const
{
  assert(index < m_types_by_id.size());
  auto type = m_types_by_id[index];
  assert(type);
  return *type;
}


}
