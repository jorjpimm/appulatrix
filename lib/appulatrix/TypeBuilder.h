#pragma once

#include "appulatrix_export.h"
#include "appulatrix/Type.h"

#include <gsl/gsl-lite.h>

#include <string>
#include <vector>

namespace appulatrix {

/// Utility to help build a Type.
class TypeBuilder
{
public:
  /// Create a new type builder
  /// \param name       The name of the type
  /// \param size       The size in bytes of the type
	/// \param type       The data type representation for the type
  /// \param builtin_id The built in id assigned to this type
  /// \param manager    The name of the type
  TypeBuilder(
    const char *name,
    std::size_t size,
    Type::DataType type,
    const boost::optional<Type::Id> &builtin_id,
    TypeManager &manager)
  : m_name(name)
  , m_size(size)
	, m_data_type(type)
  , m_builtin_id(builtin_id)
  , m_manager(manager)
  {
  }
  
  /// Add a field by name and member pointer.
  /// \param name   The name of the field.
  /// \param member The address of the field.
  template <typename Cls, typename T> TypeBuilder &add_interface(
    const char *name,
    T Cls::*member)
  {
    add_field_impl(
      name,
      find_offset(member),
      Type::find_index<T>(m_manager),
      Type::Field::null_parent(),
      Type::Field::FlagsUnion(Type::Field::Flag::Interface)
    );
    return *this;
  }
  
  /// Add a new field to the type.
  /// \param name   The name of the field.
  /// \param offset The offset of the field in bytes.
  /// \param type   The type of the field.
  TypeBuilder &add(
    const char *name,
    std::size_t offset,
    Type::Id type)
  {
    add_field_impl(name, offset, type);
    return *this;
  }

  /// Add a field by name and member pointer.
  /// \param name   The name of the field.
  /// \param member The address of the field.
  template <typename Cls, typename T> TypeBuilder &add(
    const char *name,
    T Cls::*member)
  {
    add_field_impl(name, find_offset(member), Type::find_index<T>(m_manager));
    return *this;
  }

  /// Add a member array field by name and pointer.
  /// \param name   The name of the field.
  /// \param member The address of the array field.
  template <typename Cls, typename T, std::size_t Size> TypeBuilder &add(
    const char *name,
    T (Cls::*member)[Size])
  {
    auto element_size = find_element_size<T>();
    add_field_array_impl(name, find_offset(member), Size, element_size, Type::find_index<T>(m_manager));
    return *this;
  }

  /// Add a member std::array field by name and pointer.
  /// \param name   The name of the field.
  /// \param member The address of the array field.
  template <typename Cls, typename T, std::size_t Size> TypeBuilder &add(
    const char *name,
    std::array<T, Size> Cls::*member)
  {
    auto element_size = find_element_size<T>();
    add_field_array_impl(name, find_offset(member), Size, element_size, Type::find_index<T>(m_manager));
    return *this;
  }

  /// Description of a structure field
  struct Field
  {
    std::string name;
    Type::FieldOffset offset;
    Type::FieldIndex parent;
    Type::Id type;
    Type::Field::FlagsUnion flags;
  };

  /// Find the structure size in bytes.
  std::size_t size() const { return m_size; }
  /// Find the data type stored in the type.
  Type::DataType data_type() const { return m_data_type; }
  /// Find the name of the structure.
  const std::string &name() const { return m_name; }
  /// Find the fields in the structure.
  const std::vector<Field> &fields() const { return m_fields; }
  /// Find the builtin id for this type.
  const boost::optional<Type::Id> &builtin_id() const { return m_builtin_id; }

private:
  /// Find the offset of the given field
  template <typename Cls, typename T> std::size_t find_offset(T Cls::*member)
  {
    Cls *cls = nullptr;
    auto *ptr = &(cls->*member);
    return reinterpret_cast<const char *>(ptr) -
      reinterpret_cast<const char *>(cls);
  }

  /// Find the size of the Element when in an
  /// array (includes any alignment padding).
  template <typename Element> std::size_t find_element_size()
  {
    Element *e = nullptr;
    auto end = e + 1;
    auto begin = e;

    return reinterpret_cast<const char *>(end) -
      reinterpret_cast<const char *>(begin);
  }

  /// Helper method to add a field by name and offset.
  Type::FieldIndex add_field_impl(
    std::string name,
    std::size_t offset,
    Type::Id type,
    Type::FieldIndex parent = Type::Field::null_parent(),
    Type::Field::FlagsUnion flags = 0)
  {
    if (offset > std::numeric_limits<Type::FieldOffset>::max())
    {
      assert(0);
      throw std::runtime_error("Invalid offset for type");
    }
    
    auto short_offset = static_cast<Type::FieldOffset>(offset);
    auto index = m_fields.size();
    if (index >= std::numeric_limits<Type::FieldIndex>::max())
    {
      assert(0);
      throw std::runtime_error("Field count reached for type");
    }
    
    m_fields.emplace_back(Field{
      std::move(name),
      short_offset,
      parent,
      type,
      flags
    });
    return static_cast<Type::FieldIndex>(index);
  }

  /// Helper metho to add an array field by offset and name.
  std::size_t add_field_array_impl(
    const char *name,
    std::size_t offset,
    std::size_t size,
    std::size_t element_size,
    Type::Id type,
    Type::FieldIndex parent = Type::Field::null_parent())
  {
    auto element_parent = add_field_impl(name, offset, Type::invalid_index(), parent);
    for (std::size_t i = 0; i < size; ++i)
    {
      add_field_impl(std::to_string(i), offset + element_size * i, type, element_parent);
    }
    return element_parent;
  }

  std::vector<Field> m_fields;
  std::string m_name;
  std::size_t m_size;
	Type::DataType m_data_type;
  boost::optional<Type::Id> m_builtin_id;
  TypeManager &m_manager;
};

}
