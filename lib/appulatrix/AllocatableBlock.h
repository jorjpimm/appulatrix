#pragma once

#include "appulatrix_export.h"
#include "appulatrix/InternalPointer.h"

#include <gsl/gsl-lite.h>

#include <atomic>
#include <cassert>

namespace appulatrix {

class AllocatableBlock;

namespace detail {

/// Utility to dispatch constructor arguments
struct ConstructorDispatch
{
  struct general_ {};
  struct special_ : general_ {};
  template<typename> struct int_ { typedef int type; };

  /// Overload which will catch normal types
  template <typename T, typename... Args>
  static void create(general_, void *data, AllocatableBlock &, Args &&...args)
  {
    new(data) T{ std::forward<Args>(args)... };
  }

  /// Overload to catch structures with a [construct_event] member
  template <
    typename T,
    typename int_<decltype(T::construct_event)>::type = 0,
    typename... Args>
  static void create(special_, void *data, AllocatableBlock &blk, Args &&...args)
  {
    T::construct_event(data, blk, std::forward<Args>(args)...);
  }
};

}

/// Manages allocation inside a given block of data
///
/// Does not allow freeing of data, except via a
/// global method to release all data.
class APPULATRIX_EXPORT AllocatableBlock
{
public:
  /// Manage how a block is constructed
  enum class InitialisationMode
  {
    Create,     ///< Create a new block
    UseExisting ///< Refer to an existing block, sharing ownership.
  };

  /// Create an allocatable block, stored in [data].
  /// \param data Location of the allocated data.
  /// \param mode	How should the block be initialised.
  /// \note Expects a piece of memory at least [min_data_size] bytes.
  AllocatableBlock(
    gsl::span<char> data,
    InitialisationMode mode = InitialisationMode::Create);

  /// Find the number of bytes which can be allocated in total in this block.
  std::size_t allocatable_capacity() const;
  /// Find the currently allocated byte count in the block.
  std::size_t allocated_size() const;
  /// Find the number of bytes available to allocate.
  std::size_t available_size() const;

  /// Change the location the block refers to,
  /// without changing any of the allocations in the block.
  void reset_location(gsl::span<char> data);

  /// Discard any allocated data,
  /// ensuring available_size == allocatable_capacity.
  void clear();

  /// Allocate a new piece of memory, of [size] bytes.
  /// \note Throws std::bad_alloc on failure.
  void *allocate(std::size_t size);

  /// Create a new type [T], passing [args] as constructor arguments
  ///
  /// [T] should either have a constructor matching [args], or
  /// a static method [construct_event]:
  ///
  /// \code{.cpp}
  /// struct ExampleStruct {
  ///   void construct_event(
  ///     void *data,                   ///< Memory for object
  ///     AllocatableBlock &allocator,  ///< Allocator for ExampleStruct 
  ///                                   ///  to allocate sub-structures
  ///     args...                       ///< Forwarded arguments for class
  ///   );
  /// };
  /// \endcode
  ///
  /// \param args Arguments to pass to the constructor.
  template <typename T, typename... Args> T *create(Args &&...args)
  {
    auto data = allocate(sizeof(T));
    detail::ConstructorDispatch::create<T>(
      detail::ConstructorDispatch::special_(),
      data,
      *this,
      std::forward<Args>(args)...
    );
    return static_cast<T*>(data);
  }

  /// Find the minimum size of data that can be used to construct this block.
  std::size_t min_data_size();

  /// Get the data the allocator is allocating into
  ///
  /// The memory starts directly after the header requiered by the allocator
  /// and continues until the end of the block.
  gsl::span<const char> get_data() const
  {
    return gsl::span<const char>(get_allocatable_start(), get_allocatable_end());
  }

private:
  struct Header;
  Header *get_header();
  const Header *get_header() const;
  char *get_allocatable_start();
  const char *get_allocatable_start() const;
  char *get_allocatable_end();
  const char *get_allocatable_end() const;

  InternalPointer<char> m_data;
  std::size_t m_size;
};

}
