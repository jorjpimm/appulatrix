#include "appulatrix/EventManager.h"
#include "appulatrix/Type.h"

namespace appulatrix
{

EventManager::EventManager(AllocatableBlock *allocatable)
: m_allocator(allocatable)
, m_events(m_allocator->create<List<Event>>(m_allocator.get()))
{
}

}
