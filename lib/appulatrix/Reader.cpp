#include "appulatrix/Reader.h"
#include "appulatrix/detail/LoggerDetails.h"

namespace appulatrix
{

Reader::Reader(const boost::filesystem::path &path)
: m_path(path)
, m_current_file(0)
, m_current_file_read_count(0)
{
  m_header = detail::MappedObject<detail::Header>::open(detail::header_file_path(path));
  if (!m_header)
  {
    throw std::runtime_error("Failed to open sequence at '" + path.string() + "', missing header.");
  }
  load_all_files();
  post_file_change();
}

void Reader::load_all_files()
{
  std::lock_guard<std::mutex> l(m_file_mutex);

  auto old_size = m_files.size();
  m_files.resize(m_header->get().active_file()+1);
  for (std::size_t i = old_size; i < m_files.size(); ++i)
  {
    m_files[i] = detail::MappedObject<detail::DataFile>::open(detail::data_file_path(m_path, i));
    if (!m_files[i])
    {
      // Failed to open file...
      m_files.resize(i);
      break;
    }
  }
}
  
void Reader::load_all_types(
  detail::DataFile &file,
  List<Type>::Iterator &start)
{
  auto end = file.m_types.types().end();
  while (start != end)
  {
    auto new_type = start.ptr();
    auto new_id = new_type->id();
    m_types.m_registered_types.push_back(new_type);
    m_types.m_type_lookup.resize(std::max(m_types.m_type_lookup.size(), std::size_t(new_id + 1)));
    m_types.m_type_lookup[new_id] = new_type;
    
    ++start;
  }
}
  
void Reader::load_all_types()
{
  load_all_types(current_data_file(), m_current_file_type_iterator);
}

void Reader::load_all_stacks(
  detail::DataFile &file,
  List<StackRef>::Iterator &start)
{
  auto end = file.m_stacks.stacks().end();
  while (start != end)
  {
    m_stacks.push_back(start.ptr());
    ++start;
  }
}
  
void Reader::load_all_stacks()
{
  load_all_stacks(current_data_file(), m_current_file_stack_iterator);
}

detail::DataFile &Reader::current_data_file()
{
  return m_files[m_current_file]->get();
}

gsl::span<const Event> Reader::read()
{
  gsl::span<const char> data;
  return read_with_data(data);
}
  
gsl::span<const Event> Reader::read_with_data(gsl::span<const char> &referenced_data)
{
  load_all_files();
  assert(m_current_file < m_files.size());

  std::lock_guard<std::mutex> l(m_read_mutex);
  while (true)
  {
    load_all_types();
    load_all_stacks();

    // Read from current file, if we can get anything
    if (auto read = read_current_file(l))
    {
      auto &file = m_files[m_current_file];
      const auto begin = &file->get();
      referenced_data = gsl::span<const char>(
        reinterpret_cast<const char*>(begin),
        begin->m_allocator.get_data().end()
      );
      return *read;
    }

    // If the active file has not moved
    // on, we need to wait for more data.
    if (m_current_file+1 >= m_files.size())
    {
      break;
    }

    pre_file_change();

    // otherwise, the active file has moved,
    // move the current file on.
    ++m_current_file;
    post_file_change();
  }

  return {};
}
  
  gsl::span<const Event> read_with_data(gsl::span<char> &referenced_data);

std::size_t Reader::stack_count() const
{
  return m_stacks.size();
}

const StackRef &Reader::get_stack(StackId index) const
{
  return *m_stacks[index];
}

boost::optional<gsl::span<const Event>>
  Reader::read_current_file(std::lock_guard<std::mutex> &)
{
  auto &file = m_files[m_current_file];
  auto &events = file->get().m_events;
  auto &event_list = events.events();
  if (m_current_file_read_count >= event_list.size())
  {
    return boost::none;
  }

  auto size = event_list.get_contiguous_size_from(m_read_point);
  if (size == 0)
  {
    if (m_read_point == event_list.end())
    {
      return boost::none;
    }

    ++m_read_point;
    return boost::none;
  }

  auto start = m_read_point;
  m_read_point += size;
  return gsl::span<const Event>{ start.ptr(), size };
}

void Reader::pre_file_change()
{
  load_all_types();
  load_all_stacks();
}

void Reader::post_file_change()
{
  initialise_read_point();
  m_current_file_type_iterator = current_data_file().m_types.types().begin();
  m_current_file_stack_iterator = current_data_file().m_stacks.stacks().begin();
}

void Reader::initialise_read_point()
{
  auto &new_event_manager = m_files[m_current_file]->get().m_events;
  m_read_point = new_event_manager.events().begin();
}

const Type &Reader::TypeLookupImpl::find_type(Type::Id index) const
{
  assert(index < m_type_lookup.size());
  return *m_type_lookup[index];
}

gsl::span<const Type * const> Reader::TypeLookupImpl::type_list() const
{
  return m_registered_types;
}

}
