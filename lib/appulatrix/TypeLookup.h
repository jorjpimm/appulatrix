#pragma once

#include "appulatrix_export.h"
#include "appulatrix/Type.h"

#include <gsl/gsl-lite.h>

#include <string>
#include <vector>

namespace appulatrix {

/// Interface which allows looking up type information
/// from both reader and writer perspectives.
class TypeLookup
{
public:
  /// Find a list of types which have been registered.
  virtual gsl::span<const Type * const> type_list() const = 0;
  
  /// Find a type by a given id.
  virtual const Type &find_type(Type::Id search_id) const = 0;
};

}
