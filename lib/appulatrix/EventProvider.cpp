#include "appulatrix/EventProvider.h"
#include "appulatrix/Logger.h"
#include "appulatrix/TypeManager.h"

#include <thread>

IMPLEMENT_TYPE_BUILDER_BUILTIN(appulatrix::EventProviderData, type, EventProvider)
{
  type.add("enabled", &appulatrix::EventProviderData::m_enabled);
  type.add("name", &appulatrix::EventProviderData::m_name);
}

IMPLEMENT_TYPE_BUILDER_BUILTIN(appulatrix::EventBlockBegin, type, EventBlockBegin)
{
  type.add("id", &appulatrix::EventBlockBegin::m_id);
}

IMPLEMENT_TYPE_BUILDER_BUILTIN(appulatrix::EventBlockEnd, type, EventBlockEnd)
{
  type.add("id", &appulatrix::EventBlockEnd::m_id);
}

namespace appulatrix
{
  
EventProviderData::EventProviderData(
  Id id,
  Id parent,
  const char *name,
  AllocatableBlock &allocator)
: m_enabled(true)
, m_id(id)
, m_parent(parent)
, m_name(name, allocator)
{
}
  
void EventProviderData::construct_event(
  void *location,
  AllocatableBlock &allocator,
  const char *name,
  Id id,
  Id parent)
{
  new(location) EventProviderData(
    id,
    parent,
    name,
    allocator);
}

struct EventProvider::GlobalData
{
  GlobalData()
  : m_root(0, nullptr, "root")
  , m_next_index(1)
  {
  }
  
  EventProvider m_root;
  Id m_next_index;
  std::mutex m_create_mutex;

  static GlobalData *get_global_provider_data()
  {
    static std::unique_ptr<GlobalData> g_event_provider_data(
      new GlobalData{}
    );

    return g_event_provider_data.get();
  }

};

void EventProvider::initialise_providers(Logger *logger)
{
  auto glb = GlobalData::get_global_provider_data();
  glb->m_root.setup_data(logger);
}

void EventProvider::terminate_providers()
{
  auto glb = GlobalData::get_global_provider_data();
  glb->m_root.setup_data(nullptr);
  glb->m_next_index = 1;
}

EventProvider::EventProvider(Id index, EventProvider *parent, const char *name)
: m_index(index)
, m_name(name)
, m_logger(nullptr)
, m_parent(parent)
, m_data(nullptr)
, m_in_use(false)
{
  if (m_parent)
  {
    m_parent->m_children.insert(this);
  }
}

EventProvider::~EventProvider()
{
  if (m_parent)
  {
    m_parent->m_children.erase(this);
  }
}

void EventProvider::setup_data(Logger *logger)
{
  m_logger = logger;
  m_data = nullptr;

  if (m_logger)
  {
    auto parent = std::numeric_limits<Id>::max();
    if (m_parent)
    {
      parent = m_parent->m_index;
    }
    
    auto lock = TryLock(this);
    assert(lock.owns_lock());
    
    auto type = Type::find_index<EventProviderData>(type_manager());
    auto event_out = add_event_impl<EventProviderData>(
      type,
      0,
      m_name.c_str(),
      m_index,
      parent);

    m_data = std::get<1>(event_out);
  }

  for (auto child : m_children)
  {
    child->setup_data(logger);
  }
}
  
void EventProvider::set_enabled(bool enabled)
{
  if (!m_logger)
  {
    return;
  }
  
  m_data->set_enabled(enabled);
}
  
bool EventProvider::is_enabled() const
{
  return m_logger && m_data->is_enabled();
}

EventProvider::EventProvider(EventProvider &&oth)
: m_index(oth.m_index)
, m_name(oth.m_name)
, m_logger(oth.m_logger)
, m_parent(oth.m_parent)
, m_children(oth.m_children)
, m_data(oth.m_data)
, m_in_use(oth.m_in_use.load())
{
  if (m_parent)
  {
    m_parent->m_children.insert(this);
  }
}

EventProvider &EventProvider::operator=(EventProvider &&oth)
{
  if (m_parent)
  {
    m_parent->m_children.erase(this);
  }
  
  m_index = oth.m_index;
  m_name = oth.m_name;
  m_logger = oth.m_logger;
  m_parent = oth.m_parent;
  m_children = std::move(oth.m_children);
  m_data = oth.m_data;
  m_in_use = oth.m_in_use.load();
  
  if (m_parent)
  {
    m_parent->m_children.insert(this);
  }
  return *this;
}

EventProvider EventProvider::create_top_level(const char *name)
{
  return GlobalData::get_global_provider_data()->m_root.create_child(name);
}

EventProvider EventProvider::create_child(const char *name)
{
  auto glb_data = GlobalData::get_global_provider_data();
  std::lock_guard<std::mutex> l(glb_data->m_create_mutex);

  auto prov = EventProvider(glb_data->m_next_index++, this, name);
  if (m_logger)
  {
    prov.setup_data(m_logger);
  }

  return prov;
}

StackId EventProvider::get_stack(std::size_t skip)
{
  if (!m_logger)
  {
    throw std::runtime_error("Not initialised");
  }

  return m_logger->stack_manager().get_stack(skip);
}
  
const StackManager &EventProvider::stack_manager() const
{
  if (!m_logger)
  {
    throw std::runtime_error("Not initialised");
  }
  
  return m_logger->stack_manager();
}

EventManager &EventProvider::event_manager()
{
  if (!m_logger)
  {
    throw std::runtime_error("Not initialised");
  }

  return m_logger->active_event_manager();
}

TypeManager &EventProvider::type_manager()
{
  if (!m_logger)
  {
    throw std::runtime_error("Not initialised");
  }
  
  return m_logger->type_manager();
}

void EventProvider::reallocate_storage()
{
  if (!m_logger)
  {
    throw std::runtime_error("Not initialised");
  }
  
  return m_logger->next_active_file();
}

}
