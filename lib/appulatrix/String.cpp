#include "appulatrix/String.h"
#include "appulatrix/AllocatableBlock.h"

namespace appulatrix
{

void String::build(const char *src, std::size_t size, AllocatableBlock &allocator)
{
  auto data = allocator.allocate(size + 1);
  
  m_data = static_cast<char*>(data);
  
  std::copy(src, src + size, m_data.get());
	m_data.get()[size] = '\0';
}

}
