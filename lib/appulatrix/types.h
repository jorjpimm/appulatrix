#pragma once

#include <cstdint>

namespace appulatrix
{

using StackId = std::uint16_t;
using TypeId = std::uint16_t;
using EventProviderId = std::uint16_t;
  
template <typename T> class TypeFinder;

}
