#pragma once

#include "appulatrix_export.h"
#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/Event.h"
#include "appulatrix/List.h"
#include "appulatrix/Type.h"

#include <chrono>

namespace appulatrix {

class Type;
class TypeBuilder;

/// Manager a list of events, appending new events as required
class APPULATRIX_EXPORT EventManager
{
public:
  /// Create a new event manager, all data created is stored in allocatable.
  /// \param allocatable The allocatable block to place events into
  EventManager(AllocatableBlock *allocatable);

  /// Add a new event to the sequence.
  /// The event is created with user data of type [T], [type] must represent the
  /// id for template argument.
  ///
  /// \param data_out The created type [T]
  /// \param type     The type of data to create
  /// \param stack    The stack associated with the event
  /// \param provider The owning event provider for the event
  /// \param args     Arguments to pass to the constructor for the events data
  template <typename T, typename... Args> const Event *add_event(
    T **data_out,
    TypeId type,
    StackId stack,
    EventProviderId provider,
    Args &&...args)
  {
    *data_out = m_allocator->create<T>(std::forward<Args>(args)...);

    return m_events->emplace_back(
      std::chrono::high_resolution_clock::now(),
      *data_out,
      stack,
      type,
      provider
    );
  }

  /// Find a list of events already added to the manager
  const List<Event> &events() const { return *m_events; }

  /// Find a size representing the smallest size the manager will allocate, when required.
  std::size_t min_allocation_size() const { return m_events->min_allocation_size(); }

private:
  InternalPointer<TypeManager> m_types;
  InternalPointer<AllocatableBlock> m_allocator;
  InternalPointer<List<Event>> m_events;
};

}
