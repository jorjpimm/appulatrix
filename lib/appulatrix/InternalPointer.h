#pragma once

#include <cassert>
#include <atomic>
#include <cstdint>
#include <limits>
#include <stdexcept>

namespace appulatrix {

/// Pointer type which stores the pointer as an offset from [this].
///
/// This utility allows storing a pointer in a piece of memory which
/// may be relocated safely.
template <typename T> class InternalPointer
{
public:
  /// Create a new pointer with a null value.
  InternalPointer()
  : InternalPointer(nullptr)
  {
  }

  /// Create a null pointer.
  InternalPointer(std::nullptr_t)
  : m_offset(null_value())
  {
  }

  /// Create a pointer refering to [offset].
  InternalPointer(T *offset)
  : InternalPointer()
  {
    assign(offset);
  }

  /// Duplicating a pointer is not allowed
  /// temporary [InternalPointer] classes may be stored far from
  /// the refered to storage, instead a real pointer should be used.
  InternalPointer(const InternalPointer &ptr) = delete;
  InternalPointer &operator=(const InternalPointer &ptr) = delete;
  
  /// Assign a new address to the internal pointer
  InternalPointer &operator=(T *ptr)
  {
    assign(ptr);
    return *this;
  }

  /// Assign this pointer the value from another pointer
  void assign(const InternalPointer &oth)
  {
    assign(oth.get());
  }

  /// Assign this pointer the value from another pointer
  void assign(const T *val)
  {
    m_offset = as_offset(val);
  }

  /// Find the value of this pointer
  T *get()
  {
    return from_offset(m_offset);
  }

  /// Find the value of this pointer
  const T *get() const
  {
    return from_offset(m_offset);
  }

  /// Dereference this pointer to its object
  /// \note asserts the value is correct
  typename std::add_lvalue_reference<T>::type operator*()
  {
    auto val = get();
    assert(val);
    return *val;
  }

  /// Dereference this pointer to its object
  /// \note asserts the value is correct
  typename std::add_lvalue_reference<const T>::type operator*() const
  {
    auto val = get();
    assert(val);
    return *val;
  }

  /// Check if this pointer is equal to another
  bool operator==(const InternalPointer &oth) const
  {
    return get() == oth.get();
  }

  /// Check if this pointer is not equal to another
  bool operator!=(const InternalPointer &oth) const
  {
    return get() != oth.get();
  }

  /// Check if this pointer is equal to another
  bool operator==(const T *oth) const
  {
    return get() == oth;
  }

  /// Check if this pointer is not equal to another
  bool operator!=(const T *oth) const
  {
    return get() != oth;
  }

  /// Find if this pointer is non-null
  explicit operator bool() const { return *this != InternalPointer(); }

  bool compare_exchange_strong(
    T *& expected,
    T *desired)
  {
    auto exp_off = as_offset(expected);
    auto res = m_offset.compare_exchange_strong(exp_off, as_offset(desired));
    expected = from_offset(exp_off);
    return res;
  }

  /// Swap in [desired] as the new value of this pointer, if [expected] is
  /// the current value of this pointer.
  bool compare_exchange_weak(
    T *& expected,
    T *desired)
  {
    auto exp_off = as_offset(expected);
    auto res = m_offset.compare_exchange_weak(exp_off, as_offset(desired));
    expected = from_offset(exp_off);
    return res;
  }

  /// Find the value of this pointer
  /// \note asserts the pointer is non-null
  T *operator->()
  {
    auto val = get();
    if (!val)
    {
      assert(val);
    }
    return val;
  }

  /// Find the value of this pointer
  /// \note asserts the pointer is non-null
  const T *operator->() const
  {
    auto val = get();
    if (!val)
    {
      assert(val);
    }
    return val;
  }

  /// The internal storage type of the pointer.
  /// This requires any pointed to object is 
  /// within min/max DiffType bytes.
  using DiffType = std::int32_t;

  /// Find the current offset in bytes from [this].
  DiffType offset() const { return m_offset; }

private:
  /// Convert [val] into a integer offset from [this].
  DiffType as_offset(const T *val) const
  {
    if (val == nullptr)
    {
      return null_value();
    }


    std::ptrdiff_t diff_val = reinterpret_cast<const char *>(val) -
      reinterpret_cast<const char *>(this);

    if (diff_val > std::numeric_limits<DiffType>::max() ||
      diff_val < std::numeric_limits<DiffType>::min())
    {
      throw std::runtime_error("Invalid pointer value");
    }

    return DiffType(diff_val);
  }

  /// Convert [val] into a pointer type.
  T *from_offset(DiffType val) const
  {
    if (val == null_value())
    {
      return nullptr;
    }

    auto ptr_val = reinterpret_cast<const char *>(this) + m_offset;
    return const_cast<T *>(reinterpret_cast<const T *>(ptr_val));
  }

  static DiffType null_value()
  {
    return std::numeric_limits<DiffType>::max();
  }

  std::atomic<DiffType> m_offset;
  template <typename U> friend class TypeFinder;
};


}
