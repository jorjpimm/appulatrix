#pragma once

#include "appulatrix_export.h"
#include "appulatrix/TypeLookup.h"
#include "appulatrix/StackManager.h"

#include <boost/filesystem/path.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/optional/optional.hpp>

#include <array>
#include <cstddef>
#include <mutex>
#include <gsl/gsl-lite.h>

namespace appulatrix {

class Event;
namespace detail {
class Header;
class MappedFile;
template <typename T> class MappedObject;
class DataFile;
}

/// Utility to read events from the logger in order
class APPULATRIX_EXPORT Reader
{
public:
  Reader(const boost::filesystem::path &path);

  /// Read as many events as possible from the manager
  gsl::span<const Event> read();
  gsl::span<const Event> read_with_data(gsl::span<const char> &referenced_data);

  TypeLookup &types()
  {
    return m_types;
  }

  std::size_t stack_count() const;
  const StackRef &get_stack(StackId index) const;
  
private:
  void load_all_files();
  void load_all_types(
    detail::DataFile &file,
    List<Type>::Iterator &start);
  void load_all_types();
  void load_all_stacks(
    detail::DataFile &file,
    List<StackRef>::Iterator &start);
  void load_all_stacks();

  detail::DataFile &current_data_file();

  boost::optional<gsl::span<const Event>> read_current_file(std::lock_guard<std::mutex> &read_mutex);
  void pre_file_change();
  void post_file_change();
  void initialise_read_point();

  boost::filesystem::path m_path;
  std::shared_ptr<detail::MappedObject<detail::Header>> m_header;
  std::vector<std::shared_ptr<detail::MappedObject<detail::DataFile>>> m_files;
  std::mutex m_file_mutex;
  std::mutex m_read_mutex;

  class TypeLookupImpl : public TypeLookup
  {
  public:
    const Type &find_type(Type::Id index) const override;
    gsl::span<const Type * const> type_list() const override;
    
  private:
    std::vector<const Type *> m_registered_types;
    std::vector<const Type *> m_type_lookup;
    
    friend class Reader;
  };
  TypeLookupImpl m_types;
  List<Type>::Iterator m_current_file_type_iterator;
  std::vector<const StackRef *> m_stacks;
  List<StackRef>::Iterator m_current_file_stack_iterator;

  std::size_t m_current_file;
  std::size_t m_current_file_read_count;
  List<Event>::Iterator m_read_point;
};


}
