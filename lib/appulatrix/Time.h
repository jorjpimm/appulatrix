#pragma once

#include <chrono>

namespace appulatrix {

using Time = std::chrono::high_resolution_clock::time_point;

}
