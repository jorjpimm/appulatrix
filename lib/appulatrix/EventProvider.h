#pragma once

#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventManager.h"
#include "appulatrix/Stack.h"
#include "appulatrix/Type.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/types.h"

#include <set>

namespace appulatrix
{

class Logger;
class Event;
class EventManager;
class StackManager;

/// Data contained inside each event provider,
/// controlling internal settings
class EventProviderData
{
public:
  using Id = EventProviderId;
  static void construct_event(
    void *location,
    AllocatableBlock &allocator,
    const char *name,
    Id id,
    Id parent);
  
  /// Find if the provider is currently enabled
  bool is_enabled() const { return m_enabled; }
  /// Set the enabled state of the event provider
  void set_enabled(bool enabled)
  {
    m_enabled = enabled;
  }

  /// Find the id associated with the provider
  Id id() const { return m_id; }
  /// The parent of the provider
  Id parent() const { return m_parent; }

  /// The name of the provider
  const String &name() const { return m_name; }
  
private:
  EventProviderData(
    Id id,
    Id parent,
    const char *name,
    AllocatableBlock &allocator);

  bool m_enabled;
  Id m_id;
  Id m_parent;
  String m_name;

  friend class TypeFinder<EventProviderData>;
};

struct EventBlockBegin
{
  std::uint64_t m_id;
};

struct EventBlockEnd
{
  std::uint64_t m_id;
};

}

DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, EventProviderData)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, EventBlockBegin)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, EventBlockEnd)

namespace appulatrix
{

/// An event provider owns a stream of events, and manages their emission
/// into the logger.
class APPULATRIX_EXPORT EventProvider
{
public:
  using Id = EventProviderId;

  /// Initialise event providers globally, all event
  /// loggers will write into [logger].
  ///
  /// All existing event providers are cleared and reinitialised.
  static void initialise_providers(Logger *logger);
  /// Clear all event providers, no further events are emitted.
  static void terminate_providers();

  /// Create an event provider with no parent provider.
  /// \param name The name of the provider
  static EventProvider create_top_level(const char *name);
  /// Create an event provider under [this].
  /// \param name The name of the provider
  EventProvider create_child(const char *name);
  ~EventProvider();

  /// Emit an event of with user data of type [T] to this event provider.
  /// \param args Arguments to pass to constructor of [T]
  /// \note If the provider is disabled, no event is emitted.
  /// \note If the provider is already emitting an event (eg, 
  ///       event within an event, or on another thread), no event
  ///       is emitted.
  template <typename T, typename... Args> void emit_event(Args &&...args)
  {
    emit_event_with_skipped_stack<T>(0, std::forward<Args&&>(args)...);
  }

  /// Emit an event of with user data of type [T] to this event provider.
  /// \param type The type of the event to emit
  /// \param args Arguments to pass to constructor of [T]
  /// \note If the provider is disabled, no event is emitted.
  /// \note If the provider is already emitting an event (eg, 
  ///       event within an event, or on another thread), no event
  ///       is emitted.
  template <typename T, typename... Args> void emit_event(
    TypeId type,
    Args &&...args)
  {
    auto lock = TryLock(this);
    if (!m_data || !m_data->is_enabled() || !lock.owns_lock())
    {
      return;
    }

    add_event_impl<T>(
      type, 
      0, 
      std::forward<Args&&>(args)...
    );
  }

  /// Emit an event of with user data of type [T] to this event provider.
  /// \param skip_count The number of elements in the stack to skip.
  /// \param args       Arguments to pass to constructor of [T]
  /// \note If the provider is disabled, no event is emitted.
  /// \note If the provider is already emitting an event (eg, 
  ///       event within an event, or on another thread), no event
  ///       is emitted.
  template <typename T, typename... Args> void emit_event_with_skipped_stack(
    std::size_t skip_count,
    Args &&...args)
  {
    auto lock = TryLock(this);
    if (!m_data || !m_data->is_enabled() || !lock.owns_lock())
    {
      return;
    }
    
    auto type = Type::find_index<T>(type_manager());
    add_event_impl<T>(
      type,
      skip_count,
      std::forward<Args&&>(args)...
    );
  }
  
  /// Change the enabled state of the provider
  /// disabled providers do not emit events into their streams.
  /// \param enabled Should the provider be set to enabled or disabled
  void set_enabled(bool enabled);
  /// Find if the provider is enabled
  bool is_enabled() const;

  /// Get a stack for the calling function, skipping [skip] frames
  /// \param skip Stack Frame count to skip
  StackId get_stack(std::size_t skip);

  /// Get the stack manager from the provider, allowing more complex
  /// stack queries
  const StackManager &stack_manager() const;

  /// Get the type manager from the provider, allowing more complex
  /// type registrations
  TypeManager &type_manager();

  /// Move the provider to a new location, moving all children
  /// and stream ownership with it.
  EventProvider(EventProvider &&);
  /// Assign another provider to this provider replacing all children
  /// and stream data.
  EventProvider &operator=(EventProvider &&);

  EventProvider(const EventProvider &) = delete;
  EventProvider &operator=(const EventProvider &) = delete;

private:
  /// Predeclaration of the internal provider global data
  struct GlobalData;

  /// Create a new event provider
  /// \note Should normally be invoked via [create_top_level] or [create_child].
  EventProvider(
      Id id,
      EventProvider *parent,
      const char *name);

  /// Get the event manager from the provider
  EventManager &event_manager();

  /// Setup the internal data to refer to [logger].
  /// \param logger The logger to push events to
  /// \note On setup the provider will emit a new [EventProviderData] event.
  void setup_data(Logger *logger);

  /// Internal method to create an event in the stream.
  /// \param type       The type of the event to create
  /// \param stack_skip The number of stack frames to skip.
  /// \param args       Arguments to pass to event data.
  template <typename T, typename... Args> 
  std::tuple<const Event *, T *> add_event_impl(
    TypeId type,
    std::size_t stack_skip,
    Args &&...args)
  {
    while(true)
    {
      try
      {
        auto &evts = event_manager();
        T *data_out = nullptr;
        auto event = evts.add_event<T>(
          &data_out,
          type,
          get_stack(stack_skip),
          m_index,
          std::forward<Args&&>(args)...);
        return std::make_tuple(event, data_out);
      }
      catch(const std::bad_alloc &)
      {
        reallocate_storage();
      }
    }
  }

  /// Skip the providers internal logger onto the next file
  /// normally because the current file is full.
  /// \todo Currently if you call this twice quickly from two threads
  ///       you might skip a whole file and waste space...
  void reallocate_storage();

  /// Locks the providers event system until released
  /// No events can be emitted whilst it is locked.
  class TryLock
  {
  public:
    TryLock(EventProvider *ths)
    : m_ths(ths)
    {
      bool exp = false;
      did_lock = m_ths->m_in_use.compare_exchange_strong(exp, true);
    }
    
    ~TryLock()
    {
      if (did_lock)
      {
        m_ths->m_in_use = false;
      }
    }
    
    /// Find if the this [Lock] owns the lock.
    bool owns_lock() const { return did_lock; }
    
  private:
    EventProvider *m_ths;
    bool did_lock;
  };
  friend class TryLock;

  Id m_index;
  std::string m_name;
  Logger *m_logger;
  EventProvider *m_parent;
  std::set<EventProvider *> m_children;
  EventProviderData *m_data;
  std::atomic<bool> m_in_use;
};

namespace detail
{

/// A scoped event emits a single event at the beginning of a scope block
/// and another event when the scope exits.
///
/// Custom data can be attached to the start of the scoped block.
template <typename T> class ScopedEvent
{
public:
  /// Create a scoped event now, under [prov].
  /// \param prov The provider to add an event to.
  /// \param args The arguments to the start event custom type.
  template <typename... Args> ScopedEvent(EventProvider &prov, Args &&... args)
  : m_provider(&prov)
  {
    if (m_provider->is_enabled())
    {
      const auto type = build_event_begin_type(m_provider->type_manager());
      m_provider->emit_event<Begin>(type, reinterpret_cast<std::uint64_t>(this), std::forward<Args&&>(args)...);
    }
  }

  ~ScopedEvent()
  {
    m_provider->emit_event<EventBlockEnd>(reinterpret_cast<std::uint64_t>(this));
  }

private:
  /// Event type emitted at the beginning of the scope block
  struct Begin
  {
    template <typename... Args> Begin(
      std::uint64_t id, 
      Args &&... args)
    : m_block{ id }
    , m_data(std::forward<Args&&>(args)...)
    {
    }

    EventBlockBegin m_block;
    T m_data;
  };

  /// Build the type event emitted at the start of the scope.
  /// \param type_manager The manager to register the type t.
  static TypeId build_event_begin_type(TypeManager &type_manager)
  {
    static boost::optional<TypeId> type;
    if (!type)
    {
      std::lock_guard<std::recursive_mutex> l(type_manager.type_creation_mutex());
      if (!type)
      {
        auto reset = [&]{ type = boost::none; };
        TypeBuilder builder(
          (std::string("EventBlockBegin_") + typeid(T).name()).c_str(),
          sizeof(Begin),
          Type::DataType::Unknown,
          boost::none,
          type_manager
        );
        builder.add_interface("block", &Begin::m_block);
        builder.add("data", &Begin::m_data);
        type = type_manager.add_type(builder, reset)->id();
      }
    }
    return *type;
  }

  EventProvider *m_provider;
};
}

#if defined(__GNUC__) && (__GNUC__ >= 4)
# define WARN_UNUSED __attribute__ ((warn_unused_result))
#elif defined(_MSC_VER) && (_MSC_VER >= 1700)
# define WARN_UNUSED _Check_return_
#else
# define WARN_UNUSED
#endif

/// Create an event in [provider] of type [T].
/// The event will be scoped - beginning here and continuing until the returned object leaves scope.
template <typename T, typename... Args>
detail::ScopedEvent<T> WARN_UNUSED emit_scoped_event(EventProvider &provider, Args &&...args)
{
  return detail::ScopedEvent<T>(provider, std::forward<Args>(args)...);
}
  
/// Create an event in [provider] of type [T].
template <typename T, typename... Args>
void emit_event(EventProvider &provider, Args &&...args)
{
  provider.emit_event<T>(std::forward<Args>(args)...);
}
  
#undef WARN_UNUSED

}
