#pragma once

#include "appulatrix/EventManager.h"
#include "appulatrix/TypeManager.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>

#include <mutex>
#include <tuple>

namespace appulatrix
{
namespace detail
{

inline boost::filesystem::path data_file_path(const boost::filesystem::path &root, std::size_t index)
{
  return root / (std::to_string(index) + ".data");
}
inline boost::filesystem::path header_file_path(const boost::filesystem::path &root)
{
  return root / "header";
}

class MappedFile
{
public:
  static MappedFile create(
    const boost::filesystem::path path,
    std::size_t size)
  {
    {
      boost::filesystem::ofstream str(path);
    }

    boost::filesystem::resize_file(
      path,
      size
    );

    return MappedFile(path);
  }

  static MappedFile open(
    const boost::filesystem::path path)
  {
    return MappedFile(path);
  }

  gsl::span<char> data()
  {
    auto data = m_region.get_address();
    return gsl::span<char>(
      reinterpret_cast<char *>(data),
      reinterpret_cast<char *>(data) + m_region.get_size()
    );
  }

private:
  MappedFile(const boost::filesystem::path path)
  : m_path(std::move(path))
  , m_file(
    path.string().c_str(),
    boost::interprocess::read_write
  )
  , m_region(m_file, boost::interprocess::read_write)
  {
  }

  boost::filesystem::path m_path;
  boost::interprocess::file_mapping m_file;
  boost::interprocess::mapped_region m_region;
};


template <typename T> class MappedObject
{
  struct PrivateDummy {};

public:
  static std::shared_ptr<MappedObject> create(
    const boost::filesystem::path path,
    std::size_t extra_size = 0)
  {
    std::shared_ptr<MappedObject> obj;
    try
    {
      obj = std::make_shared<MappedObject>(
        MappedFile::create(
          path,
          sizeof(T) + extra_size
        ),
        PrivateDummy{}
      );
    }
    catch(boost::interprocess::interprocess_exception &)
    {
      return nullptr;
    }

    auto data = obj->m_file.data();
    auto remaining_data = gsl::span<char>(data.begin() + sizeof(T), data.end());
    
    new (obj->m_data) T(remaining_data);
    return obj;
  }

  static std::shared_ptr<MappedObject> open(
    const boost::filesystem::path path)
  {
    try
    {
      return std::make_shared<MappedObject>(
        MappedFile::open(path),
        PrivateDummy{}
      );
    }
    catch(boost::interprocess::interprocess_exception &)
    {
      return nullptr;
    }
  }

  T &get()
  {
    return *m_data;
  }

  MappedObject(MappedFile path, PrivateDummy)
  : m_file(std::move(path))
  , m_data(reinterpret_cast<T*>(m_file.data().data()))
  {
  }

private:
  MappedFile m_file;
  T *m_data;
};

class Header
{
public:
  Header(gsl::span<char>)
  : m_active_file(0)
  {
  }

  std::size_t active_file() const { return m_active_file.load(); }
  
  void next_active_file()
  {
    ++m_active_file;
  }
  std::atomic<std::size_t> m_active_file;
};

class DataFile
{
public:
  DataFile(
    gsl::span<char> data
  )
  : m_allocator(data)
  , m_stacks(&m_allocator)
  , m_types(&m_allocator)
  , m_events(&m_allocator)
  {
  }

  AllocatableBlock m_allocator;
  StackList m_stacks;
  TypeList m_types;
  EventManager m_events;
};

}
}
