#include "appulatrix/Stack.h"

#ifdef _WIN32
# include "appulatrix/StackProviders/WindowsProvider.h"
#endif

#ifndef _WIN32
# include "appulatrix/StackProviders/LibUnwindProvider.h"
#endif

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>

namespace appulatrix
{

#ifdef _WIN32
WindowsProvider<32> g_hash_stack_provider;
WindowsProvider<32> &g_string_stack_provider = g_hash_stack_provider;
#elif defined(__APPLE__)
LightLibUnwindProvider<32> g_hash_stack_provider;
LibUnwindProvider g_string_stack_provider;
#else
LightLibUnwindProvider<32> g_hash_stack_provider;
LibUnwindProvider g_string_stack_provider;
#endif
  
template <typename Prov, typename Stk>
  typename Prov::Container &get_container(
    Prov &,
    Stk &stack)
{
  static_assert(
    sizeof(typename Prov::Container) <= sizeof(typename Stk::Data),
    "container must fit in stack");
  
  return *(typename Prov::Container*)stack.data();
}

template <typename Prov, typename Stk>
  const typename Prov::Container &get_container(
    Prov &,
    const Stk &stack)
{
  static_assert(
    sizeof(typename Prov::Container) <= sizeof(typename Stk::Data),
    "container must fit in stack");
  return *(const typename Prov::Container*)stack.data();
}

}

std::size_t std::hash<appulatrix::Stack>::operator()(const appulatrix::Stack &a) const
{
  return appulatrix::g_hash_stack_provider.hash(
    appulatrix::get_container(appulatrix::g_hash_stack_provider, a)
  );
}


namespace appulatrix
{
  
Stack Stack::current_stack_thread(std::size_t skip)
{
  skip += 1;

  Stack stack;
  g_hash_stack_provider.fill_from_current(
    get_container(g_hash_stack_provider, stack),
    skip
  );
  return stack;
}
  
appulatrix::FormattedStackContainer Stack::format() const
{
  appulatrix::FormattedStackContainer stack;
  g_hash_stack_provider.format(
    get_container(g_hash_stack_provider, *this),
    stack
  );

  return stack;
}

bool Stack::operator==(const Stack &oth) const
{
  return g_hash_stack_provider.equal(
    get_container(g_hash_stack_provider, *this),
    get_container(g_hash_stack_provider, oth)
  );
}

DetailedStack DetailedStack::current_stack_thread(std::size_t skip)
{
  skip += 1;
  
  std::remove_reference<decltype(g_string_stack_provider)>::type::Container stack;
  
  g_string_stack_provider.fill_from_current(
    stack,
    skip
  );
  
  DetailedStack detailed_stack;
  g_string_stack_provider.format(
    stack,
    detailed_stack.m_stack
  );
  
  return detailed_stack;
}
  
DetailedStack DetailedStack::from_stack_string(const std::string &stack)
{
  DetailedStack detailed_stack;
  detailed_stack.m_stack = FormattedStackContainer::from_string(stack);
  
  return detailed_stack;
}

StackId StackHash::add(const Stack &stk)
{
  StackId index = StackId(m_stacks.size());
  m_stacks[stk] = index;
  return index;
}

boost::optional<StackId> StackHash::find(const Stack &stk)
{
  auto it = m_stacks.find(stk);
  if (it == m_stacks.end())
  {
    return boost::none;
  }

  return it->second;
}

}
