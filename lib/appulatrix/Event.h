#pragma once

#include "appulatrix_export.h"
#include "appulatrix/InternalPointer.h"
#include "appulatrix/Time.h"
#include "appulatrix/types.h"

namespace appulatrix {

class TypeBuilder;

/// A piece of data, emitted at a given
class APPULATRIX_EXPORT Event
{
public:

  Event(
    Time time,
    const void *data,
    StackId stack,
    TypeId type,
    EventProviderId provider)
  : time(time)
  , data(data)
  , stack(stack)
  , type(type)
  , provider(provider)
  {
  }

  /// When did the event occur.
  Time time;
  /// User data payload
  InternalPointer<const void> data;
  /// Stack for event location;
  StackId stack;
  /// What is the data type of the [data] field.
  TypeId type;
  /// Which provider emitted the event
  EventProviderId provider;
};

}
