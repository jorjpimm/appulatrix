#pragma once

#include "appulatrix_export.h"
#include "appulatrix/String.h"
#include "appulatrix/TypeManager.h"
#include "appulatrix/types.h"

#include <cstdint>


/// Type builders for default types
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::uint8_t)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::uint16_t)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::uint32_t)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::uint64_t)

DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::int8_t)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::int16_t)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::int32_t)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, std::int64_t)

DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, bool)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, char)

DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, float)
DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, double)

DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, void *)

DECLARE_TYPE_BUILDER(APPULATRIX_EXPORT, String)

namespace appulatrix
{
namespace BuiltInTypes
{

/// Indices for types which are always assgined a given id
enum class Indices : TypeId
{
  EventProvider,
  EventBlockBegin,
  EventBlockEnd,
  
  FirstUserType,
};

inline bool operator==(Indices a, TypeId b)
{
  return (TypeId)a == b;
}

inline bool operator==(TypeId a, Indices b)
{
  return (TypeId)b == a;
}

}
}
