#pragma once

#include "appulatrix_export.h"
#include "appulatrix/List.h"
#include "appulatrix/InternalPointer.h"
#include "appulatrix/Stack.h"
#include "appulatrix/String.h"

#include <mutex>
#include <unordered_map>
#include <thread>

#if defined(NDEBUG) || defined(__APPLE__)
# define DUPLICATE_STACK_CHECKING 1
#else
# define DUPLICATE_STACK_CHECKING 1
#endif

namespace appulatrix {

class AllocatableBlock;
  
struct StackRef
{
  StackRef(
    StackId id,
    const char *string,
    AllocatableBlock &allocator)
  : index(id)
  , stack_string(string, allocator)
  {
  }
  
  StackId index;
  String stack_string;
};
  
/// Manager a list of stacks, appending new stacks as required
class APPULATRIX_EXPORT StackList
{
public:
  /// Create a new stack manager, all data created is stored in allocatable.
  StackList(AllocatableBlock *allocatable);
  
  const List<StackRef> &stacks() const { return *m_stacks; }
  
  /// Add a new stack
  const StackRef *add_stack(StackId index, const std::string &stack);
  
private:
  InternalPointer<AllocatableBlock> m_allocator;
  InternalPointer<List<StackRef>> m_stacks;
};
  
/// Manager a list of stacks, appending new stacks as required
class APPULATRIX_EXPORT StackManager
{
public:
  StackManager();
  
  void set_current_stack_list(StackList &list);
  const StackList &current_stack_list() const
  {
    assert(m_current_stack_list);
    return *m_current_stack_list;
  }
  
  /// Get a stack for the current threads location, or find a link to a previously created stack
  bool locked_by_current_thread() const;
  StackId get_stack(std::size_t skip = 0);
  
  std::size_t min_allocation_size() const;
  
private:
  /// Add a new stack
  StackId store_stack(
    const Stack &stack,
    const std::string &detailed);
  
  std::mutex m_stack_mutex;
  std::thread::id m_current_thread;
  StackHash m_stacks;
  std::unordered_map<std::string, StackId> m_stack_strings;
  StackList *m_current_stack_list;
};

}
