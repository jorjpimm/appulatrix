#include "appulatrix/StackManager.h"
#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/List.h"
#include "appulatrix/StackProviders/Utils.h"

#include <iostream>

namespace appulatrix
{
  
StackList::StackList(AllocatableBlock *allocatable)
: m_allocator(allocatable)
, m_stacks(m_allocator->create<List<StackRef>>(m_allocator.get()))
{
}

const StackRef *StackList::add_stack(StackId index, const std::string &stack)
{
  auto val = m_stacks->emplace_back(
    index,
    stack.c_str(),
    *m_allocator
  );
  assert(val);
  return val;
}
  
StackManager::StackManager()
: m_current_thread{}
, m_current_stack_list(nullptr)
{
}

void StackManager::set_current_stack_list(appulatrix::StackList &list)
{
  m_current_stack_list = &list;
}

bool StackManager::locked_by_current_thread() const
{
  return m_current_thread == std::this_thread::get_id();
}
  
StackId StackManager::get_stack(std::size_t skip)
{ 
  auto stack = Stack::current_stack_thread(skip + 1);
  
  std::lock_guard<std::mutex> l(m_stack_mutex);
  m_current_thread = std::this_thread::get_id();
  auto fin = gsl::finally([&]{ m_current_thread = std::thread::id{}; });
  if (auto found = m_stacks.find(stack))
  {
    return *found;
  }
  
  auto detailed_stack = DetailedStack::current_stack_thread(skip + 1).to_string();
  auto value = store_stack(stack, detailed_stack);
  
#if DUPLICATE_STACK_CHECKING
  if (detailed_stack.size() && m_stack_strings.find(detailed_stack) != m_stack_strings.end())
  {
    std::cerr << "Duplicate stack with non-identical stack pointers" << std::endl;
    std::cerr << stack.format().to_string() << std::endl;
    std::cerr << detailed_stack << detailed_stack.size() << std::endl;
    std::terminate();
  }
  m_stack_strings[detailed_stack] = value;
#endif

  return value;
}
  
std::size_t StackManager::min_allocation_size() const
{
  const std::size_t stack_length = 128;
  return 32 * stack_length;
}

StackId StackManager::store_stack(
  const Stack &stack,
  const std::string &detailed)
{
  auto index = m_stacks.add(stack);
  m_current_stack_list->add_stack(index, detailed);
  return index;
}

}
