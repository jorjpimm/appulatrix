#pragma once

#include "appulatrix_export.h"
#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/EventManager.h"
#include "appulatrix/StackManager.h"
#include "appulatrix/TypeManager.h"

#include <boost/filesystem/path.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/optional/optional.hpp>

#include <array>
#include <cstddef>
#include <mutex>
#include <gsl/gsl-lite.h>

namespace appulatrix {

class Event;

namespace detail {
class Header;
class MappedFile;
template <typename T> class MappedObject;
class DataFile;
}

/// Utility to log recorded events to file
class APPULATRIX_EXPORT Logger
{
public:
  static const std::size_t megabyte = 1000 * 1000;
  Logger(
    const boost::filesystem::path &path,
    std::size_t starting_file_size = megabyte,
    std::size_t max_file_size = megabyte,
    double growth_factor = 1.0
  );

  void next_active_file();
  void verify_file_space();

  /// Find the type manager for this logger.
  TypeManager &type_manager() { return m_type_manager; }

  /// Find the stack manager.
  StackManager &stack_manager() { return m_stack_manager; }

  /// Find the current event manager.
  EventManager &active_event_manager();

  std::size_t logged_event_count() const;

private:
  void create_active_file();

  StackManager m_stack_manager;
  TypeManager m_type_manager;


  boost::filesystem::path m_path;
  std::shared_ptr<detail::MappedObject<detail::Header>> m_header;
  std::shared_ptr<detail::MappedObject<detail::DataFile>> m_active_file;
  std::vector<std::shared_ptr<detail::MappedObject<detail::DataFile>>> m_old_files;
  std::mutex m_active_file_mutex;

  std::size_t m_logged_event_count_previous_files;
  std::size_t m_min_allocation_size;
  std::size_t m_next_file_size;
  std::size_t m_max_file_size;
  double m_growth_factor;
};

}
