#pragma once

#include "appulatrix/StackProviders/PointerProvider.h"
#include "appulatrix/StackProviders/Utils.h"

#include <cxxabi.h>
#include <cstdlib>
#include <execinfo.h>
#include <memory>
#include <sstream>
#include <string>

#include <iostream>

namespace appulatrix {

template <std::size_t EntryCount> class BacktraceProvider : public PointerProvider<EntryCount>
{
public:
  using Container = typename PointerProvider<EntryCount>::Container;
  
  /// Capture the current stack trace
  bool fill_from_current(Container &container, std::size_t skip)
  {
    skip += 1;
    container.size = backtrace((void**)container.contents.data(), container.contents.size());
    if (container.size <= 0 || container.size > container.contents.size())
    {
      return false;
    }
    
    // Budge up all the data to discard skip
    container.hash = 0;
    for (std::size_t i = 0; i < container.size; ++i)
    {
      container.contents[i] = container.contents[i + skip];
      this->hash_combine(container.hash, container.contents[i]);
    }

    FormattedStackContainer output;
    format(container, output);
    std::cout << output.to_string() << std::endl << std::endl;
    

    return true;
  }
  
  void format(const Container &container, FormattedStackContainer &output)
  {
    auto symbols = backtrace_symbols(container);

    for (std::size_t i = 0; i < container.size; ++i)
    {
      output.add_stack_entry(
        (std::uint64_t)container.contents[i],
        boost::none,
        symbols.get()[i],
        boost::none,
        boost::none
      );
    }
  }

protected:
  std::unique_ptr<char *, void (*)(void *)> backtrace_symbols(const Container &container)
  {
    std::unique_ptr<char *, void (*)(void *)> strs(
      ::backtrace_symbols(const_cast<void **>(container.contents.data()), container.size),
      std::free
    );

    return strs;
  }
};

}
