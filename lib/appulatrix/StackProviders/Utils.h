#pragma once

#include <boost/algorithm/string.hpp>
#include <boost/optional.hpp>
#include <gsl/gsl-lite.h>

#include <cstdio>

namespace appulatrix
{

class FormattedStackContainer
{
public:
  static const char Separator = '#';

  struct StackEntry
  {
    std::string pc;
    boost::optional<std::string> offset;

    boost::optional<std::string> symbol_name;

    boost::optional<std::string> filename;
    boost::optional<std::string> line_number;
  };

  static FormattedStackContainer from_string(const std::string &str)
  {
    FormattedStackContainer stack;
    stack.m_stack = str;
    return stack;
  }

  std::vector<StackEntry> entries() const
  {
    const char separators[] = { Separator, '\0' };

    std::vector<std::string> rows;
    boost::split(rows, m_stack, boost::is_any_of("\n"));


    std::vector<StackEntry> output;
    std::vector<std::string> elements;
    for (auto &row : rows)
    {
      StackEntry entry;
      boost::split(elements, row, boost::is_any_of(separators));
      if (elements.size() != 5)
      {
        continue;
      }

      entry.pc = elements[0];
      if (!elements[1].empty())
      {
        entry.offset = elements[1];
      }

      if (!elements[2].empty())
      {
        entry.symbol_name = elements[2];
      }

      if (!elements[3].empty())
      {
        entry.filename = elements[3];
      }

      if (!elements[4].empty())
      {
        entry.line_number = elements[4];
      }

      output.emplace_back(std::move(entry));
    }

    return output;
  }

  void add_stack_entry(
    std::uint64_t pc,
    const boost::optional<std::uint64_t> &offset,
    const boost::optional<gsl::czstring> &symbol_name,
    const boost::optional<gsl::czstring> &filename,
    const boost::optional<std::uint64_t> &line_number)
  {
    char buffer[64];
    auto print_to_buffer = [&](std::uint64_t val) -> const char *
    {
        std::snprintf(buffer, sizeof(buffer), "%llx", (long long unsigned int)val);
        return buffer;
    };

    m_stack += print_to_buffer(pc);

    m_stack += Separator;
    if (offset)
    {
      m_stack += print_to_buffer(*offset);
    }

    m_stack += Separator;
    if (symbol_name)
    {
      m_stack += *symbol_name;
    }

    m_stack += Separator;
    if (filename)
    {
      m_stack += *filename;
    }

    m_stack += Separator;
    if (line_number)
    {
      m_stack += print_to_buffer(*line_number);
    }

    m_stack += '\n';
  }

  const std::string &to_string() const
  {
    return m_stack;
  }

private:
  std::string m_stack;
};
}
