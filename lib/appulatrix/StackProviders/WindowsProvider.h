#pragma once

#include "appulatrix/StackProviders/PointerProvider.h"
#include "appulatrix/StackProviders/Utils.h"

#pragma warning(push)
#pragma warning(disable: 4091)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
# include <Windows.h>
# include <DbgHelp.h>
#pragma warning(pop)

#include <boost/filesystem/path.hpp>

#include <iomanip>

namespace appulatrix {

template <std::size_t EntryCount> class WindowsProvider : public PointerProvider<EntryCount>
{
public:
  WindowsProvider()
  : m_process(::GetCurrentProcess())
  {
    ::SymSetOptions(SYMOPT_LOAD_LINES);
    ::SymInitialize(m_process, 0, true);
  }

  ~WindowsProvider()
  {
    ::SymCleanup(m_process);
  }

  /// Capture the current stack trace
  bool fill_from_current(Container &container, std::size_t skip)
  {
    std::size_t position = skip + 1;

    ULONG hash = 0;
    container.size = ::CaptureStackBackTrace(
      (ULONG)position,
      (ULONG)EntryCount,
      const_cast<void **>(container.contents.data()),
      &hash
    );
    container.hash = hash;

    if (container.size <= 0 || container.size > container.contents.size())
    {
      return false;
    }

    return true;
  }

  void format(const Container &container, FormattedStackContainer &output)
  {
    for (std::size_t i = 0; i < container.size; ++i)
    {
      get_symbol_data(output, container.contents[i]);
    }
  }

private:
  void get_symbol_data(
    FormattedStackContainer &output,
    const void *symbol_location)
  {
    const std::size_t max_name_size = 256;
    BYTE *buffer = (BYTE *)alloca(sizeof(SYMBOL_INFO) + max_name_size);
    memset(buffer, 0, sizeof(buffer));

    auto symbol_location_64 = reinterpret_cast<std::uint64_t>(symbol_location);

    PSYMBOL_INFO symbol = (PSYMBOL_INFO)buffer;
    symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    symbol->MaxNameLen = (ULONG)max_name_size;

    std::uint64_t offset = 0;
    if (!::SymFromAddr(m_process,
                     symbol_location_64,
                     &offset,
                     symbol))
    {
      symbol->Name[0] = 0;
      offset = 0;
    }
    
    boost::optional<boost::filesystem::path> file;
    boost::optional<std::uint64_t> line;
    DWORD line_offset = 0;
    IMAGEHLP_LINE64 line_data;
    line_data.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
    if (SymGetLineFromAddr64(m_process, symbol_location_64, &line_offset, &line_data))
    {
      file = line_data.FileName;
      line = line_data.LineNumber;
    }
    
    symbol->Name[max_name_size-1] = '\0';
    output.add_stack_entry(
      (std::uint64_t)symbol_location,
      offset,
      symbol->Name,
      file ? boost::optional<gsl::czstring>(file->string().data()) : boost::none,
      line
    );
  }

  HANDLE m_process;
};

}
