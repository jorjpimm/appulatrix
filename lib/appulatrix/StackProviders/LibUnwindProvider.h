#pragma once

#include "appulatrix/StackProviders/PointerProvider.h"
#include "appulatrix/StackProviders/Utils.h"

#include <cxxabi.h>
#include <iomanip>
#include <libunwind.h>
#include <sstream>

namespace appulatrix {

template <std::size_t EntryCount> class LightLibUnwindProvider : public PointerProvider<EntryCount>
{
public:
  using Container = typename PointerProvider<EntryCount>::Container;
  
  /// Capture the current stack trace
  bool fill_from_current(Container &container, std::size_t skip)
  {
    unw_context_t context;
    unw_getcontext(&context);
    
    unw_cursor_t cursor;
    unw_init_local(&cursor, &context);

    container.size = 0;
    container.hash = 0;
    // Unwind frames one by one, going up the frame stack.
    while (unw_step(&cursor) > 0 && container.size < container.contents.size())
    {
      if (skip)
      {
        --skip;
        continue;
      }
      
      unw_word_t pc;
      unw_get_reg(&cursor, UNW_REG_IP, &pc);
      if (pc == 0) {
        break;
      }
      container.contents[container.size] = (void *)(uintptr_t)pc;
      this->hash_combine(container.hash, container.contents[container.size]);
      ++container.size;
    }

    return true;
  }
  
  void format(const Container &container, FormattedStackContainer &output)
  {
    for (std::size_t i = 0; i < container.size; ++i)
    {
      output.add_stack_entry(
       (std::uint64_t)container.contents[i],
       boost::none,
       boost::none,
       boost::none,
       boost::none
     );
    }
  }
};

class LibUnwindProvider
{
public:
  struct Container 
  {
    unw_context_t context;
    std::size_t skip;
    std::size_t hash;
  };

  /// Capture the current stack trace
  bool fill_from_current(Container &container, std::size_t skip)
  {
    unw_getcontext(&container.context);
    container.skip = skip;
    return true;
  }

  void format(const Container &container, FormattedStackContainer &output)
  {
    unw_cursor_t cursor;
    unw_init_local(&cursor, (unw_context_t *)&container.context);

    // Unwind frames one by one, going up the frame stack.
    std::size_t skip = container.skip;
    while (unw_step(&cursor) > 0)
    {
      if (skip)
      {
        --skip;
        continue;
      }
      
      unw_word_t offset, pc;
      unw_get_reg(&cursor, UNW_REG_IP, &pc);
      if (pc == 0) {
        break;
      }

      std::unique_ptr<char, void (*)(void *)> demangled_name(nullptr, std::free);
      char sym[256];
      boost::optional<gsl::czstring> sym_value;
      if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0)
      {
        sym_value = sym;

        demangled_name = get_demangled_name(sym);
        if (demangled_name)
        {
          sym_value = demangled_name.get();
        }
      }

      output.add_stack_entry(
        pc,
        offset,
        sym_value,
        boost::none,
        boost::none
      );
    }
  }
  
  std::unique_ptr<char, void (*)(void *)> get_demangled_name(const char *sym)
  {
    std::size_t demangled_len = 128;
    int status = 0;
    auto symbol_demangled = (char *)malloc(demangled_len);
    symbol_demangled = abi::__cxa_demangle(sym, symbol_demangled, &demangled_len, &status);
    if (status == 0)
    {
      return std::unique_ptr<char, void (*)(void *)>(symbol_demangled, std::free);
    }
    
    std::free(symbol_demangled);
    return std::unique_ptr<char, void (*)(void *)>(nullptr, std::free);
  }
};

}
