#pragma once

#include <cstddef>
#include <functional>

namespace appulatrix {

template <std::size_t EntryCount> class PointerProvider
{
public:
  struct Container 
  {
    std::size_t size;
    std::array<const void *, EntryCount> contents;
    std::size_t hash;
  };

  bool equal(const Container &a, const Container &b)
  {
    if (a.size != b.size)
    {
      return false;
    }

    for (std::size_t i = 0; i < a.size; ++i)
    {
      if (a.contents[i] != b.contents[i])
      {
        return false;
      }
    }
    
    return true;
  }

  std::size_t hash(const Container &cont)
  {
    return cont.hash;
  }

protected:
  template <class T> inline void hash_combine(std::size_t& seed, const T& v)
  {
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
  }
};

}
