#pragma once

#include "appulatrix_export.h"
#include "appulatrix/List.h"
#include "appulatrix/InternalPointer.h"
#include "appulatrix/Type.h"
#include "appulatrix/TypeLookup.h"

#include <iostream>
#include <mutex>

namespace appulatrix {

class AllocatableBlock;
class TypeBuilder;

/// Manager a list of types, appending new events as required
class APPULATRIX_EXPORT TypeList
{
public:
  /// Create a new type manager, all data created is stored in allocatable.
  TypeList(AllocatableBlock *allocatable);

  const List<Type> &types() const { return *m_types; }

  /// Add a new type
  const Type *add_type(const TypeId index, const TypeBuilder &type);

  std::size_t min_allocation_size() const { return m_types->min_allocation_size(); }

private:
  InternalPointer<AllocatableBlock> m_allocator;
  InternalPointer<List<Type>> m_types;
};

/// Manager a list of types, appending new events as required
class APPULATRIX_EXPORT TypeManager : public TypeLookup
{
public:
  TypeManager();
  ~TypeManager();

  void set_current_type_list(TypeList &list);
  const TypeList &current_type_list() const { assert(m_current_type_list); return *m_current_type_list; }

  /// Add a new type
  const Type *add_type(const TypeBuilder &type, std::function<void()> const& reset);
  
  using TypePtr = const Type *;
  gsl::span<const Type * const> type_list() const override { return m_types; }
  const Type &find_type(Type::Id search_id) const override;

  template <typename T> const Type &get_type()
  {
    auto idx = Type::find_index<T>(*this);
    return find_type(idx);
  }

  std::size_t min_allocation_size() const { return m_current_type_list->min_allocation_size(); }

  /// Get the mutex responsible for locking around creation of type data.
  std::recursive_mutex &type_creation_mutex() { return m_type_creation_mutex; }

private:
  Type::Id m_next_type_id;
  std::size_t m_type_count;
  std::vector<const Type *> m_types;
  std::vector<const Type *> m_types_by_id;
  std::vector<std::function<void()>> m_reset;
  std::recursive_mutex m_type_creation_mutex;

  TypeList *m_current_type_list;
};

/// Specialised for each serialisable type
template <typename T> class TypeFinder;

template <typename T> class TypeFinder<const T *> : public TypeFinder<T *> {};
template <typename T> class TypeFinder<const T &> : public TypeFinder<T &> {};
template <typename T> class TypeFinder<const T> : public TypeFinder<T> {};

/// Base class with registration utilities
template <typename T> struct TypeFinderBase
{
  static const appulatrix::Type::Id get_index(TypeManager &manager)
  {
    auto &d = data();
    if (!d)
    {
      std::lock_guard<std::recursive_mutex> l(manager.type_creation_mutex());
      if (!d)
      {
        auto reset = [&]{ d = boost::none; };
        d.emplace(
          manager.add_type(TypeFinder<T>::create(manager), reset)->id(),
          &manager
        );
      }
    }
    assert(d);
    assert(&manager == d->m_manager);
    assert(d->m_built_index != Type::invalid_index());
    return d->m_built_index;
  }

  struct Data
  {
    Data(Type::Id index, const TypeManager *manager)
    : m_built_index(index)
    , m_manager(manager)
    {
    }

    Type::Id m_built_index;
    const TypeManager *m_manager;
  };

private:
  static boost::optional<Data> &data()
  {
    static boost::optional<Data> d;
    return d;
  }
};

#define DECLARE_TYPE_BUILDER(export_decl, name) \
  namespace appulatrix { \
  template <> class export_decl TypeFinder<name> : public TypeFinderBase<name> { public:\
    static appulatrix::TypeBuilder create(TypeManager &manager); \
    static void fill_description(appulatrix::TypeBuilder &type); \
  }; }


#define IMPLEMENT_TYPE_BUILDER(name, type_name) namespace appulatrix { \
  TypeBuilder TypeFinder<name>::create(TypeManager &manager) { \
    TypeBuilder builder(#name, sizeof(name), Type::DataType::Unknown, boost::none, manager);\
    fill_description(builder); \
    return builder; } } \
  void appulatrix::TypeFinder<name>::fill_description(appulatrix::TypeBuilder &type_name)

#define IMPLEMENT_TYPE_BUILDER_BUILTIN(name, type_name, id) namespace appulatrix { \
  TypeBuilder TypeFinder<name>::create(TypeManager &manager) { \
    TypeBuilder builder(#name, sizeof(name), Type::DataType::Unknown, TypeId(appulatrix::BuiltInTypes::Indices::id), manager);\
    fill_description(builder); \
    return builder; } } \
  void appulatrix::TypeFinder<name>::fill_description(appulatrix::TypeBuilder &type_name)


}
