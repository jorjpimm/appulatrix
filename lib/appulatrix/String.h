#pragma once

#include "appulatrix_export.h"
#include "appulatrix/InternalPointer.h"
#include "appulatrix/types.h"

#include <cstring>
#include <ostream>
#include <string>

namespace appulatrix
{

class AllocatableBlock;

class APPULATRIX_EXPORT String
{
public:
  String(const char *data, std::size_t size, AllocatableBlock &allocator)
  {
    build(data, size, allocator);
  }
  
  String(const char *data, AllocatableBlock &allocator)
  {
    build(data, allocator);
  }
  
  String(const std::string &data, AllocatableBlock &allocator)
  {
    build(data, allocator);
  }
  
  const char *get() const { return m_data.get(); }
  
	void build(const char *data, std::size_t size, AllocatableBlock &allocator);
	void build(const char *data, AllocatableBlock &allocator)
  {
    build(data, std::strlen(data), allocator);
  }
  
  void build(const std::string &data, AllocatableBlock &allocator)
  {
    build(data.data(), data.size(), allocator);
  }
  
  bool operator==(const char *oth) const
  {
		return std::strcmp(m_data.get(), oth) == 0;
  }
  
  bool operator!=(const char *oth) const
  {
		return !(*this == oth);
  }
  
  bool operator==(const std::string &oth) const
  {
		return oth == m_data.get();
  }
  
  bool operator!=(const std::string &oth) const
  {
		return !(*this == oth);
  }

private:
  InternalPointer<char> m_data;
  template <typename T> friend class TypeFinder;
};

inline std::ostream &operator<<(std::ostream &str, const String &val)
{
  str << val.get();
  return str;
}

}
