#pragma once

#include "appulatrix_export.h"
#include "appulatrix/InternalPointer.h"
#include "appulatrix/String.h"
#include "appulatrix/types.h"


#include <boost/optional/optional.hpp>
#include <boost/variant.hpp>
#include <gsl/gsl-lite.h>

namespace appulatrix {

class AllocatableBlock;
class Type;
class TypeBuilder;
template <typename T> class TypeFinder;
class TypeLookup;
class TypeManager;

/// A type describes how the bytes in a piece
/// of memory relate to a user structure.
class APPULATRIX_EXPORT Type
{
public:
  using Id = TypeId;
  using FieldIndex = std::uint16_t;
  using FieldOffset = std::uint32_t;

  /// An individual field in a structure.
  class APPULATRIX_EXPORT Field
  {
  public:
    using FlagsUnion = std::uint8_t;
    enum class Flag : FlagsUnion
    {
      Interface = 1
    };
    
    Field(
      const char *name,
      Type::Id type,
      FieldOffset offset,
      FieldIndex parent,
      FlagsUnion flags,
      AllocatableBlock &allocator)
    : m_name(name, allocator)
    , m_offset(offset)
    , m_parent(parent)
    , m_type(type)
    , m_flags(flags)
    {
    }

    /// Find the fields descriptive name
    const String &name() const { return m_name; }

    /// Find the fields type
    Type::Id type() const { return m_type; }

    /// Find the fields offset in bytes from the parent type
    FieldOffset offset() const { return m_offset; }

    /// Find the fields logical parent in the type
    FieldIndex parent() const { return m_parent; }

    /// Find the flags set for this field
    FlagsUnion flags() const { return m_flags; }

    /// Find if a flag is set
    bool has_flag(Flag flg) const { return (m_flags&FlagsUnion(flg)) != 0; }

    /// Find a pointer to the field data from a pointer to the parent type.
    const void *get_data_ptr(const void *parent) const;

    /// Dump the contents of the given field [data] into a stream.
    /// \note expects [data] to be of this field type.
    void dump(std::ostream &str, TypeLookup &lookup, const void *data, std::size_t indent = 0) const;

    /// The index for fields which have no parent (parented to root)
    static FieldIndex null_parent() { return std::numeric_limits<FieldIndex>::max(); }

  private:
    String m_name;
    FieldOffset m_offset;
    FieldIndex m_parent;
    Type::Id m_type;
    FlagsUnion m_flags;
  };

  using DataVariant = boost::variant<
    std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t,
    std::int8_t, std::int16_t, std::int32_t, std::int64_t,
    float, double, bool
    >;

  enum class DataType : std::uint8_t
  {
    Unknown,
    Boolean,
    UnsignedInteger,
    Integer,
    Real
  };

  /// Find a Type index for the type [T]
  template <typename T> static const TypeId find_index(TypeManager &manager)
  {
    return TypeFinder<T>::get_index(manager);
  }

  /// Create a new type, from given parameters.
  /// \sa Type::build
  Type(
    Id index,
    const TypeBuilder &builder,
    AllocatableBlock &allocator);

  /// Find the name of the structure.
  const String &name() const { return m_name; }

  /// Find contents of the type.
  DataType data_type() const { return m_data_type; }

  /// Find the size of the structure in bytes.
  std::size_t size() const { return m_size; }

  /// Find the assigned unique index of this type
  Id id() const { return m_id; }

  /// Find the fields inside the structure
  const gsl::span<const Field> fields() const
  {
    return gsl::span<const Field>(m_fields.get(), m_field_count);
  }

  /// Get the field information for a child field
  boost::optional<const Field &> find_field(const char *str) const;
  boost::optional<const Field &> find_field(const std::string &str) const;

  /// Get the value of a primitive field
  boost::optional<DataVariant> get_field_value(TypeLookup &manager, const char *str, const void *data) const;
  boost::optional<DataVariant> get_field_value(TypeLookup &manager, const std::string &str, const void *data) const;

  template <typename T>T get_field_value(TypeLookup &manager, const char *str, const void *data) const
  {
    auto val = get_field_value(manager, str, data);
    if (!val)
    {
      throw std::runtime_error(std::string("Unknown field ") + str);
    }

    return boost::get<T>(*val);
  }

  /// Find an interface of type [type] internal to this given type
  /// This will either be one of the top level fields, or, the [data] 
  /// object itself, depending on the types of [data] (or none).
  boost::optional<const void *> find_interface(const void *data, TypeId type) const;
  

  /// Get the data for the given type as a variant of the correct type.
  /// \note for DataType::Unknown types, nothing is returned.
  boost::optional<DataVariant> get_value(const void *data) const;

  /// Dump the contents of the given [data] into a stream.
  /// \note expects [data] to be of this type.
  void dump(std::ostream &str, TypeLookup &lookup, const void *data, bool leading_indent = true, std::size_t indent=0) const;

  static Id invalid_index() { return std::numeric_limits<Id>::max(); }

private:
  std::size_t m_size;

  std::size_t m_field_count;
  InternalPointer<Field> m_fields;

  String m_name;

  DataType m_data_type;

  Id m_id;
};

}
