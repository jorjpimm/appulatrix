#pragma once

#include "appulatrix/AllocatableBlock.h"

#include <array>
#include <cstddef>

namespace appulatrix {

namespace detail {

/// A Fixed size list of T objects, with a pointer to the next fixed list.
template <typename T, std::size_t FixedSize> class LinkNode
{
public:
  LinkNode()
  : m_reserved_size(0)
  , m_complete_size(0)
  {
  }

  template <typename... Args> bool emplace_back(const T **output, Args &&...args)
  {
    std::uint32_t current_value = m_reserved_size;
    while(true)
    {
      if (current_value >= capacity())
      {
        return false;
      }

      if (m_reserved_size.compare_exchange_weak(current_value, current_value + 1))
      {
        break;
      }
    }

    auto location = m_contents.data() + current_value;
    *output = new(location) T(std::forward<Args>(args)...);

    std::uint32_t required_value = current_value;
    while(!m_complete_size.compare_exchange_weak(required_value, current_value + 1))
    {
      required_value = current_value;
    }

    assert(*output);
    return true;
  }

  /// Set the next value of the node - only works when there is not an existing next
  bool set_next(LinkNode<T, FixedSize> *next)
  {
    LinkNode *null_expected = nullptr;
    return m_next.compare_exchange_strong(null_expected, next);
  }
  const LinkNode<T, FixedSize> *next() const { return m_next.get(); }
  LinkNode<T, FixedSize> *next() { return m_next.get(); }

  const T &operator[](std::size_t i) const
  {
    return at(i);
  }

  const T &at(std::size_t i) const
  {
    if (i >= m_complete_size)
    {
      throw std::range_error("Invalid index");
    }

    return *reinterpret_cast<const T*>(m_contents.data()+i);
  }

  const T *ptr(std::size_t i) const
  {
    if (i > m_complete_size)
    {
      throw std::range_error("Invalid index");
    }

    return reinterpret_cast<const T*>(m_contents.data()+i);
  }

  std::size_t capacity() const { return FixedSize; }
  std::size_t size() const { return m_complete_size; }
  std::size_t reserved_size() const { return m_reserved_size; }

private:
  using ElementType = typename std::aligned_storage<sizeof(T), alignof(T)>::type;
  std::array<ElementType, FixedSize> m_contents;
  InternalPointer<LinkNode<T, FixedSize>> m_next;
  std::atomic<std::uint32_t> m_reserved_size;
  std::atomic<std::uint32_t> m_complete_size;
};

}

template <typename T, std::size_t FixedSize = 128> class List
{
public:
  using LinkNode = detail::LinkNode<T, FixedSize>;

  class Iterator
  {
  public:
    Iterator()
    : m_node(nullptr)
    , m_index(0)
    {
    }

    Iterator(
      const LinkNode *node,
      std::size_t index)
    : m_node(node)
    , m_index(index)
    {
    }

    bool operator==(const Iterator &oth) const
    {
      return m_node == oth.m_node && m_index == oth.m_index;
    }

    bool operator!=(const Iterator &oth) const
    {
      return !(*this == oth);
    }

    void operator++()
    {
      ++m_index;

      while (m_index >= m_node->size())
      {
        if (!advance_list_node())
        {
          return;
        }
      }
    }

    Iterator &operator+=(std::size_t count)
    {
      m_index += count;

      while (m_index >= m_node->size())
      {
        if (!advance_list_node())
        {
          break;
        }
      }

      return *this;
    }

    Iterator operator+(std::size_t count) const
    {
      auto it = *this;
      for (std::size_t i = 0; i < count && it.m_node; ++i)
      {
        ++it;
      }
      return it;
    }

    const T &operator*() const
    {
      return m_node->at(m_index);
    }

    const T *ptr() const
    {
      return m_node->ptr(m_index);
    }

    const LinkNode *node() const
    {
      return m_node;
    }

    std::size_t index() const
    {
      return m_index;
    }

  private:
    bool advance_list_node()
    {
      if (auto next = m_node->next())
      {
        m_index -= m_node->size();
        m_node = next;
        return true;
      }

      return false;
    }
    const LinkNode *m_node;
    std::size_t m_index;
  };

  List(AllocatableBlock *allocator)
  : m_allocator(allocator)
  , m_size(0)
  {
    create_new_node();

    LinkNode *null_expected = nullptr;
    m_first.compare_exchange_strong(null_expected, m_current.get());
  }

  std::size_t size() const { return m_size; }
  std::size_t min_allocation_size() const { return sizeof(LinkNode); }

  Iterator begin() const { return Iterator(m_first.get(), 0); }
  Iterator end() const
  {
    auto current = m_current.get();
    return Iterator(current, current->size());
  }

  std::size_t get_contiguous_size_from(Iterator it) const
  {
    auto node = it.node();
    return node->size() - it.index();
  }

  const T &operator[](std::size_t i) const
  {
    return at(i);
  }

  const T &at(std::size_t i) const
  {
    std::size_t adjusted_i = i;
    const LinkNode *block = m_first.get();
    while(block && adjusted_i >= FixedSize)
    {
      adjusted_i -= FixedSize;
      block = block->next();
      if (!block)
      {
        throw std::range_error("Invalid index");
      }
    }

    return block->at(adjusted_i);
  }

  template <typename... Args> const T *emplace_back(Args &&...args)
  {
    const T *output = nullptr;
    while(!m_current || !m_current->emplace_back(&output, std::forward<Args>(args)...))
    {
      create_new_node();
    }

    ++m_size;
    assert(output);
    return output;
  }

private:
  void create_new_node()
  {
    if (!m_current || !m_current->next())
    {
      LinkNode *new_last =
        m_allocator->create<LinkNode>();

      {
        LinkNode *null_expected = nullptr;
        assert(new_last);
        if (!m_current.compare_exchange_strong(null_expected, new_last))
        {
          auto current = m_current.get();
          while(true)
          {
            if (!current)
            {
              assert(false);
            }

            if (current->set_next(new_last))
            {
              break;
            }

            current = current->next();
          }
        }
      }
    }

    auto current = m_current.get();
    if (current->reserved_size() >= current->capacity())
    {
      auto current_next = current->next();
      assert(current_next);
      m_current.compare_exchange_strong(current, current_next);
    }
  }

  InternalPointer<AllocatableBlock> m_allocator;
  InternalPointer<LinkNode> m_first;
  InternalPointer<LinkNode> m_current;
  std::atomic<std::size_t> m_size;
};

}
