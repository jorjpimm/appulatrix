#include "appulatrix/AllocatableBlock.h"

#include <atomic>
#include <iostream>

namespace appulatrix 
{

struct AllocatableBlock::Header
{
  Header()
  : next_free(0)
  {
  }

  std::atomic<std::size_t> next_free;
};

AllocatableBlock::AllocatableBlock(gsl::span<char> data, InitialisationMode mode)
{
  reset_location(data);

  if (mode == InitialisationMode::Create)
  {
    new (get_header()) Header();
  }
}

std::size_t AllocatableBlock::allocatable_capacity() const
{
  return get_allocatable_end() - get_allocatable_start();
}

std::size_t AllocatableBlock::allocated_size() const
{
  return (get_allocatable_start() + get_header()->next_free) - get_allocatable_start();
}

std::size_t AllocatableBlock::available_size() const
{
  return allocatable_capacity() - allocated_size();
}

void AllocatableBlock::reset_location(gsl::span<char> data)
{
  if (data.size() <= sizeof(Header))
  {
    throw std::runtime_error("Invalid block size");
  }

  m_data = data.data();
  m_size = data.size();
}

void AllocatableBlock::clear()
{
  Header *header = get_header();

  header->next_free = 0;
}

void *AllocatableBlock::allocate(std::size_t size)
{
  do {
    auto current_allocated = get_header()->next_free.load();
    auto new_next_free = current_allocated + size;
    if (new_next_free > allocatable_capacity())
    {
      throw std::bad_alloc();
    }

    if (get_header()->next_free.compare_exchange_weak(current_allocated, new_next_free))
    {
      return get_allocatable_start() + current_allocated;
    }
  } while(true);
}

std::size_t AllocatableBlock::min_data_size()
{
  return sizeof(Header) + 1;
}

AllocatableBlock::Header *AllocatableBlock::get_header()
{
	return reinterpret_cast<Header*>(m_data.get());
}

const AllocatableBlock::Header *AllocatableBlock::get_header() const
{
	return reinterpret_cast<const Header*>(m_data.get());
}

char *AllocatableBlock::get_allocatable_start()
{
	return reinterpret_cast<char *>(get_header() + 1);
}

char *AllocatableBlock::get_allocatable_end()
{
	return m_data.get() + m_size;
}

const char *AllocatableBlock::get_allocatable_start() const
{
	return reinterpret_cast<const char *>(get_header() + 1);
}

const char *AllocatableBlock::get_allocatable_end() const
{
	return m_data.get() + m_size;
}

}
