#pragma once

#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <jsoncons/json.hpp>

struct Config
{
  Config(const boost::filesystem::path &path)
  {
    if (boost::filesystem::exists(path))
    {
      try
      {
        m_config = jsoncons::json::parse_file(path.string());
      }
      catch (const jsoncons::json_exception &e)
      {
        std::cerr << "Invalid appulatrix config '" << e.what() << "'" << std::endl;
      }
    }
    else
    {
      std::cerr << "No config found at " << boost::filesystem::absolute(path) << std::endl;
    }
  }

  template <typename T> boost::optional<T> get(const char *data) const
  {
    if (m_config.has_member(data))
    {
      auto val = m_config[data];
      return val.as<T>();
    }
    return boost::none;
  }

  template <typename T>T get(const char *data, const T &t) const
  {
    return get<T>(data).get_value_or(t);
  }

private:
  jsoncons::json m_config;
};
