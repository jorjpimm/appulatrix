#ifdef __linux__

#include "AppLogger.h"
#include "MemoryTypes.h"
#include "Wrapper.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <functional>
#include <thread>
#include <unordered_map>

#include "appulatrix/Logger.h"
#include "gsl/gsl-lite.h"

#include <dlfcn.h>

struct LinuxAppLogger;
static LinuxAppLogger *g_data;

struct LinuxAppLogger : public AppLogger
{
  static std::string make_identifier()
  {
    return std::to_string(getpid());
  }

  LinuxAppLogger()
  : AppLogger(make_identifier())
  {
    g_data = this;
  }

  ~LinuxAppLogger()
  {
    assert(g_data == this);
    g_data = nullptr;
  }
};

static std::unique_ptr<LinuxAppLogger> g_data_container(new LinuxAppLogger());

AppLogger &AppLogger::instance() 
{ 
  return *g_data;
}

void *malloc(std::size_t size)
{
  static Wrapper<decltype(malloc)> wrapper(dlsym(RTLD_NEXT, "malloc"));

  auto injector = g_data ? &AppLogger::instance() : nullptr;
  if (!injector)
  {
    return wrapper.call_force_disabled(size);
  }

  auto cb = [&](void *result, std::size_t size)
  {
    injector->on_malloc(result, size);
  };

  return wrapper(*injector, cb, size);
}


void free(void *ptr)
{
  static Wrapper<decltype(free)> wrapper(dlsym(RTLD_NEXT, "free"));

  auto injector = g_data ? &AppLogger::instance() : nullptr;
  if (!injector || ptr == injector)
  {
    return wrapper.call_force_disabled(ptr);
  }

  auto cb = [&](void *result)
  {
    injector->on_free(result);
  };

  return wrapper(*injector, cb, ptr);
}

void *realloc(void *old_ptr, std::size_t size)
{
  static Wrapper<decltype(realloc)> wrapper(dlsym(RTLD_NEXT, "realloc"));

  auto injector = g_data ? &AppLogger::instance() : nullptr;
  if (!injector)
  {
    return wrapper.call_force_disabled(old_ptr, size);
  }

  auto cb = [&](void *new_ptr, void *old_ptr, std::size_t size)
  {
    injector->on_realloc(new_ptr, old_ptr, size);
  };

  return wrapper(*injector, cb, old_ptr, size);
}


#endif
