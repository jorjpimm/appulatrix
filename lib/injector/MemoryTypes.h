#pragma once

#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/TypeBuilder.h"

class Allocation
{
public:
  Allocation(void *ptr, std::uint64_t s)
  : pointer(ptr)
  , size(s)
  {
  }

  const void *pointer;
  std::uint64_t size;
};

class Deallocation
{
public:
  Deallocation(void *ptr)
  : pointer(ptr)
  {
  }

  const void *pointer;
};

class Reallocation
{
public:
  Reallocation(void *old_ptr, void *new_ptr, std::uint64_t size)
  : dealloc(old_ptr)
  , alloc(new_ptr, size)
  {
  }

  Deallocation dealloc;
  Allocation alloc;
};


#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, Allocation)

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, Deallocation)

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, Reallocation)
