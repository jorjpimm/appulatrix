#pragma once

#include "appulatrix_injector_export.h"
#include "appulatrix/EventProvider.h"
#include "appulatrix/Logger.h"
#include "Config.h"
#include "MemoryTypes.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>

#include <csignal>
#include <thread>

#define LOG_ALLOCATION_EVENTS 0

struct APPULATRIX_INJECTOR_EXPORT AppLogger
{
  static const std::size_t SkipCount = 5;
  static const std::int32_t MagicNumber = 0xca4efade;

  boost::filesystem::path make_storage_path(
    const std::string &id)
  {
    boost::filesystem::path container = m_config.get<std::string>(
      "storage_path",
      boost::filesystem::temp_directory_path().string()
    );
    std::string time = boost::posix_time::to_iso_string(boost::posix_time::second_clock::local_time());
    return container / ("appulatrix_" + id + "_" + time);
  }

  AppLogger(const std::string &identifier)
  : m_initialised(0)
  , m_config("appulatrix.config")
  , m_id(identifier)
  , m_logger(make_storage_path(identifier))
  , m_globally_disabled_in_thread_lock(false)
  , m_allocation_provider(appulatrix::EventProvider::create_top_level("allocations"))
  {
    appulatrix::EventProvider::initialise_providers(&m_logger);
    m_allocation_provider.set_enabled(m_config.get("log_allocations", false));
    m_initialised = MagicNumber;
    check_sane("ctor");
    
#if LOG_ALLOCATION_EVENTS
    fprintf(stdout, "Initialised AppLogger\n");
#endif
  }

  ~AppLogger()
  {
    appulatrix::EventProvider::terminate_providers();
    check_sane("dtor");
    m_initialised = 0;
    
#if LOG_ALLOCATION_EVENTS
    fprintf(stdout, "Destroyed AppLogger\n");
#endif

    dump_state();
  }

  void on_malloc(void *ptr, std::size_t size)
  {
#if LOG_ALLOCATION_EVENTS
    fprintf(stdout, "Allocated %p of size %zu at %u\n", ptr, size, unsigned(stack));
#endif

    m_allocation_provider.emit_event_with_skipped_stack<Allocation>(SkipCount, ptr, size);
  }

  void on_free(void *ptr)
  {
#if LOG_ALLOCATION_EVENTS
    fprintf(stdout, "Freed %p at %u\n", ptr, unsigned(stack));
#endif

    m_allocation_provider.emit_event_with_skipped_stack<Deallocation>(SkipCount, ptr);
  }

  void on_realloc(void *old_ptr, void *new_ptr, std::size_t size)
  {
#if LOG_ALLOCATION_EVENTS
    fprintf(stdout, "Reallocating %p to %p of size %zu at %u\n", old_ptr, new_ptr, size, unsigned(stack));
#endif

    m_allocation_provider.emit_event_with_skipped_stack<Reallocation>(SkipCount, old_ptr, new_ptr, size);
  }

  bool disabled() const
  {
    return !m_initialised ||
      m_globally_disabled_in_thread_lock ||
      !m_allocation_provider.is_enabled() ||
      m_allocation_provider.stack_manager().locked_by_current_thread();
  }

  void find_thread_lock(bool **lock_val)
  {
    static thread_local bool s_lock;
    *lock_val = &s_lock;
  }
  
  bool lock_thread()
  {
    bool expected = false;
    while(!m_globally_disabled_in_thread_lock.compare_exchange_weak(expected, true))
    {
      expected = false;
    }
    
    auto fin = gsl::finally([&]() { m_globally_disabled_in_thread_lock = false; });
    
    bool *lock = nullptr;
    find_thread_lock(&lock);
    if (*lock == true)
    {
      return false;
    }
    *lock = true;
    return true;
  }
  
  void unlock_thread()
  {
    bool *lock = nullptr;
    find_thread_lock(&lock);
    *lock = false;
  }

  void check_sane(const char *desc) const
  {
    if (m_initialised != MagicNumber && m_initialised != 0)
    {
      fprintf(stderr, "%s: Shit got insane? %#010x\n", desc, unsigned(m_initialised));
    }
  }

  void dump_state() const
  {
    std::cout 
      << "Recorded " << m_logger.logged_event_count() << " events." 
      << std::endl;
  }

  static AppLogger &instance();

  int m_initialised;

  Config m_config;
  std::string m_id;
  appulatrix::Logger m_logger;
  std::atomic<bool> m_globally_disabled_in_thread_lock;
  appulatrix::EventProvider m_allocation_provider;
};
