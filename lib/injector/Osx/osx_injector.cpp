#ifdef __APPLE__

#include "AppLogger.h"
#include "MemoryTypes.h"
#include "Wrapper.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <functional>
#include <thread>
#include <unordered_map>

#include "appulatrix/Logger.h"
#include "gsl/gsl-lite.h"

#include <mach/mach_init.h>
#include <malloc/malloc.h>

struct OsxAppLogger : public AppLogger
{
  static std::string make_identifier()
  {
    return std::to_string(getpid());
  }

  OsxAppLogger()
  : AppLogger(make_identifier())
  {
    malloc_zone_t *dz = malloc_default_zone();

    m_malloc.set_function((void *)dz->malloc);
    dz->malloc = &malloc;
    
    m_free.set_function((void *)dz->free);
    dz->free = &free;
    
    m_free_definite_size.set_function((void *)dz->free_definite_size);
    dz->free_definite_size = &free_definite_size;
    
    m_realloc.set_function((void *)dz->realloc);
    dz->realloc = &realloc;
  }

  static void *malloc(_malloc_zone_t *zone, std::size_t size)
  {
    auto &inst = instance();
    auto cb = [&](void *ptr, _malloc_zone_t *zone, std::size_t size)
    {
      inst.on_malloc(ptr, size);
    };

    return inst.m_malloc(inst, cb, zone, size);
  }
  
  static void free(_malloc_zone_t *zone, void *ptr)
  {
    auto &inst = instance();
    auto cb = [&](_malloc_zone_t *zone, void *arg)
    {
      inst.on_free(arg);
    };
    
    return inst.m_free(inst, cb, zone, ptr);
  }
  
  static void free_definite_size(_malloc_zone_t *zone, void *ptr, std::size_t size)
  {
    auto &inst = instance();
    auto cb = [&](_malloc_zone_t *zone, void *arg, std::size_t size)
    {
      inst.on_free(arg);
    };
    
    return inst.m_free_definite_size(inst, cb, zone, ptr, size);
  }
  
  static void *realloc(_malloc_zone_t *zone, void *ptr, std::size_t size)
  {
    auto &inst = instance();
    auto cb = [&](void *res, _malloc_zone_t *zone, void *arg, std::size_t size)
    {
      inst.on_realloc(res, arg, size);
    };

    return inst.m_realloc(inst, cb, zone, ptr, size);
  }
  
  static OsxAppLogger &instance();

  Wrapper<void *(_malloc_zone_t *, std::size_t)> m_malloc;
  Wrapper<void (_malloc_zone_t *, void *)> m_free;
  Wrapper<void (_malloc_zone_t *, void *, std::size_t)> m_free_definite_size;
  Wrapper<void *(_malloc_zone_t *, void *, std::size_t)> m_realloc;
};

OsxAppLogger g_data;

OsxAppLogger &OsxAppLogger::instance()
{
  return g_data;
}

AppLogger &AppLogger::instance() { return OsxAppLogger::instance(); }

#endif
