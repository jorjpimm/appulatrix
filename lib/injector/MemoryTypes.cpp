#include "MemoryTypes.h"

IMPLEMENT_TYPE_BUILDER(Allocation, type)
{
  type.add("pointer", &Allocation::pointer);
  type.add("size", &Allocation::size);
}

IMPLEMENT_TYPE_BUILDER(Deallocation, type)
{
  type.add("pointer", &Allocation::pointer);
}

IMPLEMENT_TYPE_BUILDER(Reallocation, type)
{
  type.add("dealloc", &Reallocation::dealloc);
  type.add("alloc", &Reallocation::alloc);
}
