#pragma once

// Code from https://vld.codeplex.com/ (LGPL)

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>

class WrapperBase;

struct PatchEntry
{
  LPCSTR importName;        // The name (or ordinal) of the imported API being patched.
  WrapperBase* original;    // Pointer to the original function.
  LPCVOID replacement;      // Pointer to the function to which the imported API should be patched through to.
};

struct ModuleEntry
{
  LPCSTR          exportModuleName; // The name of the module exporting the patched API.
  UINT_PTR        moduleBase;       // The base address of the exporting module (filled in at runtime when the modules are loaded).
  PatchEntry*   patchTable;
};

BOOL PatchImport(HMODULE importmodule, ModuleEntry *patchModule);

void ReplaceImportsInLoadedModules(ModuleEntry *modules);
