#ifdef _WIN32

// Code from https://vld.codeplex.com/ (LGPL)

#include "Vld.h"
#include "VldDbgHelp.h"
#include "Wrapper.h"

#define R2VA(moduleBase, rva)  (((PBYTE)moduleBase) + rva) // Relative Virtual Address to Virtual

ImageDirectoryEntries g_Ide;

HMODULE GetCallingModule(UINT_PTR pCaller)
{
  HMODULE hModule = NULL;
  MEMORY_BASIC_INFORMATION mbi;
  if (VirtualQuery((LPCVOID)pCaller, &mbi, sizeof(MEMORY_BASIC_INFORMATION)) == sizeof(MEMORY_BASIC_INFORMATION))
  {
    // the allocation base is the beginning of a PE file
    hModule = (HMODULE)mbi.AllocationBase;
  }
  return hModule;
}

LPVOID FindRealCode(LPVOID pCode)
{
  LPVOID result = pCode;
  if (pCode != NULL)
  {
    if (*(WORD *)pCode == 0x25ff) // JMP r/m32
    {
#ifdef _WIN64
      LONG offset = *((LONG *)((ULONG_PTR)pCode + 2));
      // RIP relative addressing
      PBYTE pNextInst = (PBYTE)((ULONG_PTR)pCode + 6);
      pCode = *(LPVOID*)(pNextInst + offset);
      return pCode;
#else
      DWORD addr = *((DWORD *)((ULONG_PTR)pCode + 2));
      pCode = *(LPVOID*)(addr);
      return FindRealCode(pCode);
#endif
    }
    if (*(BYTE *)pCode == 0xE9) // JMP rel32
    {
      // Relative next instruction
      PBYTE	pNextInst = (PBYTE)((ULONG_PTR)pCode + 5);
      LONG	offset = *((LONG *)((ULONG_PTR)pCode + 1));
      pCode = (LPVOID*)(pNextInst + offset);
      return FindRealCode(pCode);
    }
  }
  return result;
}

BOOL PatchImport(HMODULE importmodule, ModuleEntry *patchModule)
{
  HMODULE exportmodule = (HMODULE)patchModule->moduleBase;
  if (exportmodule == NULL)
    return FALSE;

  IMAGE_IMPORT_DESCRIPTOR *idte = NULL;
  IMAGE_SECTION_HEADER    *section = NULL;
  ULONG                    size = 0;

  // Locate the importing module's Import Directory Table (IDT) entry for the
  // exporting module. The importing module actually can have several IATs --
  // one for each export module that it imports something from. The IDT entry
  // gives us the offset of the IAT for the module we are interested in.
  {
    idte = (IMAGE_IMPORT_DESCRIPTOR*)g_Ide.ImageDirectoryEntryToDataEx((PVOID)GetCallingModule((UINT_PTR)importmodule), TRUE,
      IMAGE_DIRECTORY_ENTRY_IMPORT, &size, &section);
  }

  if (idte == NULL) {
    // This module has no IDT (i.e. it imports nothing).
    return FALSE;
  }
#ifdef PRINTHOOKINFO
  bool dllNamePrinted = false;
  CHAR  cwBuffer[2048] = { 0 };
  LPSTR pszBuffer = cwBuffer;
  DWORD dwMaxChars = _countof(cwBuffer);
  DWORD dwLength = ::GetModuleFileNameA(importmodule, pszBuffer, dwMaxChars);
#endif

  int result = 0;
  while (idte->FirstThunk != 0x0) {
    PCHAR importdllname = (PCHAR)R2VA(importmodule, idte->Name);
    UNREFERENCED_PARAMETER(importdllname);

    auto *patchEntry = patchModule->patchTable;
    int i = 0;
    while (patchEntry->importName)
    {
      LPCSTR importname = patchEntry->importName;
      LPCVOID replacement = patchEntry->replacement;

      // Get the *real* address of the import. If we find this address in the IAT,
      // then we've found the entry that needs to be patched.
      //LPVOID import = VisualLeakDetector::_RGetProcAddress(exportmodule, importname);
      LPVOID import = GetProcAddress(exportmodule, importname);
      if (!import)
        import = GetProcAddress(exportmodule, importname);
      import = FindRealCode(import);

      if (import == NULL) // Perhaps the named export module does not actually export the named import?
      {
        patchEntry++; i++;
        continue;
      }

      // Locate the import's IAT entry.
      IMAGE_THUNK_DATA *thunk = (IMAGE_THUNK_DATA*)R2VA(importmodule, idte->FirstThunk);
      IMAGE_THUNK_DATA *origThunk = (IMAGE_THUNK_DATA*)R2VA(importmodule, idte->OriginalFirstThunk);
      for (; origThunk->u1.Function != NULL;
      origThunk++, thunk++)
      {
        LPVOID func = FindRealCode((LPVOID)thunk->u1.Function);
        if (((DWORD_PTR)func == (DWORD_PTR)import))
        {
          // Found the IAT entry. Overwrite the address stored in the IAT
          // entry with the address of the replacement. Note that the IAT
          // entry may be write-protected, so we must first ensure that it is
          // writable.
          if (import != replacement)
          {
            if (patchEntry->original != NULL)
              patchEntry->original->set_function(func);

            DWORD protect;
            if (VirtualProtect(&thunk->u1.Function, sizeof(thunk->u1.Function), PAGE_EXECUTE_READWRITE, &protect)) {
              thunk->u1.Function = (DWORD_PTR)replacement;
              if (VirtualProtect(&thunk->u1.Function, sizeof(thunk->u1.Function), protect, &protect)) {
#ifdef PRINTHOOKINFO
                if (!IS_ORDINAL(importname)) {
                  DbgReport(L"Hook dll \"%S\" import %S!%S()\n",
                    strrchr(pszBuffer, '\\') + 1, patchModule->exportModuleName, importname);
                }
                else {
                  DbgReport(L"Hook dll \"%S\" import %S!%zu()\n",
                    strrchr(pszBuffer, '\\') + 1, patchModule->exportModuleName, importname);
                }
#endif
              }
            }
          }
          // The patch has been installed in the import module.
          result++;
          break;
        }
#ifdef PRINTHOOKINFO
        PIMAGE_IMPORT_BY_NAME funcEntry = (PIMAGE_IMPORT_BY_NAME)
          R2VA(importmodule, origThunk->u1.AddressOfData);
        if (stricmp(importdllname, patchModule->exportModuleName) == 0)
        {
          if (!(origThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG) && !IS_ORDINAL(importname) &&
            strcmp(reinterpret_cast<const char*>(funcEntry->Name), importname) == 0)
          {
            if (!dllNamePrinted)
            {
              dllNamePrinted = true;
              DbgReport(L"Hook dll \"%S\":\n",
                strrchr(pszBuffer, '\\') + 1);
            }
            DbgReport(L"Import found %S(\"%S\") for dll \"%S\".\n",
              importname, patchModule->exportModuleName, importdllname);
            break;
          }
          if ((origThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG) && IS_ORDINAL(importname) &&
            (IMAGE_ORDINAL(origThunk->u1.Ordinal) == (UINT_PTR)importname))
          {
            if (!dllNamePrinted)
            {
              dllNamePrinted = true;
              DbgReport(L"Hook dll \"%S\":\n",
                strrchr(pszBuffer, '\\') + 1);
            }
            DbgReport(L"Import found %zu(\"%S\") for dll \"%S\".\n",
              importname, patchModule->exportModuleName, importdllname);
            break;
          }
        }
#endif
      }
      patchEntry++; i++;
    }

    idte++;
  }

  // The import's IAT entry was not found. The importing module does not
  // import the specified import.
  return result > 0;
}

void ReplaceImportsInLoadedModules(ModuleEntry *modules)
{
  auto callback = [](
    PCTSTR  ModuleName,
    DWORD64 ModuleBase,
    ULONG   ModuleSize,
    PVOID   modules
    ) -> BOOL
  {
    auto module_ptr = (ModuleEntry *)modules;
    for (; module_ptr->exportModuleName; ++module_ptr)
    {
      PatchImport((HMODULE)ModuleBase, module_ptr);
    }
    return TRUE;
  };

  EnumerateLoadedModules64(
    GetCurrentProcess(),
    callback,
    modules
  );
}

#endif
