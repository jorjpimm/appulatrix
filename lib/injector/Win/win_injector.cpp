#ifdef _WIN32

#include "MemoryTypes.h"
#include "AppLogger.h"
#include "Wrapper.h"
#include "Vld.h"

#include <gsl/gsl-lite.h>
#include <Windows.h>
#include <Psapi.h>

#include <cstdlib>
#include <cstdio>
#include <functional>
#include <iostream>
#include <thread>
#include <unordered_map>

struct WinAppLogger : public AppLogger
{
  std::string make_identifier()
  {
    std::string id = std::to_string(::GetCurrentProcessId());

    std::array<char, MAX_PATH> filename;
    std::size_t len = ::GetModuleFileNameEx(GetCurrentProcess(), nullptr, filename.data(), DWORD(filename.size()));

    return id + "_" + boost::filesystem::path(filename.begin(), filename.begin() + len).stem().string();
  }

  WinAppLogger()
  : AppLogger(make_identifier())
  {
    patch_functions();
  }

  static WinAppLogger &instance();

private:
  void patch_functions()
  {
    PatchEntry ucrtbased_entries[] = {
      { "malloc", &m_malloc, WinAppLogger::malloc },
      { "_malloc_dbg", &m_malloc_dbg, WinAppLogger::malloc_dbg },
      { "free", &m_free, WinAppLogger::free },
      { "realloc", &m_realloc, WinAppLogger::realloc },
      { "_free_dbg", &m_free_dbg, WinAppLogger::free_dbg },
      {}
    };
    PatchEntry ucrtbase_entries[] = {
      { "malloc", &m_malloc, WinAppLogger::malloc },
      { "free", &m_free, WinAppLogger::free },
      { "realloc", &m_realloc, WinAppLogger::realloc },
      {}
    };

    ModuleEntry modules[] = {
      { "ucrtbased.dll", (UINT_PTR)GetModuleHandle("ucrtbased.dll"), ucrtbased_entries },
      { "ucrtbase.dll", (UINT_PTR)GetModuleHandle("ucrtbase.dll"), ucrtbase_entries },
      {}
    };

    ReplaceImportsInLoadedModules(modules);
  }

  static void *malloc(std::size_t size)
  {
    auto &inst = instance();
    auto cb = [&](void *result, std::size_t size)
    {
      inst.on_malloc(result, size);
    };

    return inst.m_malloc(inst, cb, size);
  }

  static void *malloc_dbg(std::size_t size, int a, const char *b, int c)
  {
    auto &inst = instance();
    auto cb = [&](void *result, std::size_t size, int a, const char *b, int c)
    {
      inst.on_malloc(result, size);
    };

    return inst.m_malloc_dbg(inst, cb, size, a, b, c);
  }

  static void free(void *ptr)
  {
    auto &inst = instance();
    auto cb = [&](void *arg)
    {
      inst.on_free(arg);
    };

    return inst.m_free(inst, cb, ptr);
  }

  static void free_dbg(void *ptr, int a)
  {
    auto &inst = instance();
    auto cb = [&](void *arg, int a)
    {
      inst.on_free(arg);
    };

    return inst.m_free_dbg(inst, cb, ptr, a);
  }

  static void *realloc(void *ptr, std::size_t size)
  {
    auto &inst = instance();
    auto cb = [&](void *result, void *arg, std::size_t size)
    {
      inst.on_realloc(result, arg, size);
    };

    return inst.m_realloc(inst, cb, ptr, size);
  }


  Wrapper<decltype(::malloc)> m_malloc;
  Wrapper<void *(std::size_t size, int, const char *, int)> m_malloc_dbg;
  Wrapper<decltype(::free)> m_free;
  Wrapper<void(void *ptr, int)> m_free_dbg;
  Wrapper<decltype(::realloc)> m_realloc;
};

WinAppLogger g_injector;
WinAppLogger &WinAppLogger::instance()
{
  return g_injector;
}

AppLogger &AppLogger::instance() { return WinAppLogger::instance(); }

#endif
