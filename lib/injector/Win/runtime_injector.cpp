#include <boost/algorithm/string.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include <Windows.h>
#include <Psapi.h>
#include <tlhelp32.h>

#include <iostream>
#include <string>

namespace po = boost::program_options;

struct Options
{
  Options()
  : description(description_text())
  {
    positional.add("process", 1);

    description.add_options()
      ("help,h", "Display help message")
      ("start,s", po::bool_switch(), "Start a new process")
      ("args", po::value(&arguments), "Arguments to pass when starting")
      ("process,p", po::value(&process)->required(), "Process to attach to")
    ;
  }

  void run(int argc, char *argv[])
  {
    po::store(
      po::command_line_parser(argc, argv)
        .options(description)
        .positional(positional)
        .run(),
      variables
    );
    po::notify(variables);
  }

  std::string description_text() const
  {
    return
      "Usage injector [options] process\n"
      "\n"
      "Allowed options";
  }

  std::string input_process() const
  {
    return process;
  }

  bool should_start() const
  {
    return variables.count("start") > 0;
  }

  po::positional_options_description positional;
  po::options_description description;

  po::variables_map variables;
  std::string process;
  bool start;
  std::string arguments;
};

struct HandleWrapper
{
  HandleWrapper(HANDLE h)
  : m_handle(h)
  {
  }

  ~HandleWrapper()
  {
    if (m_handle)
    {
      CloseHandle(m_handle);
      m_handle = nullptr;
    }
  }

  HandleWrapper(HandleWrapper &&h)
  : m_handle(h.m_handle)
  {
    h.m_handle = NULL;
  }

  HandleWrapper &operator=(HandleWrapper &&h)
  {
    std::swap(m_handle, h.m_handle);
    return *this;
  }

  explicit operator bool() const
  {
    return m_handle != nullptr;
  }

  HANDLE get() const
  {
    return m_handle;
  }

  HANDLE m_handle;
};

boost::optional<HandleWrapper> find_process(const std::string &process)
{
  PROCESSENTRY32 entry;
  entry.dwSize = sizeof(PROCESSENTRY32);
  HandleWrapper snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

  boost::optional<int> process_id;
  try {
    process_id = std::stoi(process);
  }
  catch (std::invalid_argument &) { }

  if (::Process32First(snapshot.get(), &entry) == TRUE)
  {
    while (::Process32Next(snapshot.get(), &entry) == TRUE)
    {
      boost::filesystem::path exe = entry.szExeFile;
      if (boost::iequals(exe.filename().string(), process) ||
        boost::iequals(exe.stem().string(), process) || 
        (process_id && *process_id == entry.th32ProcessID))
      {
        HandleWrapper hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
        if (hProcess)
        {
          return hProcess;
        }
      }
    }
  }
  return boost::none;
}

boost::filesystem::path get_module_filename(HANDLE h)
{
  std::array<char, MAX_PATH> filename;
  std::size_t len = ::GetModuleFileNameEx(h, nullptr, filename.data(), DWORD(filename.size()));
  return boost::filesystem::path(filename.begin(), filename.begin() + len);
}

void run_injector(const Options &opts)
{
  bool wait_for_exit = false;
  boost::optional<HandleWrapper> process;
  boost::optional<HandleWrapper> resume_thread;
  if (!opts.start)
  {
    process = find_process(opts.input_process());
  }
  else
  {
    STARTUPINFO startup_info = {};
    PROCESS_INFORMATION process_info = {};
    BOOL res = CreateProcess(
      opts.process.c_str(),
      const_cast<char *>(opts.arguments.c_str()),
      NULL,
      NULL,
      FALSE,
      CREATE_SUSPENDED,
      NULL,
      NULL,
      &startup_info,
      &process_info
    );

    if (res != TRUE)
    {
      std::cerr << "Failed to start process " << opts.process << " " << opts.arguments << std::endl;
      exit(EXIT_FAILURE);
    }

    process = process_info.hProcess;
    resume_thread = process_info.hThread;
    wait_for_exit = true;
  }
  if (!process)
  {
    std::cerr << "No process found matching " << opts.input_process() << std::endl;
    exit(EXIT_FAILURE);
  }

  HandleWrapper current_process = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, ::GetCurrentProcessId());
  auto &dst_process = *process;

  auto dst_path = get_module_filename(dst_process.get());

  std::cout << "Attaching to " << boost::filesystem::path(dst_path).filename().string() << " (" << ::GetProcessId(dst_process.get()) << ")" << std::endl;

  auto my_dir = get_module_filename(current_process.get());
  auto injector_path = my_dir.parent_path() / "appulatrix_injector.dll";
  std::cout << "Injecting " << injector_path << std::endl;

  auto remote_string = ::VirtualAllocEx(
    dst_process.get(),
    nullptr,
    injector_path.size(),
    MEM_COMMIT, 
    PAGE_READWRITE
  );
  ::WriteProcessMemory(
    dst_process.get(), 
    remote_string, 
    (void*)injector_path.string().data(),
    injector_path.size(),
    nullptr
  );

  HMODULE hKernel32 = ::GetModuleHandle("Kernel32");
  auto load_library_address = (LPTHREAD_START_ROUTINE)::GetProcAddress(hKernel32, "LoadLibraryA");
  auto thread = ::CreateRemoteThread(
    dst_process.get(),
    nullptr, 
    0,
    load_library_address,
    remote_string,
    0,
    nullptr
  );
  ::WaitForSingleObject(thread, INFINITE);

  // Get handle of the loaded module
  DWORD hLibModule;   // Base address of loaded module (==HMODULE);
  ::GetExitCodeThread(thread, &hLibModule);

  if (hLibModule != 0)
  {
    std::cout << "Successfully injected." << std::endl;
  }
  else
  {
    std::cout << "Failed to inject." << std::endl;
  }

  // Clean up
  ::CloseHandle(thread);
  ::VirtualFreeEx(
    dst_process.get(),
    remote_string, 
    injector_path.size(),
    MEM_RELEASE
  );

  if (resume_thread)
  {
    ResumeThread((*resume_thread).get());
  }

  if (wait_for_exit)
  {
    WaitForSingleObject((*process).get(), INFINITE);
    DWORD exit_code;
    GetExitCodeProcess((*process).get(), &exit_code);
    if (exit_code != EXIT_SUCCESS)
    {
      std::cerr << "Injected process did not succeed" << std::endl;
      exit(exit_code);
    }
  }
}


int main(int argc, char *argv[])
{
  Options opts;

  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "Display help message")
  ;

  try
  {
    po::variables_map vm;
    po::store(
      po::command_line_parser(argc, argv)
        .options(desc)
        .allow_unregistered()
        .run(),
      vm
    );
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << opts.description << std::endl;
      return EXIT_SUCCESS;
    }

    opts.run(argc, argv);
    run_injector(opts);
    return EXIT_SUCCESS;
  }
  catch(...)
  {
    std::cerr << opts.description << std::endl;

    std::cerr << "Unhandled exception!" << std::endl <<
      boost::current_exception_diagnostic_information() << std::endl;

    return EXIT_FAILURE;
  }
}
