#pragma once

#include "AppLogger.h"

namespace detail
{
template <typename Cb, typename... Args> void run_callback(AppLogger &logger, Cb cb, Args &&...args)
{
  if (logger.disabled())
  {
    return;
  }
  
  cb(std::forward<Args&&>(args)...);
}

template <typename Derived, typename ReturnType, typename FnType> class WrapperImpl
{
public:
  template <typename Cb, typename... Args>
    ReturnType operator()(AppLogger &logger, Cb cb, Args &&...args)
  {
    logger.check_sane("1");
    auto fn = static_cast<Derived *>(this)->get_function();
    auto res = fn(std::forward<Args&&>(args)...);
    run_callback(logger, cb, res, std::forward<Args&&>(args)...);
    logger.check_sane("2");
    return res;
  }

  template <typename... Args>
    ReturnType call_force_disabled(Args &&...args)
  {
    auto fn = static_cast<Derived *>(this)->get_function();
    return fn(std::forward<Args&&>(args)...);
  }
};

template <typename Derived, typename FnType> class WrapperImpl<Derived, void, FnType>
{
public:
  template <typename Cb, typename... Args>
    void operator()(AppLogger &logger, Cb cb, Args &&...args)
  {
    auto fn = static_cast<Derived *>(this)->get_function();
    fn(std::forward<Args&&>(args)...);
    run_callback(logger, cb, std::forward<Args&&>(args)...);
  }

  template <typename... Args>
    void call_force_disabled(Args &&...args)
  {
    auto fn = static_cast<Derived *>(this)->get_function();
    return fn(std::forward<Args&&>(args)...);
  }
};
}

class WrapperBase
{
public:
  WrapperBase(void *fn)
  : m_real_function(fn)
  {
  }

  void *get_function() const { return m_real_function; }

  void set_function(void *fn)
  {
    m_real_function = fn;
  }

private:
  void *m_real_function = nullptr;
};

template <typename FnType> class Wrapper
  : public detail::WrapperImpl<
      Wrapper<FnType>,
      typename std::function<FnType>::result_type,
      FnType>,
    public WrapperBase
{
public:
  Wrapper(void *fn = nullptr)
  : WrapperBase(fn)
  {
  }

  FnType *get_function() const
  {
    return reinterpret_cast<FnType*>(WrapperBase::get_function());
  }
};
