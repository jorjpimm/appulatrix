#include "TimelineScale.h"
#include "Graph.h"

#include <QPainter>
#include <QMouseEvent>

#include <array>

TimelineScale::TimelineScale(
  QWidget *parent)
: QWidget(parent)
, m_timeline(TimelineCore::DragMode::DragDomain)
{
  setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
  
  auto c = cursor();
  c.setShape(Qt::OpenHandCursor);
  setCursor(c);
}

QSize TimelineScale::sizeHint() const
{
  return QSize(50, 50); 
}

void TimelineScale::set_range(const VisibleTimeRange &range)
{
  m_timeline.set_range(range);
  update();
}

bool TimelineScale::event(QEvent *e)
{
  auto transform = [&](int x) {
    auto visible_ratio = double(x) / double(width());
    auto duration_offset = range().domain().time_to_ratio(range().visible().begin());
    return duration_offset + visible_ratio * range().visible_ratio();
  };

  if (m_timeline.accept_event(e, transform))
  {
    emit visible_changed(m_timeline.range().visible());
    update();
    return true;
  }

  return QWidget::event(e);
}

void TimelineScale::mousePressEvent(QMouseEvent *e)
{
  if (e->button() == Qt::RightButton)
  {
    m_right_click_drag.emplace();
    m_right_click_drag->first = map_pixel_to_time(e->pos().x());
    m_right_click_drag->second = map_pixel_to_time(e->pos().x());
    update();
  }
}

void TimelineScale::mouseMoveEvent(QMouseEvent *e)
{
  if (e->buttons() & Qt::RightButton)
  {
    m_right_click_drag->second = map_pixel_to_time(e->pos().x());
    update();
  }
}

void TimelineScale::mouseReleaseEvent(QMouseEvent *e)
{
  if (e->button() == Qt::RightButton && m_right_click_drag)
  {  
    if (m_right_click_drag->first > m_right_click_drag->second)
    {
      std::swap(m_right_click_drag->first, m_right_click_drag->second);
    }

    emit visible_changed(TimeRange(*m_right_click_drag));
    m_right_click_drag = boost::none;
    update();
  }
}

void TimelineScale::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);

  graph_tools::TimeAxis axis(range().domain().begin(), [this](
    appulatrix::Time x,
    const graph_tools::Axis<appulatrix::Time> &axis) -> QString
  {
    using namespace std::chrono;
    auto offset = duration_cast<nanoseconds>(x - range().domain().begin());
    double nanoseconds =
      double(nanoseconds::period::den) / double(nanoseconds::period::num);

    std::array<char, 128> buf;
    snprintf(buf.data(), buf.size(), "%.2f s", double(offset.count())/nanoseconds);

    return buf.data();
  });
  axis.set_range(0, width());
  axis.set_domain(range().visible().begin(), range().visible().end());
  
  graph_tools::paint_axis(
    &painter,
    axis,
    QPointF(0, height()-5),
    QPointF(1, 0),
    QPointF(0, -1),
    10,
    graph_tools::TickLabelPosition::Above,
    width());
  
  if (m_right_click_drag)
  {
    auto col = palette().color(QPalette::Highlight);
    painter.setPen(QPen(col, 2));
    
    col.setAlpha(192);
    painter.setBrush(col);
    
    auto x1 = m_timeline.range().visible().time_to_ratio(m_right_click_drag->first);
    auto x2 = m_timeline.range().visible().time_to_ratio(m_right_click_drag->second);
    painter.drawRect(width() * x1, 0, width() * (x2 - x1), height());
  }
}

TimelineScale::Time TimelineScale::map_pixel_to_time(int x) const
{
  auto ratio = double(x) / width();
  return m_timeline.range().visible().ratio_to_time(ratio);
}
