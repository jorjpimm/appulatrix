#include "SelectionContainer.h"

#include <gsl/gsl-lite.h>

#include <cassert>

SelectionContainer::SelectionContainer()
{
}

const Selection &SelectionContainer::get_selection() const
{
    return m_selection;
}

void SelectionContainer::set_selection(const Selection &sel)
{
  assert(!m_in_selection_change);
  m_in_selection_change = true;
  auto fin = gsl::finally([&]{ m_in_selection_change = false; });
  
  m_selection = sel;
  emit selection_changed();
}
