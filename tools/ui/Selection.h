#pragma once

#include <unordered_set>

namespace appulatrix
{
class Event;
}

class Selection
{
public:
  /// Clear all selected items
  void clear();

  /// Find if item [event] is selected.
  bool is_selected(const appulatrix::Event *event) const;
  /// Set [event]'s selection state to [is_selected].
  void set_selected(const appulatrix::Event *event, bool is_selected);
  
  /// Find all the selected items in [container], extracted using [extract].
  template <typename T, typename Extract>
  std::unordered_set<const appulatrix::Event *> query_selected(const T &container, Extract extract) const
  {
    std::unordered_set<const appulatrix::Event *> output;
    for (auto &item : container)
    {
      auto ev = extract(item);
      if (m_selected.find(ev) != m_selected.end())
      {
        output.insert(ev);
      }
    }
    return output;
  }

private:
  std::unordered_set<const appulatrix::Event *> m_selected;
};
