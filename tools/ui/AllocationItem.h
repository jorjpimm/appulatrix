#pragma once

#include "AllocationDataManager.h"
#include "Graph.h"

#include <QGraphicsObject>

using AllocationGraphType = graph_tools::Graph<appulatrix::Time, std::size_t>;

class AllocationItem : public AllocationGraphType::Item
{
public:
  AllocationItem(
    const std::shared_ptr<AllocationDataManager> &allocs)
  : m_allocs(allocs)
  {
  }

  void paint(
    QPainter *painter,
    const graph_tools::Axis<appulatrix::Time> &x_axis,
    const graph_tools::Axis<std::size_t> &y_axis) override
  {
    const auto &points = m_allocs->points();
    if (points.empty()) return;
    
    QPainterPath path;
    path.moveTo(x_axis(points.front().event->time), y_axis(0));

    for (auto &pt : points)
    {
      path.lineTo(x_axis(pt.event->time), y_axis(pt.current_size));
    }
    path.lineTo(x_axis(points.back().event->time), y_axis(0));
    path.closeSubpath();

    painter->setBrush(Qt::yellow);
    painter->drawPath(path);
  }

private:
  std::shared_ptr<AllocationDataManager> m_allocs;
};
