#pragma once

#include "TimelineCore.h"
#include "appulatrix/Time.h"

#include <QWidget>

class TimelineOverview : public QWidget
{
  Q_OBJECT
public:
  using Time = appulatrix::Time;
  using TimePair = std::pair<Time, Time>;

  TimelineOverview(
    QWidget *parent);

  QSize sizeHint() const override;

  void set_range(const VisibleTimeRange &domain);
  const VisibleTimeRange &range() const { return m_timeline.range(); }

signals:
  void visible_changed(const TimeRange &time);

protected:
  bool event(QEvent *e) override;
  void paintEvent(QPaintEvent *event) override;
  
  QRect visible_rect() const;

  TimelineCore m_timeline;
};
