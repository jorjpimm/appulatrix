#include "Selection.h"

void Selection::clear()
{
  m_selected.clear();
}

bool Selection::is_selected(const appulatrix::Event *event) const
{
  return m_selected.find(event) != m_selected.end();
}

void Selection::set_selected(const appulatrix::Event *event, bool is_selected)
{
  if (is_selected)
  {
    m_selected.insert(event);
  }
  else
  {
    m_selected.erase(event);
  }
}