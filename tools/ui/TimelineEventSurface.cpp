#include "TimelineEventSurface.h"
#include "Graph.h"

#include <boost/optional.hpp>

#include <iostream>

#include <QMouseEvent>
#include <QTransform>

class RootItem : public QGraphicsItem
{
  QRectF boundingRect() const override
  {
    return QRectF();
  }
  void paint(
    QPainter *painter,
    const QStyleOptionGraphicsItem *option,
    QWidget *widget) override
  {
  }
};

TimelineEventSurface::TimelineEventSurface()
: QGraphicsView()
, m_timeline(TimelineCore::DragMode::DragDomain)
{
  setMouseTracking(true);
  m_scene = new QGraphicsScene(this);
  setScene(m_scene);
  
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  
  setAlignment(Qt::AlignLeft | Qt::AlignTop);
  setResizeAnchor(QGraphicsView::NoAnchor);
  setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
  
  m_root = new RootItem();
  m_scene->addItem(m_root);
  
  m_transform_pixel_to_domain = [&](int x) {
    auto visible_ratio = double(x) / double(width());
    auto duration_offset = range().domain().time_to_ratio(range().visible().begin());
    return duration_offset + visible_ratio * range().visible_ratio();
  };
}

void TimelineEventSurface::set_range(const VisibleTimeRange &domain)
{
  m_timeline.set_range(domain);
  setup_timeline_viewport();
  
  m_time_axis.set_domain(domain.visible().begin(), domain.visible().end());
  m_time_axis.set_range(map_x_to_milliseconds(0), map_x_to_milliseconds(width()));
  
  update();
}

bool TimelineEventSurface::event(QEvent *e)
{
  if (m_timeline.accept_event(e, m_transform_pixel_to_domain))
  {
    emit visible_changed(m_timeline.range().visible());
    
    update();
    return true;
  }
  
  if (e->type() == QEvent::Leave)
  {
    m_highlight_position = boost::none;
    update_highlight();
    update();
  }

  return QGraphicsView::event(e);
}

void TimelineEventSurface::resizeEvent(QResizeEvent *e)
{
  QGraphicsView::resizeEvent(e);
  setup_timeline_viewport();
}

void TimelineEventSurface::wheelEvent(QWheelEvent *e)
{
  if (m_timeline.accept_event(e, m_transform_pixel_to_domain))
  {
    emit visible_changed(m_timeline.range().visible());
    
    update();
  }
}

void TimelineEventSurface::mouseMoveEvent(QMouseEvent *e)
{
  if (m_timeline.accept_event(e, m_transform_pixel_to_domain))
  {
    emit visible_changed(m_timeline.range().visible());
    
    update();
  }
  
  if (e->type() == QEvent::MouseMove)
  {
    m_highlight_position = e->pos();
    update_highlight();
    update();
  }
  
  QGraphicsView::mouseMoveEvent(e);
}

void TimelineEventSurface::mouseReleaseEvent(QMouseEvent *e)
{
  if (m_timeline.accept_event(e, m_transform_pixel_to_domain))
  {
    emit visible_changed(m_timeline.range().visible());
    
    update();
  }


  
  QGraphicsView::mouseReleaseEvent(e);
}

void TimelineEventSurface::add_item(QGraphicsItem *item)
{
  item->setParentItem(m_root);
}

void TimelineEventSurface::highlight_item(boost::optional<std::pair<appulatrix::Time, int>> pos)
{
  if (pos)
  {
    //auto pt = m_root->mapToScene(QPointF(m_time_axis.map_to_range(pos->first), pos->second));
    //auto items = m_scene->items(pt);
  }
}

void TimelineEventSurface::drawForeground(QPainter *painter, const QRectF &rect)
{
  if (m_highlight_position)
  {
    painter->setPen(Qt::gray);
    painter->drawLine(m_highlight_position->x(), 0, m_highlight_position->x(), height());
  }
}

void TimelineEventSurface::setup_timeline_viewport()
{
  auto offset = (range().visible().begin() - range().domain().begin()).count();

  auto sf_dbl = double(scale_factor);
  auto duration_ns = range().visible().duration().count();
  auto duration_scale = width() / (duration_ns / sf_dbl);

  QTransform tx;
  tx.scale(duration_scale, 1.0);
  tx.translate(-offset / sf_dbl, 0);

  m_root->setTransform(tx);
  
  resetTransform();
  setSceneRect(QRectF(0, 0, width(), height()));
  
  update_highlight();
}

void TimelineEventSurface::update_highlight()
{
  m_root->update();
  
  if (!m_highlight_position)
  {
    highlight_item(boost::none);
    return;
  }
  
  auto x = x_axis().map_from_range(map_x_to_milliseconds(m_highlight_position->x()));
  highlight_item(std::make_pair(x, m_highlight_position->y()));
}

double TimelineEventSurface::map_x_to_milliseconds(int x) const
{
  return m_root->mapFromScene(mapToScene(x, 0)).x();
}
