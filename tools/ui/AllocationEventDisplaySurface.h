#pragma once

#include "TimelineEventSurface.h"
#include "AllocationDataManager.h"

class AllocationEventDisplaySurface : public TimelineEventSurface
{
public:
  class AllocationItem : public QGraphicsItem
  {
  public:
    AllocationItem(
      AllocationEventDisplaySurface *surface)
    : m_surface(surface)
    {
    }
    
    void set_domain(const VisibleTimeRange &domain)
    {
      m_domain = domain;
      prepareGeometryChange();
    }
    
    void set_highlight_time(const boost::optional<appulatrix::Time> &time)
    {
      m_highlight_time = time;
      update();
    }
    
    QRectF boundingRect() const override
    {
      return QRectF(m_surface->scale_x(m_domain.domain().begin()), 0, m_surface->scale_x(m_domain.domain().end()), m_surface->height());
    }
    
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) override
    {
      graph_tools::NumberAxis<double> y_axis({});
      y_axis.set_domain(0, m_surface->m_data_manager->maximum_allocated_bytes());
      y_axis.set_range(m_surface->height(), 0);
      
      const auto &points = m_surface->m_data_manager->points();
      if (points.empty()) return;
      
      QPainterPath path;
      auto prev_time = m_domain.domain().begin();
      std::size_t prev_size = 0;
      auto prev_y = y_axis(0);
      path.moveTo(m_surface->scale_x(m_domain.domain().begin()), prev_y);
      
      boost::optional<std::tuple<appulatrix::Time, appulatrix::Time, std::size_t>> highlight_range;
      
      for (auto &pt : points)
      {
        auto x = m_surface->scale_x(pt.event->time);
        path.lineTo(x, prev_y);
        
        prev_y = y_axis(pt.current_size);
        path.lineTo(x, prev_y);
        
        if (m_highlight_time &&
          (prev_time <= *m_highlight_time && *m_highlight_time <= pt.event->time))
        {
          highlight_range = std::make_tuple(prev_time, pt.event->time, prev_size);
        }
        
        prev_time = pt.event->time;
        prev_size = pt.current_size;
      }
      
      if (m_highlight_time &&
          (prev_time <= *m_highlight_time))
      {
        highlight_range = std::make_tuple(prev_time, m_domain.domain().end(), points.back().current_size);
      }
      
      path.lineTo(m_surface->scale_x(m_domain.domain().end()), y_axis(points.back().current_size));
      path.lineTo(m_surface->scale_x(m_domain.domain().end()), y_axis(0));
      path.closeSubpath();
      
      QColor colour(Qt::blue);
      
      painter->setBrush(colour);
      painter->setPen(Qt::transparent);
      painter->drawPath(path);
      
      if (highlight_range)
      {
        auto x1 = m_surface->scale_x(std::get<0>(*highlight_range));
        auto x2 = m_surface->scale_x(std::get<1>(*highlight_range));
        
        auto y1 = y_axis(std::get<2>(*highlight_range));
        auto y2 = y_axis(0);
        QRectF highlight(x1, y1, x2 - x1, y2-y1);
        
        painter->setBrush(colour.lighter());
        painter->drawRect(highlight);
      }
    }
  
  private:
    VisibleTimeRange m_domain;
    AllocationEventDisplaySurface *m_surface;
    boost::optional<appulatrix::Time> m_highlight_time;
  };
  
  AllocationEventDisplaySurface(
    appulatrix::TypeLookup &types)
  : m_types(types)
  , m_data_manager(std::make_shared<AllocationDataManager>())
  , m_allocation_item(new AllocationItem(this))
  {
    add_item(m_allocation_item);
  }
  
  void set_range(const VisibleTimeRange &range) override
  {
    TimelineEventSurface::set_range(range);
    m_allocation_item->set_domain(range);
  }
  
  void add_event(const appulatrix::Event &ev) override
  {
    auto do_alloction = [&](const appulatrix::Event &ev, const appulatrix::Type &type, const void *data)
    {
      auto ptr = type.get_field_value<std::uint64_t>(m_types, "pointer", data);
      auto size = type.get_field_value<std::uint64_t>(m_types, "size", data);
      m_data_manager->add_allocation(ev, ptr, size);
    };
    
    auto do_dealloction = [&](const appulatrix::Event &ev, const appulatrix::Type &type, const void *data)
    {
      auto ptr = type.get_field_value<std::uint64_t>(m_types, "pointer", data);
      m_data_manager->add_deallocation(ev, ptr);
    };
   
    auto &&type = m_types.find_type(ev.type);
    if (type.name() == "Allocation")
    {
      do_alloction(ev, type, ev.data.get());
    }
    else if (type.name() == "Deallocation")
    {
      do_dealloction(ev, type, ev.data.get());
    }
    else if (type.name() == "Reallocation")
    {
      auto dealloc = type.find_field("dealloc");
      do_dealloction(ev, m_types.find_type(dealloc->type()), dealloc->get_data_ptr(ev.data.get()));
      
      auto alloc = type.find_field("alloc");
      do_alloction(ev, m_types.find_type(alloc->type()), alloc->get_data_ptr(ev.data.get()));
    }
  }
  
  void highlight_item(boost::optional<std::pair<appulatrix::Time, int>> pos)
  {
    if (pos)
    {
      m_allocation_item->set_highlight_time(pos->first);
    }
    else
    {
      m_allocation_item->set_highlight_time(boost::none);
    }
  }
  
private:
  appulatrix::TypeLookup &m_types;
  std::shared_ptr<AllocationDataManager> m_data_manager;
  AllocationItem *m_allocation_item;
};
