#pragma once

#include "TimelineCore.h"
#include "Graph.h"

#include <boost/optional.hpp>

#include <QGraphicsScene>
#include <QGraphicsView>

namespace appulatrix
{
class Event;
class EventProviderData;
}

class Selection;

class TimelineEventSurface : public QGraphicsView
{
  Q_OBJECT
public:
  TimelineEventSurface();
  
  virtual void add_event(const appulatrix::Event &ev) = 0;
  virtual void add_child_event(const appulatrix::Event &ev)
  {
    add_event(ev);
  }
  
  virtual void set_range(const VisibleTimeRange &domain);
  const VisibleTimeRange &range() const { return m_timeline.range(); }
  
  virtual void refresh_selection(const Selection &selection) { }
  virtual bool has_any_selected() const { return false; }
  
  /// Find the time axis to map between scene and time coordinates
  const graph_tools::TimeAxis &x_axis() const
  {
    return m_time_axis;
  }
  
  const int scale_factor = 1e6;
  double scale_x(appulatrix::Time time) const
  {
    auto ns_offset = (time - range().domain().begin()).count();
    return ns_offset / double(scale_factor);
  }
  
signals:
  void visible_changed(const TimeRange &time);
  void selection_changed(const Selection &selection);

protected:
  bool event(QEvent *e) override;
  void resizeEvent(QResizeEvent *e) override;
  void wheelEvent(QWheelEvent *event) override;
  void mouseMoveEvent(QMouseEvent *e) override;
  void mouseReleaseEvent(QMouseEvent *e) override;
  
  void add_item(QGraphicsItem *item);
  
  virtual void highlight_item(boost::optional<std::pair<appulatrix::Time, int>> pos);
  
  virtual void drawForeground(QPainter *painter, const QRectF &rect);

private:
  void setup_timeline_viewport();
  void update_highlight();
  
  double map_x_to_milliseconds(int x) const;
  
  TimelineCore m_timeline;
  graph_tools::TimeAxis m_time_axis;
  boost::optional<QPoint> m_highlight_position;
  
  QGraphicsScene *m_scene;
  QGraphicsItem *m_root;
  
  std::function<double(int)> m_transform_pixel_to_domain;
};

class TimelineEventSurfaceFactory
{
public:
  virtual TimelineEventSurface *make_surface(const appulatrix::EventProviderData *) = 0;
};
