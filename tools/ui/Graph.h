#pragma once

#include "appulatrix/Event.h"

#include <QGraphicsItem>
#include <QPainter>

#include <cmath>
#include <memory>
#include <tuple>

namespace graph_tools
{

namespace detail
{
inline double pt_length(const QPointF &pt)
{
  return sqrt(pt.x() * pt.x() + pt.y() * pt.y());
}
}
  
struct Tick
{
  double location;
  QString label;
  bool major;
};
using TickVector = std::vector<Tick>;
  
template <typename AxisType> class Axis
{
public:
  using DomainPair = std::pair<AxisType, AxisType>;
  using FormatCallback = std::function<QString(AxisType, const Axis<AxisType> &)>;
  
  Axis(FormatCallback format)
  : m_format(format)
  {
  }
  
  void set_domain(const AxisType &begin, const AxisType &end)
  {
    m_domain = std::make_pair(begin, end);
  }
  
  void set_domain(const DomainPair &domain)
  {
    m_domain = domain;
  }
  
  void set_range(double begin, double end)
  {
    m_range_offset = begin;
    m_range_scale = end - begin;
  }
  
  virtual double map_from_domain(AxisType type) const = 0;
  virtual AxisType map_to_domain(double type) const = 0;
  virtual void get_tick_info(std::size_t &count, double &origin, double &inc, std::size_t &maj_step) const
  {
    count = 11;
    origin = map_from_domain(m_domain.first);
    inc = 1.0 / (count - 1);
    maj_step = 5;
  }
  
  double operator()(const AxisType &d) const
  {
    return map_to_range(d);
  }
  
  double map_to_range(const AxisType &d) const
  {
    auto ratio = map_from_domain(d);
    ratio = std::min(ratio, 1.0);
    ratio = std::max(ratio, 0.0);
    
    auto scaled = ratio * m_range_scale;
    return m_range_offset + scaled;
  }
  
  AxisType map_from_range(double range) const
  {
    auto ratio = (range - m_range_offset) / m_range_scale;
    return map_to_domain(ratio);
  }
  
  virtual TickVector get_ticks(const QPointF &axis_displacement) const
  {
    std::size_t count;
    std::size_t maj_step;
    double inc;
    double pos;
    get_tick_info(count, pos, inc, maj_step);
    assert(count < 1000);
    
    TickVector ticks;
    for (std::size_t i = 0; i < count; ++i)
    {
      auto location = map_to_domain(pos);
      ticks.emplace_back(Tick{ pos, format(location), (i % maj_step) == 0 });
      pos += inc;
    }
    
    return ticks;
  }

  const DomainPair &domain() const { return m_domain; }
  
  
  QString format(AxisType type) const
  {
    if (!m_format)
    {
      return QString("?");
    }
    
    return m_format(type, *this);
  }
  
private:
  DomainPair m_domain = {};
  double m_range_offset = {};
  double m_range_scale = {};
  std::function<double(AxisType, const DomainPair &)> m_domain_scale;
  FormatCallback m_format;
};
  
enum class TickLabelPosition
{
  Under,
  Above,
  Left
};

template <typename AxisType> void paint_axis(
  QPainter *painter,
  const AxisType &axis,
  QPointF origin,
  QPointF direction,
  QPointF tick_direction,
  double tick_size,
  TickLabelPosition tick_pos,
  double length)
{
  auto ticks = axis.get_ticks(direction * length);
  paint_axis_lines(
    painter,
    origin,
    direction,
    tick_direction,
    tick_size,
    tick_pos,
    length,
    ticks);
  
}

inline void paint_axis_lines(
  QPainter *painter,
  QPointF origin,
  QPointF direction,
  QPointF tick_direction,
  double tick_size,
  TickLabelPosition tick_pos,
  double length,
  const TickVector &ticks)
{
  auto axis_vector = direction * length;
  
  auto end_point = origin + axis_vector;
  painter->drawLine(origin, end_point);
  
  const auto tick_offset = tick_direction * tick_size;
  auto tick_disp = (axis_vector / detail::pt_length(axis_vector)) * length;
  for (auto &tick : ticks)
  {
    auto tick_location = origin + tick_disp * tick.location;
    auto tick_end = tick_location + tick_offset;
    if (tick.major)
    {
      tick_end += tick_offset;
    }
    painter->drawLine(tick_location, tick_end);
    
    auto text_width = 100;
    auto text_height = 20;
    QRectF rect;
    int text_align = 0;
    if (tick_pos == TickLabelPosition::Under)
    {
      rect = QRectF(
                    tick_end.x() - text_width/2,
                    tick_end.y() - 3,
                    text_width,
                    text_height
                    );
      text_align = Qt::AlignCenter;
    }
    else if (tick_pos == TickLabelPosition::Above)
    {
      rect = QRectF(
                    tick_end.x() - text_width/2,
                    tick_end.y() - text_height,
                    text_width,
                    text_height
                    );
      text_align = Qt::AlignCenter;
    }
    else if (tick_pos == TickLabelPosition::Left)
    {
      rect = QRectF(
        tick_end.x() - text_width - 3,
        tick_end.y() - text_height/2,
        text_width,
        text_height
      );
      text_align = Qt::AlignRight | Qt::AlignVCenter;
    }
    else
    {
      assert(false);
    }
    painter->drawText(rect, text_align, tick.label);
  }
}

template <typename AxisType> class NumberAxis : public Axis<AxisType>
{
public:
  NumberAxis(typename Axis<AxisType>::FormatCallback format)
  : Axis<AxisType>(format)
  {
  }
  
  double map_from_domain(AxisType d) const override
  {
    auto &domain = Axis<AxisType>::domain();
    auto length = domain.second - domain.first;
    auto ratio = d - domain.first;
    return double(ratio) / length;
  }
  
  AxisType map_to_domain(double d) const override
  {
    auto &domain = Axis<AxisType>::domain();
    return domain.first + (domain.second - domain.first) * d;
  }
};

class TimeAxis : public Axis<appulatrix::Time>
{
public:
  using Time = appulatrix::Time;
  TimeAxis(
    const Time &origin = {},
    Axis<Time>::FormatCallback format = {})
  : Axis<Time>(format)
  , m_origin(origin)
  {
  }
  
  double map_from_domain(appulatrix::Time d) const override
  {
    using namespace std::chrono;
    auto ratio = duration_cast<nanoseconds>(d - domain().first);
    return map_from_domain(ratio);
  }
  
  double map_from_domain(std::chrono::nanoseconds ns) const
  {
    using namespace std::chrono;
    auto &domain = Axis<Time>::domain();
    auto length = duration_cast<nanoseconds>(domain.second - domain.first);
    return double(ns.count()) / length.count();
  }
  
  appulatrix::Time map_to_domain(double d) const override
  {
    using namespace std::chrono;
    auto &domain = Axis<Time>::domain();
    auto length = duration_cast<nanoseconds>(domain.second - domain.first).count();
    return domain.first + nanoseconds(std::uint64_t(length * d));
  }
  
  void get_tick_info(std::size_t &count, double &origin, double &inc, std::size_t &maj_step) const override
  {
    using namespace std::chrono;
    auto &domain = Axis<Time>::domain();
    assert(domain.second > domain.first);
    auto range = domain.second - domain.first;
    
    auto round_to = [this](appulatrix::Time t, nanoseconds round) -> double
    {
      auto full_ns = duration_cast<nanoseconds>(t - m_origin).count();
      auto rounded = round * (full_ns / round.count());
      return map_from_domain(m_origin + nanoseconds(rounded));
    };
    
    std::chrono::nanoseconds inc_ns = milliseconds(1);
    
    if (range > seconds(5))
    {
      origin = round_to(domain.first, seconds(1));
      inc_ns = milliseconds(500);
    }
    else if (range > seconds(1))
    {
      origin = round_to(domain.first, seconds(1));
      inc_ns = milliseconds(100);
    }
    else if (range > milliseconds(500))
    {
      origin = round_to(domain.first, milliseconds(500));
      inc_ns = milliseconds(50);
    }
    else if (range > milliseconds(100))
    {
      origin = round_to(domain.first, milliseconds(100));
      inc_ns = milliseconds(10);
    }
    else
    {
      origin = round_to(domain.first, milliseconds(10));
      inc_ns = milliseconds(1);
    }
    
    inc = map_from_domain(inc_ns);
    
    auto length = map_from_domain(domain.second) - origin;
    count = (length / std::abs(inc)) + 1;
    assert(count < 1000);
    
    maj_step = 5;
  }
  
private:
  const Time m_origin;
};


template <typename AxisXType, typename AxisYType>
class Graph : public QGraphicsObject
{
public:
  static const std::size_t AxisCount = 2;

  class Item
  {
  public:
    virtual void paint(
      QPainter *painter,
      const Axis<AxisXType> &x,
      const Axis<AxisYType> &y) = 0;
  };

  Graph(
    QSizeF size,
    const std::shared_ptr<Axis<AxisXType>> &axis_x,
    const std::shared_ptr<Axis<AxisYType>> &axis_y)
  : m_size(size)
  , m_tick_size(5.0)
  , m_x_axis(axis_x)
  , m_y_axis(axis_y)
  {
    m_label_font.setPixelSize(8);
  }

  void set_size(QSizeF size)
  {
    m_size = size;
  }

  QRectF boundingRect() const override
  {
    return QRectF(QPointF(0, 0), m_size);
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override
  {
    auto bnds = boundingRect();
    auto rect = QRectF(QPointF(0, 0), m_size);
    painter->fillRect(rect, Qt::lightGray);
    
    painter->setFont(m_label_font);
    auto origin = bnds.bottomLeft();
    paint_axis(
      painter,
      *m_x_axis,
      origin,
      QPointF(1.0, 0.0),
      QPointF(0, 1),
      m_tick_size,
      TickLabelPosition::Under,
      m_size.width()
    );
    paint_axis(
      painter,
      *m_y_axis,
      origin,
      QPointF(0.0, -1.0),
      QPointF(-1, 0),
      m_tick_size,
      TickLabelPosition::Left,
      m_size.height()
    );
    
    m_x_axis->set_range(origin.x(), origin.x() + m_size.width());
    m_y_axis->set_range(origin.y(), origin.y() - m_size.height());
    
    for (auto item : m_items)
    {
      item->paint(painter, *m_x_axis, *m_y_axis);
    }
  }
  
  Axis<AxisXType> &x_axis() { return *m_x_axis; }
  Axis<AxisYType> &y_axis() { return *m_y_axis; }

  void add_item(const std::shared_ptr<Item> &it)
  {
    m_items.emplace_back(std::move(it));
  }

private:
  QSizeF m_size;
  QFont m_label_font;
  double m_tick_size;
  std::vector<std::shared_ptr<Item>> m_items;

  const std::shared_ptr<Axis<AxisXType>> m_x_axis;
  const std::shared_ptr<Axis<AxisYType>> m_y_axis;
};
}
