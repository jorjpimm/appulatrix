#include "appulatrix/Stack.h"

#include <catch/catch.hpp>

#include <memory>
#include <string>
#include <unordered_map>

namespace appulatrix
{


template <typename Key> class TreeItem
{
public:
  TreeItem() = default;
  TreeItem(TreeItem &&) = default;
  TreeItem &operator=(TreeItem &&) = default;
  
  TreeItem(const TreeItem &) = delete;
  TreeItem &operator=(const TreeItem &) = delete;
  
  template <typename TreeData> void add_stack(
    const TreeData &tree_data,
    std::size_t from_index
  )
  {
    if (from_index >= tree_data.size())
    {
      return;
    }
    
    auto this_entry = tree_data.key(from_index);
    auto &child = find_or_create(this_entry);
    child->add_stack(tree_data, from_index + 1);
  }
  
  const TreeItem *find(
    const Key &key
  ) const
  {
    auto it = m_sub_tree.find(key);
    if (it != m_sub_tree.end())
    {
      return it->second.get();
    }
    return nullptr;
  }
  
  std::unique_ptr<TreeItem> &find_or_create(
    const Key &key
  )
  {
    auto it = m_sub_tree.find(key);
    if (it == m_sub_tree.end())
    {
      m_sub_tree[key] = std::unique_ptr<TreeItem>(new TreeItem);
      return m_sub_tree[key];
    }
    else
    {
      return it->second;
    }
  }
  
  std::size_t size() const { return m_sub_tree.size(); }
  
private:
  std::unordered_map<Key, std::unique_ptr<TreeItem>> m_sub_tree;
};


template <typename TreeData> class StackTree
{
public:
  StackTree()
  {
  }

  void add_stack(const TreeData &entries)
  {
    m_root.add_stack(entries, 0);
  }
  
  const TreeItem<typename TreeData::KeyType> &root()
  {
    return m_root;
  }

private:
  TreeItem<typename TreeData::KeyType> m_root;
};

}

class StackTreeData
{
public:
  using Entry = appulatrix::FormattedStackContainer::StackEntry;
  using KeyType = std::string;
  
  StackTreeData(const std::vector<appulatrix::FormattedStackContainer::StackEntry> &s)
  : m_stack(s)
  {
  }
  
  const Entry &at(std::size_t i) const
  {
    auto index = size() - i - 1;
    assert(index < m_stack.size());
    return m_stack[index];
  }
  
  KeyType key(std::size_t i) const
  {
    return *at(i).symbol_name;
  }
  
  std::size_t size() const { return m_stack.size(); }
  
private:
  const std::vector<appulatrix::FormattedStackContainer::StackEntry> &m_stack;
};

appulatrix::FormattedStackContainer::StackEntry make_stack_entry(
  const char *pc,
  const char *off,
  const char *sym
)
{
  return appulatrix::FormattedStackContainer::StackEntry{
    std::string(pc),
    std::string(off),
    std::string(sym)
  };
}

SCENARIO("Stack Parsing Test", "[stack]")
{
  auto test_stack1 = std::vector<appulatrix::FormattedStackContainer::StackEntry>{
    make_stack_entry("0x02", "0x2", "pork()"),
    make_stack_entry("0x11", "0x1", "pie()"),
    make_stack_entry("0x41", "0x2", "fudge()"),
  };

  auto test_stack2 = std::vector<appulatrix::FormattedStackContainer::StackEntry>{
    make_stack_entry("0x01", "0x1", "sausage()"),
    make_stack_entry("0x10", "0x0", "pie()"),
    make_stack_entry("0x41", "0x2", "fudge()"),
  };

  auto test_stack3 = std::vector<appulatrix::FormattedStackContainer::StackEntry>{
    make_stack_entry("0x00", "0x0", "sausage()"),
    make_stack_entry("0x12", "0x2", "thing()"),
    make_stack_entry("0x41", "0x2", "fudge()"),
  };


  GIVEN("a stack tree")
  {
    appulatrix::StackTree<StackTreeData> tree;

    WHEN("Stack is empty")
    {
      THEN("Stack is default")
      {
        CHECK(tree.root().size() == 0);
        CHECK(!tree.root().find("fudge()"));
      }
    }

    WHEN("Single item is added to stack")
    {
      tree.add_stack(StackTreeData(test_stack1));
      THEN("Stack has 1 item added")
      {
        CHECK(tree.root().size() == 1);
        REQUIRE(tree.root().find("fudge()"));
        REQUIRE(tree.root().find("fudge()")->find("pie()"));
        REQUIRE(tree.root().find("fudge()")->find("pie()")->find("pork()"));
        CHECK(tree.root().find("fudge()")->size() == 1);
        CHECK(tree.root().find("fudge()")->find("pie()")->size() == 1);
        CHECK(tree.root().find("fudge()")->find("pie()")->find("pork()")->size() == 0);
      }
      
      AND_WHEN("More items are added")
      {
        tree.add_stack(StackTreeData(test_stack2));
        tree.add_stack(StackTreeData(test_stack3));

        THEN("Stack has tree built")
        {
          CHECK(tree.root().size() == 1);
          REQUIRE(tree.root().find("fudge()")->find("pie()")->find("sausage()"));
          CHECK(tree.root().find("fudge()")->size() == 2);
          CHECK(tree.root().find("fudge()")->find("pie()")->size() == 2);
          CHECK(tree.root().find("fudge()")->find("pie()")->find("pork()")->size() == 0);
          CHECK(tree.root().find("fudge()")->find("pie()")->find("sausage()")->size() == 0);
          
          CHECK(tree.root().size() == 1);
          REQUIRE(tree.root().find("fudge()")->find("thing()"));
          REQUIRE(tree.root().find("fudge()")->find("thing()")->find("sausage()"));
          CHECK(tree.root().find("fudge()")->size() == 2);
          CHECK(tree.root().find("fudge()")->find("thing()")->size() == 1);
          CHECK(tree.root().find("fudge()")->find("thing()")->find("sausage()")->size() == 0);
        }
      }
    }
  }
}
