#include "appulatrix/Event.h"
#include "appulatrix/Reader.h"
#include "AllocationItem.h"
#include "AllocationDataManager.h"
#include "Graph.h"

#include <QGraphicsView>
#include <QResizeEvent>

class StackIndicator : public AllocationGraphType::Item
{
public:
  StackIndicator(
    const std::shared_ptr<AllocationDataManager> &data)
  : m_allocation_manager(data)
  {
  }

  void set_selected_stack(boost::optional<appulatrix::StackId> stack)
  {
    m_selected_stack = stack;
  }

  void paint(
    QPainter *painter,
    const graph_tools::Axis<appulatrix::Time> &x_axis,
    const graph_tools::Axis<std::size_t> &y_axis) override
  {
    if (m_selected_stack)
    {
      const auto &points = m_allocation_manager->points();
      if (points.empty()) return;

      painter->setPen(Qt::red);

      auto y_min = y_axis(y_axis.domain().first);
      auto y_max = y_axis(y_axis.domain().second);

      const auto stk = *m_selected_stack;
      for (auto &pt : points)
      {
        if (pt.event->stack == stk)
        {
          auto x = x_axis(pt.event->time);
          painter->drawLine(x, y_min, x, y_max);
        }
      }
    }
  }

private:
  std::shared_ptr<AllocationDataManager> m_allocation_manager;
  boost::optional<appulatrix::StackId> m_selected_stack;
};

class DragSelection : public QGraphicsItem
{
public:
  DragSelection()
  {
  }

  QRectF boundingRect() const override
  {
    return painted_bounds;
  }

  void paint(
    QPainter *painter,
    const QStyleOptionGraphicsItem *option,
    QWidget *widget = 0) override
  {
    painter->drawRect(current);
  }

  void set_start(QPointF start)
  {
    current = QRectF(start, QSizeF(0, 0));
    painted_bounds = current;
    update();
  }

  void set_end(QPointF end)
  {
    current.setBottomRight(end);
    painted_bounds = painted_bounds.united(current);
    update();
  }

private:
  QRectF current;
  QRectF painted_bounds;
};

class AllocationGraph : public QGraphicsView
{
public:
  AllocationGraph(
    const std::shared_ptr<AllocationDataManager> &data,
    QWidget *parent
  )
  : QGraphicsView(parent)
  , m_data_manager(data)
  {
    setScene(&m_scene);

    auto format_x = [](
      appulatrix::Time x,
      const graph_tools::Axis<appulatrix::Time> &axis) -> QString
    {
      using namespace std::chrono;
      auto offset = duration_cast<nanoseconds>(x - axis.domain().first);
      double nanoseconds =
        double(nanoseconds::period::den) / double(nanoseconds::period::num);

      std::array<char, 128> buf;
      snprintf(buf.data(), buf.size(), "%.2f s", double(offset.count())/nanoseconds);

      return buf.data();
    };

    auto format_y = [](
      std::size_t x,
      const graph_tools::Axis<std::size_t> &axis) -> QString
    {
      auto kilo = 1000;

      std::array<char, 128> buf;
      snprintf(buf.data(), buf.size(), "%.1f KB", double(x)/kilo);

      return buf.data();
    };

    m_graph = new AllocationGraphType(
      QSizeF(0, 0),
      std::make_shared<graph_tools::TimeAxis>(m_data_manager->start_time(), format_x),
      std::make_shared<graph_tools::NumberAxis<std::size_t>>(format_y)
    );
    auto allocation_item = std::make_shared<AllocationItem>(m_data_manager);
    m_graph->add_item(allocation_item);

    m_selected_stacks = std::make_shared<StackIndicator>(m_data_manager);
    m_graph->add_item(m_selected_stacks);

    QObject::connect(
      m_data_manager.get(),
      &AllocationDataManager::data_added,
      [this]()
      {
        m_graph->x_axis().set_domain(
          m_data_manager->start_time(),
          m_data_manager->end_time()
        );
        m_graph->y_axis().set_domain(
          0,
          m_data_manager->maximum_allocated_bytes()
        );

        m_graph->update();
      }
    );

    relocate_graph(size());
    m_scene.addItem(m_graph);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  }

  void set_selected_stack(boost::optional<appulatrix::StackId> stack)
  {
    m_selected_stacks->set_selected_stack(stack);
    m_graph->update();
  }

protected:
  void mousePressEvent(QMouseEvent *event) override
  {
    m_drag = std::make_shared<DragSelection>();
    m_scene.addItem(m_drag.get());
    m_drag->set_start(mapToScene(event->pos()));
  }

  void mouseMoveEvent(QMouseEvent *event) override
  {
    if (m_drag)
    {
      m_drag->set_end(mapToScene(event->pos()));
    }
  }

  void mouseReleaseEvent(QMouseEvent *event) override
  {
    m_drag = nullptr;
  }

  void relocate_graph(QSizeF size)
  {
    m_graph->setPos(mapToScene(50, 20));
    m_graph->set_size(QSizeF(size.width() - 70, size.height() - 70));
  }

  void resizeEvent(QResizeEvent *event) override
  {
    relocate_graph(size());
  }

  void showEvent(QShowEvent *event) override
  {
    relocate_graph(size());
  }

private:
  std::shared_ptr<AllocationDataManager> m_data_manager;
  QGraphicsScene m_scene;
  AllocationGraphType *m_graph;
  std::shared_ptr<StackIndicator> m_selected_stacks;
  std::shared_ptr<DragSelection> m_drag;
};
