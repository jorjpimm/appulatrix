#pragma once

#include "appulatrix/Time.h"

#include <chrono>
#include <functional>
#include <QWidget>

class QNativeGestureEvent;

class TimeRange
{
public:
  using Time = appulatrix::Time;

  TimeRange();
  TimeRange(const std::pair<Time, Time> &);
  TimeRange(const Time &begin, const Time &end);

  double time_to_ratio(const Time &t) const;
  Time ratio_to_time(double) const;

  double ratio_of_duration(const std::chrono::nanoseconds &ns) const;

  std::chrono::nanoseconds duration() const { return m_range.second - m_range.first; }

  const Time &begin() const { return m_range.first; }
  const Time &end() const { return m_range.second; }

private:
  std::pair<Time, Time> m_range;
};

class VisibleTimeRange
{
public:
  VisibleTimeRange();
  VisibleTimeRange(const TimeRange &domain, const TimeRange &visible);

  void pan_visible(double domain_delta);
  void zoom_visible(double domain_zoom_centre, double factor);

  const TimeRange &domain() const { return m_domain; }
  const TimeRange &visible() const { return m_visible; }

  double visible_ratio() const 
  { 
    return double(m_visible.duration().count()) / double(m_domain.duration().count()); 
  }

private:
  TimeRange m_visible;
  TimeRange m_domain;
};

class TimelineCore
{
public:
  enum class DragMode
  {
    DragDomain,
    DragVisible
  };

  TimelineCore(
    DragMode drag_mode);

  void set_range(const VisibleTimeRange &domain);
  const VisibleTimeRange &range() const { return m_range; }

  using Transform = std::function<double(int)>;
  bool accept_event(QEvent *event, const Transform &x_scaler);

private:
  bool nativeGestureEvent(QNativeGestureEvent *wheel, const Transform &x_scaler);
  bool wheelEvent(QWheelEvent *wheel, const Transform &x_scaler);
  bool mousePressEvent(QMouseEvent *event, const Transform &x_scaler);
  bool mouseMoveEvent(QMouseEvent *event, const Transform &x_scaler);
  bool mouseReleaseEvent(QMouseEvent *event, const Transform &x_scaler);

private:
  VisibleTimeRange m_range;
  DragMode m_drag_mode;

  bool m_dragging;
  QPoint m_last_pos;
};
