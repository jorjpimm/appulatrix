#pragma once

#include "appulatrix/EventProvider.h"

#include <QTreeWidget>

struct SetTypesEvent
{
  SetTypesEvent(
    std::size_t stack_count)
  : stack_count(appulatrix::StackId(stack_count))
  {
  }
  appulatrix::StackId stack_count;
};

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, SetTypesEvent)
IMPLEMENT_TYPE_BUILDER(SetTypesEvent, type)
{
  type
    .add("stack_count", &SetTypesEvent::stack_count);
}

class TypeManager : public QTreeWidget
{
public:

  TypeManager(QWidget *parent)
  : QTreeWidget(parent)
  , m_event_provider(appulatrix::EventProvider::create_top_level("type_manager"))
  {
    setHeaderLabels(QStringList() << "Id" << "Name" << "Size");
  }

  void set_types(const appulatrix::TypeLookup &types)
  {
    auto b = appulatrix::emit_scoped_event<SetTypesEvent>(
      m_event_provider, 
      types.type_list().size()
    );

    clear();
    
    for (auto &&type : types.type_list())
    {      
      auto type_item = new QTreeWidgetItem(this);
      type_item->setText(0, QString::number(type->id()));
      type_item->setText(1, type->name().get());
      type_item->setText(2, QString::number(type->size()));
      addTopLevelItem(type_item);
    }
  }

private:
  appulatrix::EventProvider m_event_provider;
};
