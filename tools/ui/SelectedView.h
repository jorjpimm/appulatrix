#pragma once

#include "SelectionContainer.h"

#include <QWidget>

class SelectedView : public QWidget
{
public:
  SelectedView(
    std::shared_ptr<SelectionContainer>,
    QWidget *parent
  )
  {
  }
};
