#include "Timeline.h"
#include "SelectionContainer.h"
#include "TimelineEventSurface.h"
#include "TimelineOverview.h"
#include "TimelineScale.h"
#include "appulatrix/EventProvider.h"

#include <QCheckBox>
#include <QEvent>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QPushButton>
#include <QScrollArea>
#include <QSplitter>


TimelineLabelControls::TimelineLabelControls(
  QWidget *parent,
  QString name)
: QWidget(parent)
, m_is_selected(false)
{
  auto main_layout = new QHBoxLayout(this);

  auto enabled = new QCheckBox();
  main_layout->addWidget(enabled);
  enabled->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  connect(enabled, SIGNAL(toggled(bool)), this, SIGNAL(on_enabled(bool)));

  m_expanded = new QPushButton();
  m_expanded->setCheckable(true);
  m_expanded->setMaximumHeight(20);
  m_expanded->setMaximumWidth(20);
  main_layout->addWidget(m_expanded);
  connect(m_expanded, SIGNAL(toggled(bool)), this, SIGNAL(on_expanded(bool)));

  m_label = new QLabel(name);
  main_layout->addWidget(m_label);

  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  set_expandable(false);
}

void TimelineLabelControls::set_expandable(bool expandable)
{
  m_expanded->setVisible(expandable);
}

bool TimelineLabelControls::is_expanded() const
{
  return m_expanded->isChecked();
}

void TimelineLabelControls::set_selected(bool is_selected)
{
  m_is_selected = is_selected;
  update();
}

void TimelineLabelControls::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);

  auto round_size = 5;
  auto line_width = 2;
  QPen outline_pen(Qt::black);
  outline_pen.setWidth(line_width);
  painter.setPen(outline_pen);

  auto widget_bounds = QRectF(0, 0, width(), height());
  auto bounds = widget_bounds.adjusted(0, 0, round_size, 0);
  auto adjust = line_width / 2;
  bounds.adjust(adjust, adjust, -adjust, -adjust);


  QColor col(Qt::white);
  if (m_is_selected)
  {
    col = col.darker();
  }
  painter.setBrush(col);
  painter.drawRoundedRect(bounds, round_size, round_size);

  painter.setPen(Qt::black);
  painter.drawLine(widget_bounds.topRight(), widget_bounds.bottomRight());
}

TimelineLabel::TimelineLabel(
  const appulatrix::EventProviderData *provider,
  TimelineLabel *parent_label,
  QWidget *parent)
: QWidget(parent)
, m_provider(provider)
, m_parent_label(parent_label)
, m_row(nullptr)
, m_controls(provider ? new TimelineLabelControls(this, provider->name().get()) : nullptr)
{
  setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

  if (m_controls)
  {
    connect(m_controls, &TimelineLabelControls::on_expanded, [this](bool expanded)
    {
      set_expanded(expanded);
    });
  }

  set_expanded(is_expanded());
}

void TimelineLabel::init_pair(TimelineLabel *l, TimelineRow *r)
{
  l->m_row = r;
  r->m_label = l;
}

TimelineLabel *TimelineLabel::add_label_and_row(
  const appulatrix::EventProviderData *provider,
  TimelineEventSurface *surface)
{
  if (m_controls)
  {
    m_controls->set_expandable(true);
  }

  auto new_label = new TimelineLabel(provider, this, this);
  auto new_row = new TimelineRow(m_row, surface);
  
  init_pair(new_label, new_row);

  set_expanded(is_expanded());
  updateGeometry();
  new_row->updateGeometry();
  return new_label;
}

bool TimelineLabel::event(QEvent *e)
{
  if (e->type() == QEvent::LayoutRequest)
  {
    layout_children();
  }

  return QWidget::event(e);
}

void TimelineLabel::refresh_selection(const Selection &sel, bool surface_has_any)
{
  if (m_controls)
  {
    m_controls->set_selected(surface_has_any);
  }
}

QSize TimelineLabel::minimumSizeHint() const
{
  return sizeHint();
}

QSize TimelineLabel::sizeHint() const
{
  auto controls_size = selfSizeHint();
  auto y = controls_size.height();

  if (is_expanded())
  {
    for (auto child : children())
    {
      if (auto label = qobject_cast<TimelineLabel*>(child))
      {
        auto size_hint = label->sizeHint();
        y += LabelPadding + size_hint.height();
      }
    }
  }
  
  return QSize(100, y);
}

QSize TimelineLabel::selfSizeHint() const
{
  if (m_controls)
  {
    return m_controls->sizeHint();
  }

  return QSize(0, 0);
}

void TimelineLabel::resizeEvent(QResizeEvent *event)
{
  layout_children();
}

void TimelineLabel::set_expanded(bool expanded)
{
  for (auto child : children())
  {
    if (auto label = qobject_cast<TimelineLabel*>(child))
    {
      label->setVisible(expanded);
      label->m_row->setVisible(expanded);
    }
  }

  auto label_ancestor = this;
  while (label_ancestor)
  {
    label_ancestor->updateGeometry();
    label_ancestor = label_ancestor->m_parent_label;
  }

  auto label_row_ancestor = m_row;
  while (label_row_ancestor)
  {
    label_row_ancestor->updateGeometry();
    label_row_ancestor = qobject_cast<TimelineRow*>(label_row_ancestor->parentWidget());
  }
}

bool TimelineLabel::is_expanded() const
{
  return m_controls ? m_controls->is_expanded() : true;
}

void TimelineLabel::layout_children()
{
  auto controls_size = selfSizeHint();
  auto used_y = controls_size.height();
  if (m_controls)
  {
    m_controls->move(0, 0);
    m_controls->resize(width(), controls_size.height());
  }
  
  for (auto child : children())
  {
    if (auto label = qobject_cast<TimelineLabel*>(child))
    {
      auto step_in = m_controls ? ChildStepIn : 0;
      auto size_hint = label->sizeHint();
      used_y += LabelPadding;
      label->move(step_in, used_y);
      used_y += size_hint.height();
      label->resize(QSize(width() - step_in, size_hint.height()));

      label->m_row->updateGeometry();
    }
  }
}

TimelineRow::TimelineRow(
  QWidget *parent,
  TimelineEventSurface *surface)
: QWidget(parent)
, m_label(nullptr)
, m_surface(surface)
{
  m_surface->setParent(this);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

void TimelineRow::layout_children()
{
  auto controls_size = m_label->selfSizeHint();
  m_surface->move(0, 0);
  m_surface->resize(width(), controls_size.height());
  auto used_y = controls_size.height();

  for (auto child : children())
  {
    if (auto row = qobject_cast<TimelineRow*>(child))
    {
      auto size_hint = row->m_label->sizeHint();
      used_y += TimelineLabel::LabelPadding;
      row->move(0, used_y);
      used_y += size_hint.height();
      row->resize(QSize(width(), size_hint.height()));
    }
  }
}

void TimelineRow::resizeEvent(QResizeEvent *event)
{
  layout_children();
}

QSize TimelineRow::minimumSizeHint() const
{
  return sizeHint();
}

QSize TimelineRow::sizeHint() const
{
  auto controls_size = m_label->selfSizeHint();
  auto y = controls_size.height();
  
  for (auto child : children())
  {
    if (auto row = qobject_cast<TimelineRow*>(child))
    {
      auto size_hint = row->m_label->sizeHint();
      y += TimelineLabel::LabelPadding + size_hint.height();
    }
  }
  
  return QSize(100, y);
}

Timeline::Timeline(
  QWidget *parent,
  const std::shared_ptr<TimelineEventSurfaceFactory> &factory,
  const std::shared_ptr<SelectionContainer> &selection)
: QWidget(parent)
, m_event_surface_factory(factory)
, m_selection(selection)
{
  auto main_layout = new QVBoxLayout(this);
  main_layout->setContentsMargins(0, 0, 0, 0);
  auto scroll_area = new QScrollArea();
  scroll_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  scroll_area->setWidgetResizable(true);
  main_layout->addWidget(scroll_area);
  
  auto main_splitter = new QSplitter(scroll_area);
  main_splitter->setChildrenCollapsible(false);
  scroll_area->setWidget(main_splitter);
  

  auto labels_widget = new QWidget(this);
  labels_widget->setMinimumWidth(100);
  main_splitter->addWidget(labels_widget);
  auto rows_widget = new QWidget(this);
  rows_widget->setBackgroundRole(QPalette::Dark);
  rows_widget->setAutoFillBackground(true);
  main_splitter->addWidget(rows_widget);
  main_splitter->setSizes(QList<int>() << 150 << 850);

  auto labels_main_layout = new QVBoxLayout(labels_widget);
  labels_main_layout->setContentsMargins(0, 0, 0, 0);
  labels_main_layout->setSpacing(2);
  auto rows_main_layout = new QVBoxLayout(rows_widget);
  rows_main_layout->setContentsMargins(0, 0, 0, 0);
  rows_main_layout->setSpacing(2);

  m_overview = new TimelineOverview(labels_widget);
  labels_main_layout->addWidget(m_overview);
  m_scale = new TimelineScale(rows_widget);
  rows_main_layout->addWidget(m_scale);
  
  connect(m_overview, &TimelineOverview::visible_changed,
    [this](const TimeRange &visible) {
      set_visible_domain(visible);
    }
  );
  connect(m_scale, &TimelineScale::visible_changed,
    [this](const TimeRange &visible) {
      set_visible_domain(visible);
    }
  );
  

  auto root_row = new TimelineRow(
    rows_widget,
    m_event_surface_factory->make_surface(nullptr)
  );
  rows_main_layout->addWidget(root_row);
  rows_main_layout->addStretch(0);

  auto root_label = new TimelineLabel(nullptr, nullptr, labels_widget);
  labels_main_layout->addWidget(root_label);
  labels_main_layout->addStretch(0);

  TimelineLabel::init_pair(root_label, root_row);

  m_providers[RootId] = ProviderData{ root_label };
  
  
  connect(m_selection.get(), &SelectionContainer::selection_changed,
    [this]() {
      refresh_selection_display();
    }
  );
}

void Timeline::add_provider(const appulatrix::EventProviderData *provider)
{
  if (m_providers.find(provider->id()) != m_providers.end())
  {
    return;
  }

  auto parent = m_providers.find(provider->parent());
  if (parent == m_providers.end())
  {
    std::cerr << "Invaild parent found!" << std::endl;
    parent = m_providers.find(RootId);
  }
  
  auto surface = m_event_surface_factory->make_surface(provider);
  auto parent_label = parent->second.label;
  auto label = parent_label->add_label_and_row(provider, surface);
  m_providers[provider->id()] = ProviderData{ label, surface, provider->parent() };

  connect(surface, &TimelineEventSurface::visible_changed,
    [this](const TimeRange &visible) {
      set_visible_domain(visible);
    }
  );
  
  connect(surface, &TimelineEventSurface::selection_changed,
    [this, surface](const Selection &new_sel) {
      m_selection->set_selection(new_sel);
    }
  );
  
  surface->set_range(m_overview->range());
}

void Timeline::set_domain(const TimeRange &domain)
{
  auto range = VisibleTimeRange(domain, m_overview->range().visible());
  m_overview->set_range(range);
  m_scale->set_range(range);

  for (auto prov : m_providers)
  {
    prov.second.label->surface()->set_range(range);
  }
}

void Timeline::set_visible_domain(const TimeRange &visible)
{
  auto range = VisibleTimeRange(m_overview->range().domain(), visible);
  m_overview->set_range(range);
  m_scale->set_range(range);

  for (auto prov : m_providers)
  {
    prov.second.label->surface()->set_range(range);
  }
}

void Timeline::add_event(const appulatrix::Event &ev)
{
  auto prov = m_providers.find(ev.provider);
  if (prov == m_providers.end())
  {
    std::cerr << "Missing provider!!!" << std::endl;
    prov = m_providers.find(RootId);
  }
  
  prov->second.surface->add_event(ev);
  
  auto parent = m_providers.find(prov->second.parent);
  while(parent != m_providers.end())
  {
    if (parent->second.surface)
    {
      parent->second.surface->add_child_event(ev);
    }
    
    if (parent->second.parent == RootId)
    {
      break;
    }
    parent = m_providers.find(parent->second.parent);
  }
}

void Timeline::refresh_selection_display()
{
  auto &&current_selection = m_selection->get_selection();
  for (auto prov : m_providers)
  {
    if (prov.second.surface)
    {
      prov.second.surface->refresh_selection(current_selection);
      
      bool has_any_selected = prov.second.surface->has_any_selected();
      prov.second.label->refresh_selection(current_selection, has_any_selected);
    }
  }
}

