#include "appulatrix/Event.h"
#include "appulatrix/Reader.h"

#include "AllocationGraph.h"
#include "EventSurfaceFactoryImpl.h"
#include "SelectedView.h"
#include "SelectionContainer.h"
#include "StackManager.h"
#include "Timeline.h"
#include "TimelineEventSurface.h"
#include "TypeManager.h"

#include <QApplication>
#include <QDockWidget>
#include <QMainWindow>

#include <iostream>

class MainWindow : public QMainWindow
{
public:
  MainWindow(
    const boost::filesystem::path &root
  )
  : m_reader(std::make_shared<appulatrix::Reader>(root))
  , m_event_surface_factory(std::make_shared<EventSurfaceFactoryImpl>(m_reader->types()))
  {
    resize(2000, 1000);

    m_selection = std::make_shared<SelectionContainer>();
    
#if 0
    {
      auto dock = new QDockWidget("Allocations", this);
      allocation_graph = new AllocationGraph(m_data_manager, dock);
      dock->setWidget(allocation_graph);
      addDockWidget(Qt::LeftDockWidgetArea, dock);
    }
#endif

    {
      auto dock = new QDockWidget("Selection", this);
      m_selection_view = new SelectedView(m_selection, dock);
      dock->setWidget(m_selection_view);
      addDockWidget(Qt::LeftDockWidgetArea, dock);
    }

    {
      auto dock = new QDockWidget("Timeline", this);
      m_timeline = new Timeline(dock, m_event_surface_factory, m_selection);
      dock->setWidget(m_timeline);
      addDockWidget(Qt::LeftDockWidgetArea, dock);
    }

    {
      auto dock = new QDockWidget("Types", this);
      m_types = new TypeManager(dock);
      dock->setWidget(m_types);
      addDockWidget(Qt::RightDockWidgetArea, dock);
    }

    {
      auto dock = new QDockWidget("Stacks", this);
      m_stacks = new StackManager(m_reader, dock);
      dock->setWidget(m_stacks);
      addDockWidget(Qt::RightDockWidgetArea, dock);
    }


    read_all_items();
  }

  void read_all_items()
  {
    boost::optional<std::pair<appulatrix::Time, appulatrix::Time>> time_range;
    while (true)
    {
      auto read_items = m_reader->read();
      if (read_items.empty())
      {
        break;
      }

      if (!time_range)
      {
        time_range = std::make_pair(read_items[0].time, read_items[read_items.size() - 1].time);
        m_timeline->set_domain(*time_range);
      }
      time_range->second = read_items[read_items.size() - 1].time;

      auto &types = m_reader->types();
      for (auto &item : read_items)
      {
#if 0
        auto do_alloction = [&](const appulatrix::Event &ev, const appulatrix::Type &type, const void *data)
        {
          auto ptr = type.get_field_value<std::uint64_t>(types, "pointer", data);
          auto size = type.get_field_value<std::uint64_t>(types, "size", data);
          m_data_manager->add_allocation(ev, ptr, size);
        };

        auto do_dealloction = [&](const appulatrix::Event &ev, const appulatrix::Type &type, const void *data)
        {
          auto ptr = type.get_field_value<std::uint64_t>(types, "pointer", data);
          m_data_manager->add_deallocation(ev, ptr);
        };
#endif

        auto &&type = types.find_type(item.type);
        if (type.id() == appulatrix::BuiltInTypes::Indices::EventProvider)
        {
          auto data = static_cast<const appulatrix::EventProviderData *>(item.data.get());
          m_timeline->add_provider(data);
        }
        else
        {
          m_timeline->add_event(item);
        }

#if 0
        if (type.name() == "Allocation")
        {
          do_alloction(item, type, item.data.get());
        }
        else if (type.name() == "Deallocation")
        {
          do_dealloction(item, type, item.data.get());
        }
        else if (type.name() == "Reallocation")
        {
          auto dealloc = type.find_field("dealloc");
          do_dealloction(item, types.find_type(dealloc->type()), dealloc->get_data_ptr(item.data.get()));

          auto alloc = type.find_field("alloc");
          do_dealloction(item, types.find_type(alloc->type()), alloc->get_data_ptr(item.data.get()));
        }
#endif
      }
    }

    // Update types in type display
    auto &types = m_reader->types();
    m_types->set_types(types);
    m_stacks->refresh_stacks();

    if (time_range)
    {
      m_timeline->set_domain(*time_range);
      m_timeline->set_visible_domain(*time_range);
    }
  }

private:
  std::shared_ptr<appulatrix::Reader> m_reader;
  std::shared_ptr<EventSurfaceFactoryImpl> m_event_surface_factory;
  Timeline *m_timeline;
  StackManager *m_stacks;
  TypeManager *m_types;
  SelectedView *m_selection_view;
  std::shared_ptr<SelectionContainer> m_selection;
};

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  if (argc < 2)
  {
    std::cerr << "No file passed to open" << std::endl;
    return EXIT_FAILURE;
  }

  MainWindow m(argv[1]);
  m.showMaximized();
  return app.exec();
}
