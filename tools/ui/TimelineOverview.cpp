#include "TimelineOverview.h"

#include <QCursor>
#include <QNativeGestureEvent>
#include <QPainter>
#include <QWheelEvent>

TimelineOverview::TimelineOverview(
  QWidget *parent)
: QWidget(parent)
, m_timeline(TimelineCore::DragMode::DragVisible)
{
  setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

  setBackgroundRole(QPalette::Dark);
  setAutoFillBackground(true);
  
  auto c = cursor();
  c.setShape(Qt::OpenHandCursor);
  setCursor(c);
}

QSize TimelineOverview::sizeHint() const
{
  return QSize(50, 50);
}

void TimelineOverview::set_range(const VisibleTimeRange &range)
{
  m_timeline.set_range(range);
  update();
}

bool TimelineOverview::event(QEvent *e)
{
  auto transform = [&](int x) {
    return double(x) / double(width());
  };

  if (m_timeline.accept_event(e, transform))
  {
    emit visible_changed(m_timeline.range().visible());
    update();
    return true;
  }

  return QWidget::event(e);
}

void TimelineOverview::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);

  using namespace std::chrono;

  auto duration_to_string = [](const nanoseconds &dur)
  {
    auto s = duration_cast<milliseconds>(dur).count() / 1000.0;
    return QString("%1s").arg(QString::number(s, 'g', 3));
  };

  const auto line_width = 2;
  const auto text_padding = 3;

  auto half_line_width = line_width/2;
  auto vis_rect = visible_rect();
  QRect rect = vis_rect.adjusted(half_line_width, half_line_width, -half_line_width, -half_line_width);

  painter.setPen(palette().color(QPalette::WindowText));
  auto duration_s = duration_to_string(range().domain().duration());
  painter.drawText(QRect(text_padding, text_padding, 50, 25), duration_s);

  QPen pen(Qt::red, line_width);
  painter.setPen(pen);
  QBrush brush(QColor(255, 0, 0, 64));
  painter.setBrush(brush);
  painter.drawRect(rect);

  painter.setPen(QColor(128, 0, 0));
  auto visible_s = duration_to_string(range().visible().duration());
  painter.drawText(QRect(vis_rect.x() + text_padding, 25, 50, 25-text_padding), visible_s);
}

QRect TimelineOverview::visible_rect() const
{
  auto b = range().domain().time_to_ratio(range().visible().begin());
  auto w = range().domain().time_to_ratio(range().visible().end()) - b;

  return QRect(b * width(), 0, w * width(), height());
}