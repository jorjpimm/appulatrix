#pragma once

#include "TimelineCore.h"
#include "appulatrix/Event.h"
#include "appulatrix/types.h"
#include "appulatrix/Time.h"

#include <QVBoxLayout>
#include <QGraphicsView>
#include <QWidget>

#include <memory>
#include <unordered_map>

class QLabel;
class QPushButton;

namespace appulatrix
{
class Event;
class EventProviderData;
}

class Selection;
class SelectionContainer;

class TimelineEventSurface;
class TimelineEventSurfaceFactory;
class TimelineLabel;
class TimelineLabelControls;
class TimelineOverview;
class TimelineRow;
class TimelineScale;

class TimelineRow : public QWidget
{
  Q_OBJECT
public:
  TimelineRow(
    QWidget *parent,
    TimelineEventSurface *surface);

  QSize minimumSizeHint() const override;
  QSize sizeHint() const override;

  TimelineEventSurface *surface()
  {
    return m_surface;
  }

protected:
  void resizeEvent(QResizeEvent *event) override;

private:
  void layout_children();

  TimelineLabel *m_label;
  TimelineEventSurface *m_surface;
  friend class TimelineLabel;
};

class TimelineLabelControls : public QWidget
{
  Q_OBJECT
public:
  TimelineLabelControls(
    QWidget *parent,
    QString name);
  
  void set_expandable(bool expandable);
  bool is_expanded() const;

  void set_selected(bool is_selected);

signals:
  void on_enabled(bool enabled);
  void on_expanded(bool expanded);

protected:
  void paintEvent(QPaintEvent *event) override;
  
  QLabel *m_label;
  QPushButton *m_expanded;
  bool m_is_selected;
};


class TimelineLabel : public QWidget
{
  Q_OBJECT
public:
  enum {
    LabelPadding = 2,
    ChildStepIn = 20
  };
  
  TimelineLabel(
    const appulatrix::EventProviderData *provider,
    TimelineLabel *parent_label,
    QWidget *parent);

  TimelineLabel *add_label_and_row(
    const appulatrix::EventProviderData *provider,
    TimelineEventSurface *surface);
  bool event(QEvent *e) override;
  
  TimelineEventSurface *surface()
  {
    return m_row->surface();
  }
  
  void refresh_selection(const Selection &sel, bool surface_has_any);

  QSize minimumSizeHint() const override;
  QSize sizeHint() const override;
  QSize selfSizeHint() const;
  
  static void init_pair(TimelineLabel *, TimelineRow *);
    
protected:
  void resizeEvent(QResizeEvent *event) override;
  
private:
  void set_expanded(bool);
  bool is_expanded() const;
  void layout_children();

  const appulatrix::EventProviderData *m_provider;
  TimelineLabel *m_parent_label;
  TimelineRow *m_row;
  
  TimelineLabelControls *m_controls;
};

class Timeline : public QWidget
{
public:
  using Time = appulatrix::Time;

  enum {
     RootId = 0
  };
  
  Timeline(
    QWidget *parent,
    const std::shared_ptr<TimelineEventSurfaceFactory> &factory,
    const std::shared_ptr<SelectionContainer> &selection);

  void add_provider(const appulatrix::EventProviderData *provider);

  void set_domain(const TimeRange &domain);
  void set_visible_domain(const TimeRange &domain);
  
  void add_event(const appulatrix::Event &ev);

private:
  void refresh_selection_display();
  
  struct ProviderData
  {
    TimelineLabel *label;
    TimelineEventSurface *surface;
    appulatrix::EventProviderId parent;
  };
  std::unordered_map<appulatrix::EventProviderId, ProviderData> m_providers;

  TimelineOverview *m_overview;
  TimelineScale *m_scale;
  std::shared_ptr<TimelineEventSurfaceFactory> m_event_surface_factory;
  
  std::shared_ptr<SelectionContainer> m_selection;
};
