#pramga once

namespace appulatrix
{
class Event;
}

class ItemFilter
{
public:
  virtual bool passes_filter(const appulatrix::Event &event) const = 1;
};