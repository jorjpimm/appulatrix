#pragma once

#include "TimelineEventSurface.h"
#include "AllocationEventDisplaySurface.h"
#include "BasicEventDisplaySurface.h"

class EventSurfaceFactoryImpl : public TimelineEventSurfaceFactory
{
public:
  EventSurfaceFactoryImpl(
    appulatrix::TypeLookup &types)
  : m_types(types)
  {
  }

  TimelineEventSurface *make_surface(const appulatrix::EventProviderData *prov) override
  {
    if (prov && prov->name() == "allocations")
    {
      return new AllocationEventDisplaySurface(m_types);
    }
    
    return new BasicEventDisplaySurface(m_types);
  }

  appulatrix::TypeLookup &m_types;
};
