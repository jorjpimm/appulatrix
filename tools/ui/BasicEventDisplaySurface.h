#pragma once

#include "TimelineEventSurface.h"
#include "Selection.h"

#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventProvider.h"

#include <unordered_map>
#include <unordered_set>

class BasicEventDisplaySurface : public TimelineEventSurface
{
public:
  class CoreEvent : public QGraphicsItem
  {
  public:
    CoreEvent(TimelineEventSurface *surface, const appulatrix::Event *ev)
    : m_surface(surface)
    , m_primary_event(ev)
    {
    }
    
    const appulatrix::Event *selection_event() const { return m_primary_event; }
    
    double timeline_height() const { return m_surface->height(); }
    
    double single_pixel_width() const
    {
      return 1.0 / sceneTransform().m11();
    }
    
    QColor colour() const
    {
      return m_colour;
    }
    
    void set_colour(QColor col)
    {
      m_colour = col;
    }
    
  private:
    QColor m_colour;
    TimelineEventSurface *m_surface;
    const appulatrix::Event *m_primary_event;
  };
  
  class BasicEvent : public CoreEvent
  {
  public:
    const std::size_t tri_size = 10;
    BasicEvent(TimelineEventSurface *surface, const appulatrix::Event *ev)
    : CoreEvent(surface, ev)
    {
      setFlag(QGraphicsItem::ItemIsSelectable);
    }

    QRectF boundingRect() const override
    {
      auto pixel = single_pixel_width();
      return QRectF(0, 1, pixel * tri_size, timeline_height()-1);
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) override
    {
      auto col = colour();
      if (isSelected())
      {
        col = col.lighter();
      }
      
      const auto pixel_width = single_pixel_width();
      QPen p(col, pixel_width);
      painter->setPen(p);
      painter->drawLine(0, 0, 0, timeline_height());
      
      QPainterPath path;
      path.moveTo(0, 0);
      path.lineTo(pixel_width*tri_size, tri_size);
      path.lineTo(0, tri_size);
      
      painter->translate(pixel_width, 0);
      painter->fillPath(path, col.lighter());
      
      painter->translate(-pixel_width, 0);
      painter->fillPath(path, col);
    }
  };

  class BasicScopedEvent : public CoreEvent
  {
  public:
    BasicScopedEvent(TimelineEventSurface *surface, const appulatrix::Event *ev)
    : CoreEvent(surface, ev)
    {
      setFlag(QGraphicsItem::ItemIsSelectable);
    }
    
    double x1 = 0.0;
    double x2 = 0.0;

  private:
    QRectF boundingRect() const override
    {
      return QRectF(0, 0, x2-x1, timeline_height());
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) override
    {
      QColor col = colour();
      if (isSelected())
      {
        col = col.lighter();
      }
      
      painter->setPen(Qt::transparent);
      painter->setBrush(col.lighter());
      
      const auto contract_amt = 1.5;
      const auto x_pixel = single_pixel_width();
      
      QRectF event(0, 0, x2-x1, timeline_height());
      painter->drawRect(event);
      
      
      auto raised = event.adjusted(
        contract_amt*x_pixel,
        contract_amt,
        -contract_amt*x_pixel,
        -contract_amt
      );
      
      painter->setBrush(col);
      painter->drawRect(raised);
    }
  };

  BasicEventDisplaySurface(
    appulatrix::TypeLookup &types)
  : m_types(types)
  {
    connect(scene(), &QGraphicsScene::selectionChanged,
      [this]()
      {
        if (m_ignore_selection_changes)
        {
          return;
        }
        
        Selection new_sel;
        if (!scene()->selectedItems().isEmpty())
        {
          auto item = scene()->selectedItems()[0];
          new_sel.set_selected(static_cast<CoreEvent*>(item)->selection_event(), true);
        }
        emit selection_changed(new_sel);
      }
    );
  }
  
  ~BasicEventDisplaySurface()
  {
    // Don't care about selection when were destroying
    m_ignore_selection_changes = true;
  }

  void add_event(const appulatrix::Event &ev) override
  {
    auto &&type = m_types.find_type(ev.type);
    if (auto ifc = type.find_interface(ev.data.get(), appulatrix::TypeId(appulatrix::BuiltInTypes::Indices::EventBlockBegin)))
    {
      auto data = static_cast<const appulatrix::EventBlockBegin *>(*ifc);
      auto scoped_event = new BasicScopedEvent(this, &ev);
      scoped_event->x1 = scale_x(ev.time);
      scoped_event->set_colour(color_for_type(ev.type));
      add_event_at_time(ev, scoped_event);
      m_ongoing_events[data->m_id] = scoped_event;
    }
    else if (auto ifc = type.find_interface(ev.data.get(), appulatrix::TypeId(appulatrix::BuiltInTypes::Indices::EventBlockEnd)))
    {
      auto data = static_cast<const appulatrix::EventBlockEnd *>(*ifc);
      auto previous_event = m_ongoing_events.find(data->m_id);
      if (previous_event != m_ongoing_events.end())
      {
        previous_event->second->x2 = scale_x(ev.time);
        m_ongoing_events.erase(previous_event);
      }
      else
      {
        add_tick_event(ev);        
      } 
    }
    else
    {
      add_tick_event(ev);
    } 
  }

  void add_tick_event(const appulatrix::Event &ev)
  {
    auto event = new BasicEvent(this, &ev);
    event->set_colour(color_for_type(ev.type));
    add_event_at_time(ev, event);
  }

  void add_event_at_time(const appulatrix::Event &ev, CoreEvent *item)
  {
    auto tx = QTransform().translate(scale_x(ev.time), 0);
    item->setTransform(tx);
    add_item(item);
    m_event_lookup[&ev] = item;
  }
  
  void refresh_selection(const Selection &selection) override
  {
    m_ignore_selection_changes = true;
    auto fin = gsl::finally([&] { m_ignore_selection_changes = false; });
    m_selected = selection.query_selected(m_event_lookup, [](const std::pair<const appulatrix::Event *, CoreEvent *> &item) { return item.first; });
    scene()->clearSelection();
    for (auto item : m_selected)
    {
      auto graphics_item = m_event_lookup[item];
      graphics_item->setSelected(item);
    }
    scene()->update();
  }
  
  bool has_any_selected() const
  {
    return m_selected.size() > 0;
  }

private:
  QColor color_for_type(appulatrix::TypeId type)
  {
    auto scale = [](std::uint8_t val, std::uint8_t min, std::uint8_t max) {
      return (double(val)/255) * (max-min) + min;
    };
    std::uint64_t type_large = 1190494759 * type;
    std::uint32_t data = type_large ^ (type_large >> 32);
    auto hue = data & 0xff;
    auto sat = scale((data >> 8) & 0xff, 64, 130);
    auto lightness = scale((data >> 16) & 0xff, 64, 192);
    return QColor::fromHsl(hue, sat, lightness);
  }
  
  std::unordered_map<const appulatrix::Event *, CoreEvent *> m_event_lookup;
  std::unordered_set<const appulatrix::Event *> m_selected;
  bool m_ignore_selection_changes = false;
  
  std::unordered_map<std::uint64_t, BasicScopedEvent *> m_ongoing_events;
  appulatrix::TypeLookup &m_types;
};
