#pragma once

#include "appulatrix/Reader.h"
#include "appulatrix/StackProviders/Utils.h"

#include <QTreeWidget>
#include <QLineEdit>
#include <QSplitter>
#include <QVBoxLayout>

class StackManager : public QWidget
{
  Q_OBJECT

public:
  static const int IndexRole = Qt::UserRole;

  StackManager(
    std::shared_ptr<appulatrix::Reader> reader,
    QWidget *parent)
  : QWidget(parent)
  , m_reader(reader)
  {
    auto layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(4);
    
    
    auto search = new QLineEdit(this);
    search->setPlaceholderText("Search");
    layout->addWidget(search);
    
    connect(search, &QLineEdit::textChanged,
      [this, search](QString text)
      {
        m_filter = text;
        refresh_stacks();
      }
    );
    
    auto splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);
    layout->addWidget(splitter);
    
    m_selected_stack = new QTreeWidget(this);
    m_selected_stack->setHeaderLabels(QStringList() << "Index" << "Address" << "Name");
    splitter->addWidget(m_selected_stack);
    
    m_stacks = new QTreeWidget(this);
    m_stacks->setHeaderLabels(QStringList() << "Id" << "Name");
    splitter->addWidget(m_stacks);
    
    connect(m_stacks, &QTreeWidget::itemSelectionChanged,
      [this]()
      {
        m_selected_stack->clear();
              
        auto items = m_stacks->selectedItems();
        if (!items.isEmpty())
        {
          int index = items[0]->data(0, IndexRole).toInt();
          set_selected_stack(index);
          emit on_stack_selected(index);
          return;
        }
        
        emit on_stack_selected(QVariant());
      }
    );
  }

  void set_selected_stack(appulatrix::StackId i)
  {
    auto &&stack = m_reader->get_stack(i);
    
    auto container = appulatrix::FormattedStackContainer::from_string(stack.stack_string.get());
    auto entries = container.entries();
    std::size_t index = 0;
    for (auto &e : entries)
    {
      QString symbol;
      if (e.symbol_name)
      {
        symbol = e.symbol_name->c_str();
      }
      
      auto frame_item = new QTreeWidgetItem(m_selected_stack);
      frame_item->setText(0, QString::number(index));
      frame_item->setText(1, e.pc.c_str());
      frame_item->setText(2, symbol);
      m_selected_stack->addTopLevelItem(frame_item);
      ++index;
    }
  }

  void refresh_stacks()
  {
    QFontMetrics met(font());
    m_stacks->clear();
    
    for (appulatrix::StackId i = 0; i < m_reader->stack_count(); ++i)
    {
      auto &&stack = m_reader->get_stack(i);
      
      auto container = appulatrix::FormattedStackContainer::from_string(stack.stack_string.get());
      auto entries = container.entries();
      
      QString stack_str;
      for (auto &e : entries)
      {
        if (e.symbol_name)
        {
          if (!stack_str.isEmpty())
          {
            stack_str += "\n";
          }
          
          QString str = e.symbol_name->c_str();
          str = str.split("(")[0];
          stack_str += met.elidedText(str, Qt::ElideMiddle, 300);
        }
      }
      
      if (!m_filter.isEmpty() && stack_str.indexOf(m_filter) == -1)
      {
        continue;
      }
      
      auto stack_item = new QTreeWidgetItem(m_stacks);
      stack_item->setText(0, QString::number(stack.index));
      stack_item->setText(1, stack_str);
      stack_item->setData(0, IndexRole, int(i));
      m_stacks->addTopLevelItem(stack_item);
    }
  }
  
signals:
  void on_stack_selected(QVariant stack);
  
private:
  std::shared_ptr<appulatrix::Reader> m_reader;
  QTreeWidget *m_selected_stack;
  QTreeWidget *m_stacks;
  QString m_filter;
};
