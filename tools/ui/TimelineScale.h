#pragma once

#include "TimelineCore.h"
#include "appulatrix/Time.h"

#include <QWidget>

#include <boost/optional.hpp>

class TimelineScale : public QWidget
{
  Q_OBJECT
public:
  using Time = appulatrix::Time;
  using TimePair = std::pair<Time, Time>;

  TimelineScale(
    QWidget *parent);

  QSize sizeHint() const override;

  void set_range(const VisibleTimeRange &domain);
  const VisibleTimeRange &range() const { return m_timeline.range(); }

signals:
  void visible_changed(const TimeRange &time);

protected:
  bool event(QEvent *) override;
  void paintEvent(QPaintEvent *event) override;
  void mousePressEvent(QMouseEvent *e) override;
  void mouseMoveEvent(QMouseEvent *e) override;
  void mouseReleaseEvent(QMouseEvent *e) override;

private:
  Time map_pixel_to_time(int x) const;

  TimelineCore m_timeline;
  boost::optional<std::pair<Time, Time>> m_right_click_drag;
};
