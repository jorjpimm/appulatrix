#include "TimelineOverview.h"

#include <QCursor>
#include <QNativeGestureEvent>
#include <QPainter>
#include <QWheelEvent>

#include <cassert>
#include <iostream>

TimeRange::TimeRange()
: m_range()
{
}

TimeRange::TimeRange(const std::pair<Time, Time> &rng)
  : m_range(rng)
{
}

TimeRange::TimeRange(const Time &begin, const Time &end)
: m_range(begin, end)
{
  assert(begin < end);
}

double TimeRange::time_to_ratio(const Time &t) const
{
  using namespace std::chrono;
  return ratio_of_duration(duration_cast<nanoseconds>(t - m_range.first));
}

TimeRange::Time TimeRange::ratio_to_time(double dbl) const
{
  using namespace std::chrono;
  auto offset_ns = duration_cast<nanoseconds>(m_range.second - m_range.first).count() * dbl;

  return m_range.first + nanoseconds(std::uint64_t(offset_ns));
}

double TimeRange::ratio_of_duration(const std::chrono::nanoseconds &ns) const
{
  using namespace std::chrono;
  return ns.count() / double(duration_cast<nanoseconds>(m_range.second - m_range.first).count());
}

VisibleTimeRange::VisibleTimeRange()
{
}

VisibleTimeRange::VisibleTimeRange(const TimeRange &domain, const TimeRange &visible)
: m_visible(visible)
, m_domain(domain)
{
}

void VisibleTimeRange::pan_visible(double domain_delta)
{
  auto b = m_domain.time_to_ratio(m_visible.begin());
  auto w = m_domain.time_to_ratio(m_visible.end()) - b;
  
  b += domain_delta;
  if (b + w > 1.0)
  {
    b = 1.0 - w;
  }
  else if (b < 0.0)
  {
    b = 0.0;
  }
  
  m_visible = TimeRange(m_domain.ratio_to_time(b), m_domain.ratio_to_time(b + w));
}

void VisibleTimeRange::zoom_visible(double domain_zoom_centre, double factor)
{
  using namespace std::chrono;
  const auto min_ratio = m_domain.ratio_of_duration(milliseconds(10));
    
  auto first_ratio = domain_zoom_centre + (m_domain.time_to_ratio(m_visible.begin()) - domain_zoom_centre) * factor;
  auto second_ratio = domain_zoom_centre + (m_domain.time_to_ratio(m_visible.end()) - domain_zoom_centre) * factor;
  if (first_ratio > second_ratio || (second_ratio - first_ratio) < min_ratio)
  {
    second_ratio = first_ratio + min_ratio;
  }
  
  if (first_ratio < 0.0)
  {
    second_ratio = std::min(1.0, second_ratio - first_ratio);
    first_ratio = 0.0;
  }
  
  if (second_ratio > 1.0)
  {
    first_ratio = 1.0 - std::min(1.0, second_ratio - first_ratio);
    second_ratio = 1.0;
  }
  
  m_visible = TimeRange(m_domain.ratio_to_time(first_ratio), m_domain.ratio_to_time(second_ratio));
}

TimelineCore::TimelineCore(
  DragMode drag_mode)
: m_drag_mode(drag_mode)
, m_dragging(false)
{
}

void TimelineCore::set_range(const VisibleTimeRange &range)
{
  m_range = range;
}

bool TimelineCore::accept_event(QEvent *e, const Transform &x_scaler)
{
  if (e->type() == QEvent::NativeGesture)
  {
    return nativeGestureEvent(static_cast<QNativeGestureEvent*>(e), x_scaler);
  }
  else if (e->type() == QEvent::Wheel)
  {
    return wheelEvent(static_cast<QWheelEvent *>(e), x_scaler);
  }
  else if (e->type() == QEvent::MouseButtonPress)
  {
    return mousePressEvent(static_cast<QMouseEvent *>(e), x_scaler);
  }
  else if (e->type() == QEvent::MouseMove)
  {
    return mouseMoveEvent(static_cast<QMouseEvent *>(e), x_scaler);
  }
  else if (e->type() == QEvent::MouseButtonRelease)
  {
    return mouseReleaseEvent(static_cast<QMouseEvent *>(e), x_scaler);
  }

  return false;
}

bool TimelineCore::nativeGestureEvent(QNativeGestureEvent *native_gesture, const Transform &x_scaler)
{
  if (native_gesture->gestureType() == Qt::ZoomNativeGesture)
  {
    auto factor = 1.0 + -native_gesture->value();
    m_range.zoom_visible(x_scaler(native_gesture->pos().x()), factor);
    return true;
  }
  return false;
}

bool TimelineCore::wheelEvent(QWheelEvent *wheel, const Transform &x_scaler)
{
  if (m_dragging)
  {
    return false;
  }
  
  if (wheel->orientation() == Qt::Vertical)
  {
    const auto delta_to_factor = 0.001;
    const auto zoom_factor = 1.0 + wheel->delta() * delta_to_factor;

    auto domain_location = x_scaler(wheel->pos().x());
    m_range.zoom_visible(domain_location, zoom_factor);
  }
  else if (wheel->orientation() == Qt::Horizontal)
  {
    double domain_delta = wheel->delta() / -5000.0;
    
    if (m_drag_mode == DragMode::DragDomain)
    {
      domain_delta *= m_range.visible_ratio();
    }
    
    m_range.pan_visible(domain_delta);
  }

  return true;
}

bool TimelineCore::mousePressEvent(QMouseEvent *event, const Transform &x_scaler)
{
  if (event->button() != Qt::LeftButton)
  {
    return false;
  }
  m_dragging = true;
  m_last_pos = event->pos();
  
  return true;
}

bool TimelineCore::mouseMoveEvent(QMouseEvent *event, const Transform &x_scaler)
{
  if (!m_dragging)
  {
    return false;
  }

  auto start = x_scaler(m_last_pos.x());
  auto end = x_scaler(event->pos().x());
  auto domain_delta = end - start;
  if (m_drag_mode == DragMode::DragDomain)
  {
    domain_delta *= -1.0;
  }
  m_range.pan_visible(domain_delta);

  m_last_pos = event->pos();
  return true;
}

bool TimelineCore::mouseReleaseEvent(QMouseEvent *event, const Transform &x_scaler)
{
  if (!m_dragging)
  {
    return false;
  }
  
  m_dragging = false;
  return true;
}
