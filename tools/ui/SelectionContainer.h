#pragma once

#include <QObject>

#include "Selection.h"

class SelectionContainer : public QObject
{
  Q_OBJECT
public:
  SelectionContainer();
  
  const Selection &get_selection() const;
  void set_selection(const Selection &sel);

  template <typename F> void mutate_selection(F f)
  {
    auto sel = get_selection();
    f(sel);
    set_selection(sel);
  };

signals:
  void selection_changed();


private:
  bool m_in_selection_change = false;
  Selection m_selection;
};
