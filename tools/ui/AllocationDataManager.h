#pragma once

#include "appulatrix/Event.h"

#include <QObject>

#include <iostream>
#include <unordered_map>

class AllocationDataManager : public QObject
{
  Q_OBJECT

public:
  AllocationDataManager()
  {
  }
  
  void add_allocation(const appulatrix::Event &ev, std::uint64_t ptr, std::size_t size)
  {
    m_open_allocations[ptr] = size;

    m_current_allocated_size += size;
    m_max_allocated_memory = std::max(m_max_allocated_memory, m_current_allocated_size);
    m_points.push_back({ &ev, m_current_allocated_size });
    emit data_added();
  }

  void add_deallocation(const appulatrix::Event &ev, std::uint64_t ptr)
  {
    auto it = m_open_allocations.find(ptr);
    if (it == m_open_allocations.end())
    {
      std::cerr << "Memory freed without allocation " << std::hex << std::showbase << ptr << std::endl;
      return;
    }

    m_current_allocated_size -= it->second;
    m_points.push_back({ &ev, m_current_allocated_size });

    m_open_allocations.erase(it);
    emit data_added();
  }

  
  std::size_t maximum_allocated_bytes() const { return m_max_allocated_memory; }
  appulatrix::Time start_time() const
  {
    if (m_points.empty()) return {};
    return m_points.front().event->time;
  }
  appulatrix::Time end_time() const
  {
    if (m_points.empty()) return {};
    return m_points.back().event->time;
  }
  
  struct Point
  {
    const appulatrix::Event *event;
    std::size_t current_size;
  };

  const std::vector<Point> &points() const { return m_points; }

signals:
  void data_added();

private:
  std::vector<Point> m_points;
  std::size_t m_current_allocated_size = 0;
  std::size_t m_max_allocated_memory = 0;
  std::unordered_map<std::uint64_t, std::size_t> m_open_allocations;
};
