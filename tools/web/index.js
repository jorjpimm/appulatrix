let mmap = require("mmap-io/build/Debug/mmap-io.node"),
    fs = require("fs")
    ;

class MappedFile
{
  constructor(path)
  {
    let size = fs.statSync(path).size;
    let fd = fs.openSync(path, "r");
    this.mapped_data = mmap.map(size, mmap.PROT_READ, mmap.MAP_SHARED, fd);
  }
}

let file = new MappedFile(process.argv[2]);

console.log(file.mapped_data);
