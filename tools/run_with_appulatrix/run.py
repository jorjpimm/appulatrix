#!/usr/bin/env python

import logging
import os
import platform
import subprocess
import sys

EXE_SEARCH_PATH = os.environ["PATH"] + os.pathsep + os.environ.get("APPULATRIX_PATH", "")

def find_in_path(program):
    import os
    def is_valid(fpath):
        return os.path.isfile(fpath)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_valid(program):
            return program
    else:
        for path in EXE_SEARCH_PATH.split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_valid(exe_file):
                return exe_file

    raise Exception("Failed to find file in path: '{}'".format(program))

def run_debugged_process_win(args):
    cmd_args = args[1:]
    cmd_args = ["--args"] + cmd_args if len(cmd_args) > 0 else []

    injector_exe = find_in_path("injector.exe")

    return subprocess.call([
        injector_exe,
        "--start"
        ] +
        cmd_args +
        [ args[0] ])

def run_debugged_process_osx(args):
    my_env = os.environ.copy()
    my_env["DYLD_INSERT_LIBRARIES"] = find_in_path("libappulatrix_injector.dylib")
    my_env["DYLD_FORCE_FLAT_NAMESPACE"] = "1"

    proc = subprocess.Popen(args, env=my_env)
    proc.wait()
    return proc.returncode

def run_debugged_process_linux(args):
    my_env = os.environ.copy()
    my_env["LD_PRELOAD"] = "libdl.so:" + find_in_path("libappulatrix_injector.so")

    proc = subprocess.Popen(args, env=my_env)
    proc.wait()
    return proc.returncode

if __name__ == "__main__":
    if len(sys.argv) < 2:
        logging.getLogger().error(
            "run.py requires an executable to run:\n"
            "  example usage: run.py [options] executable.out arguments")
        sys.exit(1)

    args = sys.argv[1:]
    if not os.path.exists(args[0]):
        logging.getLogger().error(
            "run.py requires an existing executable to run, got {}".format(args[0]))
        sys.exit(1)


    if platform.system() == "Windows":
        code = run_debugged_process_win(args)
    elif platform.system() == "Linux":
        code = run_debugged_process_linux(args)
    else:
        code = run_debugged_process_osx(args)

    if code != 0:
        sys.exit(code)
