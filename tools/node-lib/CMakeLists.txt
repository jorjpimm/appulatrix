cmake_minimum_required(VERSION 3.2)
project(appulatrix_node)

# Include imported data from built appulatrix
include(${CMAKE_SOURCE_DIR}/gen/AppulatrixTargets.cmake)
include(${CMAKE_SOURCE_DIR}/gen/AppulatrixVariables.cmake)

find_package(Boost
  COMPONENTS
    date_time
    filesystem
    system
    program_options
  REQUIRED
)
link_directories(${BOOST_LIBRARYDIR})

include_directories(
    ${Boost_INCLUDE_DIRS}
)

add_library(appulatrix_node SHARED
  src/appulatrix-lib.cpp
  src/v8_tools.h
)

set_target_properties(appulatrix_node
  PROPERTIES
    PREFIX ""
    SUFFIX ".node"
)

target_include_directories(appulatrix_node
  PUBLIC
    ${CMAKE_JS_INC}
)

target_link_libraries(appulatrix_node
  PUBLIC
    ${CMAKE_JS_LIB}
    appulatrix
)

set(APPULATRIX_BUILD_TYPE Release)
set(APPULATRIX_BUILD_DIR )
if (APPLE OR WIN32)
  set(APPULATRIX_BUILD_DIR "${APPULATRIX_BUILD_TYPE}/")
endif()

set(APPULATRIX_NODE_LIBRARY ${NODE_LIB_BINARY_DIR}/${NODE_LIB_BUILD_NAME})
configure_file(
  ${CMAKE_SOURCE_DIR}/appulatrix-defs.js.in
  ${CMAKE_SOURCE_DIR}/gen/appulatrix-defs.js
  @ONLY
)
