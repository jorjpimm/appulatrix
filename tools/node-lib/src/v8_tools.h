#pragma once

#define NOMINMAX
#include <nan.h>
#include "appulatrix/Reader.h"
#include "appulatrix/Event.h"

template <typename K, typename V> void set(
  v8::Local<v8::Object> &obj,
  const K &key,
  const V &value);

template <typename T> v8::Local<T> to_v8(const v8::Local<T> &v)
{
  return v;
}

v8::Local<v8::Value> to_v8(const char *v)
{
  return Nan::New(v).FromMaybe(Nan::Undefined());
}

template <typename T> v8::Local<v8::Value> to_v8(const T &v)
{
  return Nan::New(v);
}

v8::Local<v8::Value> to_v8(const appulatrix::Type::Field &type)
{
  v8::Local<v8::Object> field_obj = Nan::New<v8::Object>();
  set(field_obj, "name", type.name().get());
  set(field_obj, "offset", std::uint32_t(type.offset()));
  if (type.parent() != std::numeric_limits<std::size_t>::max())
  {
    set(field_obj, "parent", std::uint32_t(type.parent()));
  }
  set(field_obj, "type", std::uint32_t(type.type()));

  return field_obj;
}

v8::Local<v8::Value> to_v8(const appulatrix::Type &type)
{ 
  v8::Local<v8::Object> type_obj = Nan::New<v8::Object>();

  set(type_obj, "name", type.name().get());
  set(type_obj, "size", std::uint32_t(type.size()));
  set(type_obj, "id", type.id());
  const char *data_type = nullptr;
  switch(type.data_type())
  {
    case appulatrix::Type::DataType::Boolean: data_type = "Boolean"; break;
    case appulatrix::Type::DataType::UnsignedInteger: data_type = "UnsignedInteger"; break;
    case appulatrix::Type::DataType::Integer: data_type = "Integer"; break;
    case appulatrix::Type::DataType::Real: data_type = "Real"; break;
    case appulatrix::Type::DataType::Unknown: break;
  }
  if (data_type)
  {
    set(type_obj, "data_type", data_type);
  }

  v8::Local<v8::Object> fields_obj = Nan::New<v8::Array>();
  std::uint32_t i = 0;
  for (auto &field : type.fields())
  {
    set(fields_obj, i, field);
    ++i;
  }
  set(type_obj, "fields", fields_obj);
  return type_obj;
}

template <typename K, typename V> void set(
  v8::Local<v8::Object> &obj,
  const K &key,
  const V &value)
{
  obj->Set(to_v8(key), to_v8(value));
}
