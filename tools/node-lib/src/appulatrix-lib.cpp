#include "v8_tools.h"

#include <boost/filesystem/path.hpp>

namespace std
{
template<> struct hash<gsl::span<const char>>
{
  typedef gsl::span<const char> argument_type;
  typedef std::size_t result_type;
  result_type operator()(argument_type const& s) const
  {
    result_type const h1 ( std::hash<const void *>{}(s.begin()) );
    result_type const h2 ( std::hash<const void *>{}(s.end()) );
    return h1 ^ (h2 << 1); // or use boost::hash_combine
  }
};
}

struct ReaderWrapper : public Nan::ObjectWrap
{
public:
  explicit ReaderWrapper(const boost::filesystem::path &path)
  : m_reader(path)
  {
  }
  ~ReaderWrapper()
  {
  }

  static void New(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void read(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void get_stack(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void stack_count(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void get_type(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void type_count(const Nan::FunctionCallbackInfo<v8::Value>& info);

  static Nan::Persistent<v8::Function> constructor;

  appulatrix::Reader m_reader;
  std::unordered_map<
    gsl::span<const char>, 
    Nan::Global<v8::Object>
    > m_files;
};

Nan::Persistent<v8::Function> ReaderWrapper::constructor;

void ReaderWrapper::New(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
  Nan::HandleScope();

  if (info.Length() != 1)
  {
      return Nan::ThrowError(
          "Reader(...) takes 1 argument: (path :string)."
      );
  }

  if (!info[0]->IsString())
  {
    return Nan::ThrowError("Reader: path (arg[0]) must be an existing file.");
  }

  if (!info.IsConstructCall()) {
    // Invoked as plain function `Reader(...)`, turn into construct call.
    const int argc = 1;
    v8::Local<v8::Value> argv[argc] = { info[0] };
    v8::Local<v8::Function> cons = Nan::New<v8::Function>(constructor);
    info.GetReturnValue().Set(cons->NewInstance(argc, argv));
    return;
  }

  const v8::String::Utf8Value path_u8(info[0]->ToString());
  const boost::filesystem::path path(*path_u8);

  auto reader = new ReaderWrapper(path);
  reader->Wrap(info.This());
  info.GetReturnValue().Set(info.This());
}


void ReaderWrapper::read(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
  ReaderWrapper* obj = ReaderWrapper::Unwrap<ReaderWrapper>(info.Holder());
  if (!obj)
  {
    return Nan::ThrowError("Reader::read: invalid this");
  }

  gsl::span<const char> data;
  auto read = obj->m_reader.read_with_data(data);
  
  if (read.size() == 0)
  {
    return;
  }
  
  auto found = obj->m_files.find(data);
  if (found == obj->m_files.end())
  {
    Nan::FreeCallback free_cb = [](char *, void *) {};
    Nan::MaybeLocal<v8::Object> buffer = Nan::NewBuffer(const_cast<char *>(data.data()), data.size(), free_cb, nullptr);
    if (buffer.IsEmpty())
    {
      return;
    }
    
    obj->m_files.insert(
      std::make_pair(
        data, 
        Nan::Global<v8::Object>(buffer.ToLocalChecked())
      )
    );
    found = obj->m_files.find(data);
    assert(found != obj->m_files.end());
  }

  auto start = (char *)read.data() - data.begin();
  assert(start > 0);

  v8::Local<v8::Object> events = Nan::New<v8::Object>();

  set(events, "offset", std::uint32_t(start));
  set(events, "size", std::uint32_t(read.size()));
  set(events, "data", Nan::New(found->second));

  info.GetReturnValue().Set(events);
}

void ReaderWrapper::get_stack(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
  ReaderWrapper* obj = ReaderWrapper::Unwrap<ReaderWrapper>(info.Holder());
  if (!obj)
  {
    return Nan::ThrowError("Reader::get_stack: invalid this");
  }

  if (info.Length() != 1)
  {
      return Nan::ThrowError(
          "Reader::get_stack takes 1 argument: (stack :int)."
      );
  }

  auto stack_index = Nan::To<std::uint32_t>(info[0]);
  if (!stack_index.IsJust())
  {
    return Nan::ThrowError("Reader::get_stack: stack (arg[0]) must be an integer.");
  }

  auto idx = stack_index.FromJust();
  if (idx >= obj->m_reader.stack_count())
  {
    return Nan::ThrowError("Reader::get_stack: stack (arg[0]) must be < stack_count().");
  }

  auto &&stack = obj->m_reader.get_stack(idx);
  info.GetReturnValue().Set(to_v8(stack.stack_string.get()));
}

void ReaderWrapper::stack_count(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
  ReaderWrapper* obj = ReaderWrapper::Unwrap<ReaderWrapper>(info.Holder());
  if (!obj)
  {
    return Nan::ThrowError("Reader::stack_count: invalid this");
  }

  auto count = obj->m_reader.stack_count();

  info.GetReturnValue().Set(to_v8(std::uint32_t(count)));
}

void ReaderWrapper::get_type(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
  ReaderWrapper* obj = ReaderWrapper::Unwrap<ReaderWrapper>(info.Holder());
  if (!obj)
  {
    return Nan::ThrowError("Reader::get_type: invalid this");
  }

  if (info.Length() != 1)
  {
    return Nan::ThrowError(
      "Reader::get_type takes 1 argument: (type :int)."
      );
  }

  auto stack_index = Nan::To<std::uint32_t>(info[0]);
  if (!stack_index.IsJust())
  {
    return Nan::ThrowError("Reader::get_type: type (arg[0]) must be an integer.");
  }

  auto idx = stack_index.FromJust();
  if (idx > obj->m_reader.stack_count())
  {
    return Nan::ThrowError("Reader::get_type: type (arg[0]) must be < type_count().");
  }

  auto type = obj->m_reader.types().type_list()[idx];
  info.GetReturnValue().Set(to_v8(*type));
}

void ReaderWrapper::type_count(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
  ReaderWrapper* obj = ReaderWrapper::Unwrap<ReaderWrapper>(info.Holder());
  if (!obj)
  {
    return Nan::ThrowError("Reader::type_count: invalid this");
  }

  auto count = obj->m_reader.types().type_list().size();
  info.GetReturnValue().Set(Nan::New(std::uint32_t(count)));
}

void Init(v8::Handle<v8::Object> exports, v8::Handle<v8::Object> module)
{
  // Prepare constructor template
  v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(ReaderWrapper::New);
  tpl->SetClassName(Nan::New("Reader").ToLocalChecked());
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  Nan::SetPrototypeMethod(tpl, "read", ReaderWrapper::read);
  Nan::SetPrototypeMethod(tpl, "stack_count", ReaderWrapper::stack_count);
  Nan::SetPrototypeMethod(tpl, "get_stack", ReaderWrapper::get_stack);
  Nan::SetPrototypeMethod(tpl, "type_count", ReaderWrapper::type_count);
  Nan::SetPrototypeMethod(tpl, "get_type", ReaderWrapper::get_type);

  ReaderWrapper::constructor.Reset(tpl->GetFunction());
  exports->Set(Nan::New("Reader").ToLocalChecked(), tpl->GetFunction());
}

NODE_MODULE(appulatrix, Init);
