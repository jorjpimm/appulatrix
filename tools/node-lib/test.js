let appulatrix = require("./appulatrix.js")
  , expect = require("chai").expect;

TEST_DATA = process.argv[3];
if (TEST_DATA == undefined)
{
  throw "Invalid test data";
}
console.log(`Reading test data from ${TEST_DATA}`)

describe("Appulatrix Reader", function() {
  describe("Stack Extraction", function() {
    let reader = appulatrix.Reader(TEST_DATA);

    it("Has no stack data without read", function() {
      expect(reader.stack_count()).to.equal(0);
    });

    it("read gets events", function() {
      let read_events = reader.read();
      console.log(read_events);
      expect(read_events.size).to.be.above(0);
    });

    it("has stacks after read", function() {
      // Check lots of strings
      expect(reader.stack_count()).to.be.above(4);
    });

    it("gets stacks correctly", function() {
      stack0 = reader.get_stack(0);
      stack1 = reader.get_stack(1);

      console.log(stack0)
      console.log(stack1)

      // Check many lines
      expect(stack0.split(/\r\n|\r|\n/).length).to.be.above(5);
      expect(stack1.split(/\r\n|\r|\n/).length).to.be.above(5);
      expect(stack0).to.not.equal(stack1);
    });
  });


  describe("Type Extraction", function() {
    let reader = appulatrix.Reader(TEST_DATA);

    it("Has no type data without read", function() {
      expect(reader.type_count()).to.equal(0);
    });

    it("read gets events", function() {
      let read_events = reader.read();
      expect(read_events.size).to.be.above(0);
    });

    it("has types after read", function() {
      // Check lots of strings
      expect(reader.type_count()).to.be.above(0);
    });

    it("gets types correctly", function() {
      for (let i = 0; i < reader.type_count(); ++i)
      {
        type = reader.get_type(i);
      }
    });
  });
});
