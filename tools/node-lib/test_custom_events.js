let appulatrix = require("./appulatrix.js")
  , expect = require("chai").expect;

TEST_DATA = process.argv[3];
if (TEST_DATA == undefined)
{
  throw "Invalid test data";
}
console.log(`Reading test data from ${TEST_DATA}`)

describe("Appulatrix Reader custom events", function() {
  describe("Stack Extraction for events", function() {
    let reader = appulatrix.Reader(TEST_DATA);

    it("Has no stack data without read", function() {
      expect(reader.stack_count()).to.equal(0);
    });

    it("read gets events", function() {
      let read_events = reader.read();
      expect(read_events.size).to.equal(8);
    });

    it("has stacks after read", function() {
      // Check lots of strings
      expect(reader.stack_count()).to.equal(8);
    });

    it("gets stacks correctly", function() {
      stack0 = reader.get_stack(0);
      stack1 = reader.get_stack(1);
      stack2 = reader.get_stack(2);

      // Check many lines
      expect(stack0.split(/\r\n|\r|\n/).length).to.be.above(1);
      expect(stack1.split(/\r\n|\r|\n/).length).to.be.above(1);
      expect(stack2.split(/\r\n|\r|\n/).length).to.be.above(1);
      expect(stack0).to.not.equal(stack1);
      expect(stack0).to.not.equal(stack2);
    });
  });


  describe("Type Extraction", function() {
    let reader = appulatrix.Reader(TEST_DATA);

    it("Has no type data without read", function() {
      expect(reader.type_count()).to.equal(0);
    });

    it("read gets events", function() {
      let read_events = reader.read();
      expect(read_events.size).to.equal(8);
    });

    it("has types after read", function() {
      // Check lots of strings
      expect(reader.type_count()).to.equal(10);

      const first_user_type = 3
      const type0 = reader.get_type(0);
      expect(type0).to.eql({ name: 'bool', 'size': 1, 'id': first_user_type, 'fields': [], 'data_type': 'Boolean' });
      
      const type1 = reader.get_type(1);
      expect(type1).to.eql({ name: 'appulatrix::String', 'size': 4, 'id': first_user_type+1, 'fields': [] });
      
      const type2 = reader.get_type(2);
      expect(type2.name).to.eql("appulatrix::EventProviderData");
      
      const type4 = reader.get_type(5);
      expect(type4).to.eql({ name: 'TestA', 'size': 1, 'id': first_user_type+3, 'fields': [] });
      const type5 = reader.get_type(7);
      expect(type5).to.eql({ name: 'TestB', 'size': 1, 'id': first_user_type+5, 'fields': [] });
      const type7 = reader.get_type(8);
      expect(type7).to.eql({ name: 'TestC', 'size': 1, 'id': first_user_type+6, 'fields': [] });

      const type_list = [
        'bool',
        'appulatrix::String',
        'appulatrix::EventProviderData',
        'std::uint64_t',
        'appulatrix::EventBlockBegin',
        'TestA',
        null,
        'TestB',
        'TestC',
      ];

      for (let i = 0; i < reader.type_count(); ++i)
      {
        if (type_list[i])
        {
          expect(reader.get_type(i)['name']).to.equal(type_list[i]);
        }
      }
    });
  });
});
