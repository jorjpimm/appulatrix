#include "appulatrix/Reader.h"
#include "appulatrix/Event.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <iostream>
#include <gsl/gsl-lite.h>

namespace fs = boost::filesystem;
namespace po = boost::program_options;

namespace boost
{

template <typename T> std::istream &operator>>(std::istream &str, optional<T> &t)
{
  t.emplace();
  return str >> *t;
}

}

struct TimeRange
{
  using HRC = std::chrono::high_resolution_clock;
  using SC = std::chrono::system_clock;

  void print(std::ostream &str, HRC::time_point begin, HRC::time_point end)
  {
    auto convert = [&](HRC::time_point tp)
    {
      auto behind = std::chrono::duration_cast<std::chrono::microseconds>(now - tp);
      SC::time_point behind_sc = system_now - behind;

      str << boost::posix_time::from_time_t(SC::to_time_t(behind_sc));
    };

    convert(begin);
    str << " - ";
    convert(end);
    str << ", " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "ms";
  }

  std::vector<const appulatrix::Event *> clip(const std::vector<const appulatrix::Event *> &ev_in)
  {
    std::vector<const appulatrix::Event *> events = ev_in;
    auto event_finder = [](const appulatrix::Event *a, const appulatrix::Time &b)
      { return a->time < b; };

    {
      auto fnd = std::lower_bound(events.begin(), events.end(), start, event_finder);
      if (fnd != events.end())
      {
        events.erase(events.begin(), fnd);
      }
    }

    {
      auto fnd = std::lower_bound(events.begin(), events.end(), end, event_finder);
      if (fnd != events.end())
      {
        events.erase(fnd, events.end());
      }
    }
    return events;
  }

  HRC::time_point start;
  HRC::time_point end;
  HRC::time_point now;
  SC::time_point system_now;
};

std::ostream &operator<<(std::ostream &str, const TimeRange &range)
{
  return str;
}

struct Options
{
  Options()
  : description(description_text())
  {
    positional.add("source", 1);

    description.add_options()
      ("help,h", "Display help message")
      ("source,s", po::value(&source)->required(), "Input file")
      ("start", po::value(&time_start), "Start time")
      ("end", po::value(&time_end), "End time")
    ;
  }

  void run(int argc, char *argv[])
  {
    po::store(
      po::command_line_parser(argc, argv)
        .options(description)
        .positional(positional)
        .run(),
      variables
    );
    po::notify(variables);
  }

  std::string description_text() const
  {
    return
      "Usage inspector [options] input-file\n"
      "\n"
      "Allowed options";
  }

  boost::filesystem::path input_file() const
  {
    return source;
  }

  TimeRange get_time_range(
    gsl::span<const appulatrix::Event *> relative_to) const
  {
    using HRC = std::chrono::high_resolution_clock;
    HRC::time_point start;
    HRC::time_point end;
    if (relative_to.empty())
    {
      start = end = HRC::now();
    }
    else
    {
      start = relative_to[0]->time;
      end = relative_to[relative_to.size()-1]->time;
    }

    auto get_time = [&](const boost::optional<std::int64_t> &opt)
      -> boost::optional<std::chrono::high_resolution_clock::time_point>
    {
      if (!opt)
      {
        return boost::none;
      }

      if (*opt < 0)
      {
        return end + std::chrono::seconds(*opt);
      }

      return start + std::chrono::seconds(*opt);
    };

    return TimeRange{
      get_time(time_start).get_value_or(start),
      get_time(time_end).get_value_or(end),
      std::chrono::high_resolution_clock::now(),
      std::chrono::system_clock::now()
    };
  }

  po::positional_options_description positional;
  po::options_description description;

  po::variables_map variables;
  fs::path source;

  boost::optional<std::int64_t> time_start;
  boost::optional<std::int64_t> time_end;
};

struct AllocationManager
{
  void malloc(std::uint64_t ptr, std::size_t size, const appulatrix::Event *event)
  {
    assert(event);
    m_live_allocations[ptr] = Allocation{ m_index++, ptr, size, event, nullptr };
  }

  void free(std::uint64_t ptr, const appulatrix::Event *event)
  {
    auto alloc = m_live_allocations.find(ptr);
    if (alloc == m_live_allocations.end())
    {
      return;
    }
    alloc->second.free_event = event;

    m_finished_allocations.push_back(alloc->second);
    m_live_allocations.erase(alloc);
  }

  void dump(std::ostream &str, const appulatrix::Reader &r) const
  {
    std::size_t total_freed_bytes = 0;
    for (auto &x : m_finished_allocations)
    {
      total_freed_bytes += x.size;
    }
    str << m_finished_allocations.size() << " allocations made, " << total_freed_bytes << " bytes" << std::endl;

    std::size_t total_allocated_bytes = 0;
    for (auto &x : m_live_allocations)
    {
      total_allocated_bytes += x.second.size;
    }
    str << m_finished_allocations.size() << " allocations still live, " << total_allocated_bytes << " bytes" << std::endl;

    struct AllocationsByStack
    {
      appulatrix::StackId stack;
      std::size_t size;
      std::size_t count;
    };
    using AllocationMap = std::map<appulatrix::StackId, AllocationsByStack>;
    AllocationMap allocations;
    for (auto &itr : m_live_allocations)
    {
      auto stk_idx = itr.second.alloc_event->stack;
      auto found = allocations.find(stk_idx);
      if (found == allocations.end())
      {
        allocations[stk_idx] = AllocationsByStack{ stk_idx, itr.second.size, 1 };
      }
      else
      {
        found->second.size += itr.second.size;
        ++found->second.count;
      }
    }

    std::vector<AllocationsByStack> sorted_allocations;
    sorted_allocations.reserve(allocations.size());
    std::for_each(allocations.begin(), allocations.end(),
      [&sorted_allocations](const AllocationMap::value_type& p)
        { sorted_allocations.push_back(p.second); });
    std::sort(sorted_allocations.begin(), sorted_allocations.end(),
      [](const AllocationsByStack &a, const AllocationsByStack &b) { return a.size > b.size; });

    for (auto &alloc : sorted_allocations)
    {
      str << std::endl;
      str << alloc.count << " live allocations (" << alloc.size << " bytes) at:" << std::endl;
      str << r.get_stack(alloc.stack).stack_string.get();
    }
  }

  struct Allocation
  {
    std::size_t index;
    std::uint64_t pointer;
    std::size_t size;
    const appulatrix::Event *alloc_event;
    const appulatrix::Event *free_event;
  };

  std::size_t m_index = 0;
  std::vector<Allocation> m_finished_allocations;
  std::unordered_map<std::uint64_t, Allocation> m_live_allocations;
};

void run_inspector(const Options &opts)
{
  auto file = opts.input_file();
  std::cout << "Reading input file " << file << std::endl;

  if (!fs::exists(file))
  {
    std::cerr << "Missing input file " << file << std::endl;
  }

  appulatrix::Reader reader(file);

  std::vector<const appulatrix::Event *> events;
  while(auto rd = reader.read())
  {
    for (auto &ev : rd)
    {
      events.push_back(&ev);
    }
  }

  auto time_range = opts.get_time_range(events);

  std::cout << "Dataset has " << events.size() << " events (";
  time_range.print(std::cout, events.front()->time, events.back()->time);
  std::cout << ")" << std::endl;

  events = time_range.clip(events);

  std::cout << "Processed dataset has " << events.size() << " events ( ";
  time_range.print(std::cout, events.front()->time, events.back()->time);
  std::cout << ")" << std::endl;

  auto &types = reader.types();
  AllocationManager allocations;
  std::cout << "Read " << events.size() << " events" << std::endl;
  for (auto &ev : events)
  {
    auto &&type = types.find_type(ev->type);

    if (type.name() == "Allocation")
    {
      allocations.malloc(
        type.get_field_value<std::uint64_t>(types, "pointer", ev->data.get()),
        type.get_field_value<std::uint64_t>(types, "size", ev->data.get()),
        ev
      );
    }
    else if (type.name() == "Deallocation")
    {
      allocations.free(
        type.get_field_value<std::uint64_t>(types, "pointer", ev->data.get()),
        ev
      );
    }
    else
    {
      std::cerr << "Unknown event: ";
      type.dump(std::cerr, types, ev->data.get());
    }
  }

  allocations.dump(std::cout, reader);
}

int main(int argc, char *argv[])
{
  Options opts;

  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "Display help message")
  ;

  try
  {
    po::variables_map vm;
    po::store(
      po::command_line_parser(argc, argv)
        .options(desc)
        .allow_unregistered()
        .run(),
      vm
    );
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << opts.description << std::endl;
      return EXIT_SUCCESS;
    }

    opts.run(argc, argv);
    run_inspector(opts);
    return EXIT_SUCCESS;
  }
  catch(...)
  {
    std::cerr << opts.description << std::endl;

    std::cerr << "Unhandled exception!" << std::endl <<
      boost::current_exception_diagnostic_information() << std::endl;

    return EXIT_FAILURE;
  }
}
