#include "appulatrix/AllocatableBlock.h"

#include <catch/catch.hpp>

const std::size_t header_size = 8;
template <typename Container> void test_for_used(appulatrix::AllocatableBlock &alloc, Container &data, std::size_t used)
{
  CHECK(alloc.allocatable_capacity() == data.size() - header_size);
  CHECK(alloc.allocated_size() == used);
  CHECK(alloc.available_size() == data.size() - header_size - used);
}

SCENARIO("AllocatableBlock", "[memory]")
{
	GIVEN("An empty initialised allocatable block")
	{
		std::array<char, 24> data = {};
		appulatrix::AllocatableBlock allocator(data);


		THEN("Testing for free space")
		{
			test_for_used(allocator, data, 0);
		}

		WHEN("Allocating new data")
		{
			const std::size_t to_allocate = 4;
			auto allocated = allocator.allocate(to_allocate);

			THEN("Testing for free space")
			{
				test_for_used(allocator, data, to_allocate);
			}

			allocator.clear();

			THEN("When cleared, the space is restored")
			{
				test_for_used(allocator, data, 0);
			}
		}

		WHEN("Linking to block from another allocator")
		{
			const std::size_t to_allocate = 4;
			auto allocated = allocator.allocate(to_allocate);
			test_for_used(allocator, data, to_allocate);

			appulatrix::AllocatableBlock allocator2(data, appulatrix::AllocatableBlock::InitialisationMode::UseExisting);
			test_for_used(allocator2, data, to_allocate);

			THEN("Allocating from second allocator")
			{
				allocator2.allocate(to_allocate);

				test_for_used(allocator, data, 2 * to_allocate);
				test_for_used(allocator2, data, 2 * to_allocate);

				allocator2.clear();

				test_for_used(allocator, data, 0);
				test_for_used(allocator2, data, 0);
			}
		}

		WHEN("Relinking allocator to a second block")
		{
			std::array<char, 16> data2 = {};

			auto allocated = allocator.allocate(8);
			test_for_used(allocator, data, 8);
			CHECK(allocated == data.data() + header_size);

			allocator.reset_location(data2);
			test_for_used(allocator, data2, 0);

			auto  allocated2 = allocator.allocate(8);
			test_for_used(allocator, data2, 8);
			CHECK(allocated2 == data2.data() + header_size);

			allocator.reset_location(data);
			test_for_used(allocator, data, 8);
		}

		WHEN("Allocating beyond capcity")
		{
			auto allocated1 = allocator.allocate(8);
			CHECK(allocated1 == data.data() + header_size);
			auto allocated2 = allocator.allocate(8);
			CHECK(allocated2 == data.data() + header_size + 8);

			CHECK_THROWS(auto allocated3 = allocator.allocate(8));

			allocator.clear();
			test_for_used(allocator, data, 0);

			CHECK_THROWS(auto allocated1 = allocator.allocate(32));
		}
	}
}
