#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventManager.h"
#include "appulatrix/StackManager.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/TypeManager.h"

#include <catch/catch.hpp>
#include <iostream>

struct FakeAllocationEvent
{
  FakeAllocationEvent(void *v)
  : value(v)
  {
  }
  
  void *value;
};

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, FakeAllocationEvent)
IMPLEMENT_TYPE_BUILDER(FakeAllocationEvent, type)
{
  type
		.add("value", &FakeAllocationEvent::value);
}

SCENARIO("Event Test", "[event]")
{
	std::array<char, 50000> data;
	appulatrix::AllocatableBlock allocator(data);
  appulatrix::TypeList type_list(&allocator);
  appulatrix::TypeManager type_manager;
  type_manager.set_current_type_list(type_list);
  appulatrix::StackList stack_list(&allocator);
  appulatrix::StackManager stack_manager;
  stack_manager.set_current_stack_list(stack_list);
  appulatrix::EventManager event_manager(&allocator);

  WHEN("Logging events")
  {
    auto x = std::make_shared<int>();
    CHECK(event_manager.events().size() == 0);
  
    auto begin = std::chrono::high_resolution_clock::now();
    auto stack = stack_manager.get_stack();
    FakeAllocationEvent *data_out = nullptr;
    appulatrix::EventProviderId provider_id = 0;
    event_manager.add_event<FakeAllocationEvent>(
        &data_out,
        type_manager.get_type<FakeAllocationEvent>().id(),
        stack,
        provider_id,
        x.get()
    );
    auto end = std::chrono::high_resolution_clock::now();
  
    CHECK(event_manager.events().size() == 1);
    auto &&event = event_manager.events()[0];
    CHECK(begin <= event.time);
    CHECK(event.time <= end);
    CHECK(event.type == appulatrix::Type::find_index<FakeAllocationEvent>(type_manager));
  
    auto data = reinterpret_cast<const FakeAllocationEvent *>(event.data.get());
    CHECK(data->value == x.get());
  }
}
