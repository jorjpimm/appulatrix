#include "appulatrix/List.h"
#include "appulatrix/AllocatableBlock.h"

#include <catch/catch.hpp>

#include <array>
#include <thread>

struct Thing {
  Thing(int i)
  : value(i)
  {
  }

  int value;

  bool operator==(const Thing &t) const
  {
  return value == t.value;
  }
};

SCENARIO("LinkedList", "[container]")
{
  std::array<char, 20000> data;
  appulatrix::AllocatableBlock allocator(data);
  appulatrix::List<Thing> list(&allocator);

  GIVEN("an empty list")
  {
    CHECK(list.size() == 0);
    CHECK(list.begin() == list.end());
  }

  WHEN("adding an item to list")
  {
    list.emplace_back(1);

    THEN("accessors return correct values")
    {
      CHECK(list.size() == 1);
      CHECK(list[0] == Thing(1));
      CHECK(list.begin() != list.end());

      auto it = list.begin();
      CHECK(*it == Thing(1));
      ++it;
      CHECK(it == list.end());
    }
  }

  WHEN("adding many items to list")
  {
    CHECK(allocator.allocated_size() == sizeof(decltype(list)::LinkNode));

    for (std::size_t i = 0; i < 513; ++i)
    {
      CHECK_NOTHROW(list.emplace_back(int(i)));
      auto expected_value = Thing(int(i));
      auto count = (i / 128) + 1;
      CHECK(allocator.allocated_size() == count * sizeof(decltype(list)::LinkNode));

      CHECK(list.size() == i + 1);
      CHECK(list[i] == expected_value);
      CHECK(list.begin() != list.end());

      auto it = list.begin();
      for (std::size_t j = 0; j < i; ++j)
      {
        CHECK_NOTHROW(++it);
				CHECK(it != list.end());
      }
      CHECK(*it == expected_value);
      CHECK_NOTHROW(++it);
      CHECK(it == list.end());
    }
  }
}

SCENARIO("LinkedList Threaded", "[container]")
{
  WHEN("adding many items to list from threads")
  {
    auto list_test = [&](std::size_t thread_count) {
      const auto sizeof_link_node = sizeof(appulatrix::List<Thing>::LinkNode);
      const auto max_block_count = 5;
      
      struct Header
      {
        Header(gsl::span<char> allocator_data)
        : allocator(allocator_data)
        , list(&allocator)
        {
        }
        
        appulatrix::AllocatableBlock allocator;
        appulatrix::List<Thing> list;
      };
      
      auto header_size = sizeof(Header);
      std::vector<char> data(
        header_size +
        max_block_count * thread_count * sizeof_link_node);
      
      auto header = new(data.data()) Header(
        gsl::span<char>(
          data.data() + header_size,
          data.size() - header_size
        )
      );
      auto finally = gsl::finally([&] { header->~Header(); });
      
      appulatrix::AllocatableBlock &allocator = header->allocator;
      appulatrix::List<Thing> &list = header->list;

      std::atomic<bool> exit(false);
      std::atomic<std::size_t> emplace_count(0);

      std::atomic<std::size_t> execution_count(0);
      std::atomic<std::size_t> working_count(0);
      std::vector<std::thread> workers;
      for (std::size_t i = 0; i < thread_count; ++i)
      {
        workers.emplace_back([&]() {
          while (!exit)
          {
            std::size_t val = emplace_count;
            if (val == 0)
            {
              std::this_thread::sleep_for(std::chrono::milliseconds(1));
              continue;
            }

            ++working_count;
            auto finally = gsl::finally([&]() { assert(working_count > 0); --working_count; });
            if (!emplace_count.compare_exchange_strong(val, val - 1))
            {
              continue;
            }

            assert(working_count > 0);
            list.emplace_back(int(val));
            ++execution_count;
          }
        });
      }

      auto wait_for_complete = [&]() {
        while (emplace_count != 0 || working_count != 0)
        {
          std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
      };

      CHECK(allocator.allocated_size() == sizeof_link_node);
      CHECK(list.size() == 0);
      emplace_count = 10;
      wait_for_complete();
      CHECK(list.size() == 10);
      CHECK(execution_count == list.size());

      emplace_count = 118;
      wait_for_complete();
      CHECK(list.size() == 128);
      CHECK(execution_count == list.size());
      CHECK(allocator.allocated_size() == sizeof_link_node);

      emplace_count = 10;
      wait_for_complete();
      CHECK(list.size() == 138);
      CHECK(execution_count == list.size());
      CHECK(allocator.allocated_size() <= 2 * thread_count * sizeof_link_node);

      emplace_count = 375;
      wait_for_complete();
      CHECK(list.size() == 513);
      CHECK(execution_count == list.size());
      CHECK(allocator.allocated_size() <= 5 * thread_count * sizeof_link_node);

      auto check_contents = [&](int start, int count)
      {
        for (int i = 1; i < (count+1); ++i)
        {
          bool found = false;
          for (std::size_t j = start; j < (start+count); ++j)
          {
            if (list[j] == Thing(i))
            {
              found = true;
              break;
            }
          }

          CHECK(found);
        }
      };
      check_contents(0, 10);
      check_contents(10, 118);
      check_contents(128, 10);
      check_contents(138, 375);

      exit = true;
      for (auto &w : workers)
      {
        w.join();
      }
    };

    list_test(2);
    list_test(4);
    list_test(8);
    list_test(512);
  }
}
