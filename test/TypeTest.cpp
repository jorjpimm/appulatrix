#include "appulatrix/AllocatableBlock.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventProvider.h"
#include "appulatrix/Type.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/TypeManager.h"

#include <array>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
#include <catch/catch.hpp>

struct EmbeddedType
{
  int a;
  float b;
};

struct TestType
{
  EmbeddedType c[5];
  std::array<int, 4> d;
  float e;
};

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, EmbeddedType)
IMPLEMENT_TYPE_BUILDER(EmbeddedType, type)
{
	type
		.add("a", &EmbeddedType::a)
		.add("b", &EmbeddedType::b);
}

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestType)
IMPLEMENT_TYPE_BUILDER(TestType, type)
{
	type
		.add("c", &TestType::c)
		.add("d", &TestType::d)
		.add("e", &TestType::e);
}

SCENARIO("Type Manager Test", "[type]")
{
  GIVEN("A manager and type list")
  {
    std::array<char, 20000> data;
    appulatrix::AllocatableBlock allocator(data);
    appulatrix::TypeList type_list(&allocator);
    appulatrix::TypeManager manager;
    manager.set_current_type_list(type_list);

    WHEN("Querying embedded type")
    {
      CHECK(manager.type_list().size() == 0);
      appulatrix::Type::find_index<EmbeddedType>(manager);
      CHECK(manager.type_list().size() == 3);
    }

    WHEN("Querying outer type")
    {
      CHECK(manager.type_list().size() == 0);
      appulatrix::Type::find_index<TestType>(manager);
      CHECK(manager.type_list().size() == 4);
    }
    
    WHEN("type is registered")
    {
      const auto expected_type_id = appulatrix::TypeId(appulatrix::BuiltInTypes::Indices::EventProvider);
      auto &&provider_type = manager.get_type<appulatrix::EventProviderData>();
      CHECK(provider_type.id() == expected_type_id);
      
      THEN("Type lookup displays type")
      {
        appulatrix::TypeLookup &lookup = manager;
        REQUIRE(lookup.type_list().size() == 3);
        CHECK(lookup.type_list()[2]->id() == expected_type_id);
        CHECK(&lookup.find_type(expected_type_id) == lookup.type_list()[2]);
      }
    }
  }
}

SCENARIO("Type Test", "[type]")
{
	std::array<char, 20000> data;
	appulatrix::AllocatableBlock allocator(data);
  appulatrix::TypeList type_list(&allocator);
  appulatrix::TypeManager manager;
  manager.set_current_type_list(type_list);

  WHEN("Using built in types")
  {
    CHECK(manager.get_type<char>().data_type() ==
      appulatrix::Type::DataType::Integer);
    CHECK(manager.get_type<std::int8_t>().data_type() ==
      appulatrix::Type::DataType::Integer);
    CHECK(manager.get_type<int>().data_type() ==
      appulatrix::Type::DataType::Integer);
    CHECK(manager.get_type<float>().data_type() ==
      appulatrix::Type::DataType::Real);
    CHECK(manager.get_type<bool>().data_type() ==
      appulatrix::Type::DataType::Boolean);
  }
  

	auto &&sub_built = manager.get_type<EmbeddedType>();
	auto &&main_built = manager.get_type<TestType>();

  WHEN("Describing a type")
  {
    CHECK(sub_built.name() == "EmbeddedType");
    CHECK(main_built.name() == "TestType");
    CHECK(sub_built.data_type() == appulatrix::Type::DataType::Unknown);
    CHECK(main_built.data_type() == appulatrix::Type::DataType::Unknown);

    CHECK(sub_built.size() == sizeof(EmbeddedType));
    CHECK(main_built.size() == sizeof(TestType));

    CHECK(sub_built.fields().size() == 2);
    CHECK(sub_built.fields()[0].name() == "a");
    CHECK(sub_built.fields()[0].type() == manager.get_type<int>().id());
    CHECK(sub_built.fields()[0].offset() == 0);
    CHECK(sub_built.fields()[0].parent() == appulatrix::Type::Field::null_parent());
    CHECK(sub_built.fields()[1].name() == "b");
    CHECK(sub_built.fields()[1].type() == manager.get_type<float>().id());
    CHECK(sub_built.fields()[1].offset() == 4);
    CHECK(sub_built.fields()[1].parent() == appulatrix::Type::Field::null_parent());

    CHECK(main_built.fields().size() == 12);
    std::size_t index = 0;
    CHECK(main_built.fields()[index].name() == "c");
    CHECK(main_built.fields()[index].type() == appulatrix::Type::invalid_index());
    CHECK(main_built.fields()[index].offset() == 0);
    CHECK(main_built.fields()[index].parent() == appulatrix::Type::Field::null_parent());

    for (std::size_t i = 0; i < 5; ++i)
    {
      ++index;
      CHECK(main_built.fields()[index].name() == std::to_string(i));
      CHECK(main_built.fields()[index].type() == manager.get_type<EmbeddedType>().id());
      CHECK(main_built.fields()[index].offset() == i * 8);
      CHECK(main_built.fields()[index].parent() == 0);
    }

    ++index;
    CHECK(main_built.fields()[index].name() == "d");
    CHECK(main_built.fields()[index].type() == appulatrix::Type::invalid_index());
    CHECK(main_built.fields()[index].offset() == 40);
    CHECK(main_built.fields()[index].parent() == appulatrix::Type::Field::null_parent());

    for (std::size_t i = 0; i < 4; ++i)
    {
      ++index;
      CHECK(main_built.fields()[index].name() == std::to_string(i));
      CHECK(main_built.fields()[index].type() == manager.get_type<int>().id());
      CHECK(main_built.fields()[index].offset() == 40 + i * 4);
      CHECK(main_built.fields()[index].parent() == 6);
    }
  
    ++index;
    CHECK(main_built.fields()[index].name() == "e");
    CHECK(main_built.fields()[index].type() == manager.get_type<float>().id());
    CHECK(main_built.fields()[index].offset() == 56);
    CHECK(main_built.fields()[index].parent() == appulatrix::Type::Field::null_parent());
  }


	WHEN("Accessing a type")
	{
		TestType data;
		data.e = 5.0f;
		
		auto field = main_built.find_field("e");
		REQUIRE(!!field);
		auto field_data = field->get_data_ptr(&data);
		auto value = manager.find_type(field->type()).get_value(field_data);
		REQUIRE(value);
		REQUIRE(*value == appulatrix::Type::DataVariant(5.0f));
    
    value = main_built.get_field_value(manager, "e", &data);
    REQUIRE(*value == appulatrix::Type::DataVariant(5.0f));
	}
}
