#include "appulatrix/EventProvider.h"
#include "appulatrix/Logger.h"
#include "appulatrix/Reader.h"

#include <boost/filesystem.hpp>
#include <catch/catch.hpp>

struct Test { };

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, Test)
IMPLEMENT_TYPE_BUILDER(Test, type)
{
}

SCENARIO("Simple Event Provider Test", "[event]")
{
  boost::filesystem::path temp = boost::filesystem::unique_path();
  
  GIVEN("A logger reader pair")
  {
    appulatrix::Logger logger(temp);
    appulatrix::Reader reader(temp);
    appulatrix::EventProvider::initialise_providers(&logger);
    auto finally = gsl::finally([] {
      appulatrix::EventProvider::terminate_providers();
    });

    WHEN("Opening an event provider")
    {
      auto basic_provider = appulatrix::EventProvider::create_top_level("test_top");
      
      auto read1 = reader.read();
      THEN("provider logs creation event")
      {
        REQUIRE(read1.size() == 2);
        
        {
          auto type = read1[0].type;
          auto &&reader_type = reader.types().find_type(type);
          CHECK((appulatrix::BuiltInTypes::Indices)reader_type.id() ==
            appulatrix::BuiltInTypes::Indices::EventProvider);
          CHECK(reader_type.name() == "appulatrix::EventProviderData");
          
          auto data = (appulatrix::EventProviderData*)read1[0].data.get();
          CHECK(data->id() == 0);
          CHECK(data->parent() == std::numeric_limits<appulatrix::EventProviderId>::max());
          CHECK(data->name() == "root");
        }

        {
          auto type = read1[1].type;
          auto &&reader_type = reader.types().find_type(type);
          CHECK((appulatrix::BuiltInTypes::Indices)reader_type.id() ==
            appulatrix::BuiltInTypes::Indices::EventProvider);
          CHECK(reader_type.name() == "appulatrix::EventProviderData");
        
          auto data = (appulatrix::EventProviderData*)read1[1].data.get();
          CHECK(data->id() == 1);
          CHECK(data->parent() == 0);
          CHECK(data->name() == "test_top");
        }
      }
      
      auto child_provider = basic_provider.create_child("test_child");
      
      auto read2 = reader.read();
      THEN("provider logs child creation event")
      {
        REQUIRE(read2.size() == 1);
        
        {
          auto type = read2[0].type;
          auto &&reader_type = reader.types().find_type(type);
          CHECK((appulatrix::BuiltInTypes::Indices)reader_type.id() ==
            appulatrix::BuiltInTypes::Indices::EventProvider);
          CHECK(reader_type.name() == "appulatrix::EventProviderData");
          
          auto data = (appulatrix::EventProviderData*)read2[0].data.get();
          CHECK(data->id() == 2);
          CHECK(data->parent() == 1);
          CHECK(data->name() == "test_child");
        }
      }
    }
  
    WHEN("Writing events to a provider")
    {
      auto basic_provider = appulatrix::EventProvider::create_top_level("test_top");
      
      THEN("provider is enabled by disabled")
      {
        CHECK(basic_provider.is_enabled());
      }
      
      WHEN("Logging events when enabled")
      {
        basic_provider.emit_event<Test>();
        
        THEN("Event is logged")
        {
          auto read = reader.read();
          REQUIRE(read.size() == 3);
          auto type = read[2].type;
          auto &&reader_type = reader.types().find_type(type);
          CHECK(reader_type.name() == "Test");
        }
      }
      
      WHEN("Logging events when disabled")
      {
        basic_provider.set_enabled(false);
        basic_provider.emit_event<Test>();
        
        THEN("Event is not logged")
        {
          auto read = reader.read();
          REQUIRE(read.size() == 2);
        }
      }
    }
  }
}
