import json
import logging
import os
import platform
import subprocess
import sys
import unittest
import uuid

PROJECT_BUILD_ROOT = sys.argv[1]
BINARY_ROOT = os.path.join(PROJECT_BUILD_ROOT, "bin")
PROJECT_ROOT = os.path.join(PROJECT_BUILD_ROOT, "..")
NODE_LIB_ROOT = os.path.join(PROJECT_ROOT, "tools", "node-lib")
RUN_WITH_APPULATRIX = "{}/tools/run_with_appulatrix/run.py".format(PROJECT_ROOT)
RUN_IN_SHELL = False
EXE_SUFFIX = ""
if platform.system() == "Windows":
    EXE_SUFFIX = ".exe"
    RUN_IN_SHELL = True

def get_generated_stream(path):
  files = os.listdir(path)
  if len(files) != 1:
    raise Exception("Invalid directory for reading {}".fomat(path))
  return os.path.abspath(os.path.join(path, files[0]))

def execute_process(args, env=None):
  print("Running process {}".format(args))

  return subprocess.call(
    args,
    shell = RUN_IN_SHELL,
    env=env
  )

class Config:
  def __init__(self, path):
    self.__dict__['data'] = { }
    self.__dict__['path'] = path

  def __setattr__(self, name, value):
    self.__dict__['data'][name] = value

  def __getattr__(self, name):
    return self.__dict__['data'][name]

  def write(self):
    path = os.path.abspath(self.__dict__['path'])
    config = os.path.join(path, 'appulatrix.config')
    with open(config, 'w') as file:
      data = json.dumps(self.__dict__['data'], indent=2)
      file.write(data)

    if not os.path.exists(path):
      raise Exception("Failed to create config")


class TestAppulatrix(unittest.TestCase):

    def test_custom_events(self):
      c = Config('./')
      c.storage_path = 'test_data_{}'.format(uuid.uuid4())
      c.log_allocations = False
      c.write()

      trace_res = execute_process(
        [
          RUN_WITH_APPULATRIX,
          "{}/bin/custom_events{}".format(PROJECT_BUILD_ROOT, EXE_SUFFIX)
        ],
        env=dict(os.environ, APPULATRIX_PATH=BINARY_ROOT)
      )
      self.assertEqual(0, trace_res)

      location = get_generated_stream(c.storage_path)
      print("Data generated at {}".format(location))
      
      node_res = execute_process(
        [
          "mocha",
          "{}/test_custom_events.js".format(NODE_LIB_ROOT),
          location
        ]
      )
      self.assertEqual(0, node_res)


    def test_allocation_injector(self):
      c = Config('./')
      c.storage_path = 'test_data_{}'.format(uuid.uuid4())
      c.log_allocations = True
      c.write()

      trace_res = execute_process(
        [
          RUN_WITH_APPULATRIX,
          "{}/bin/allocator{}".format(PROJECT_BUILD_ROOT, EXE_SUFFIX)
        ],
        env=dict(os.environ, APPULATRIX_PATH=BINARY_ROOT)
      )
      self.assertEqual(0, trace_res)

      location = get_generated_stream(c.storage_path)
      print("Data generated at {}".format(location))
      
      node_res = execute_process(
        [
          "mocha",
          "{}/test.js".format(NODE_LIB_ROOT),
          location
        ]
      )
      self.assertEqual(0, node_res)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit("ERROR: A command-line parameter must be supplied for the binary directory")
    del sys.argv[1:]

    unittest.main()
