#include "appulatrix/Stack.h"

#include <catch/catch.hpp>
#include <iostream>

void blueberry_pie(appulatrix::Stack &stk)
{
  stk = appulatrix::Stack::current_stack_thread(0);
  
  {
    auto entries = stk.format().entries();
    CHECK(entries.size() >= 3);
  }
  
  {
    auto detailed_stack = appulatrix::DetailedStack::current_stack_thread(0);
    auto stack_str = detailed_stack.to_string();
    CHECK(stack_str.size() >= 3);
    
    auto entries = detailed_stack.entries();
    CHECK(entries.size() >= 3);
#ifndef NDEBUG
    CHECK(entries[0].symbol_name->find("blueberry_pie") != std::string::npos);
    CHECK(entries[1].symbol_name->find("humble_pie") != std::string::npos);
    CHECK(entries[2].symbol_name->find("pork_pie") != std::string::npos);
#endif
  }
}

void humble_pie(appulatrix::Stack &stk)
{
  blueberry_pie(stk);
}

void pork_pie(appulatrix::Stack &stk)
{
  humble_pie(stk);
}

SCENARIO("Stack Test", "[stack]")
{
  GIVEN("current thread stack")
  {
    appulatrix::Stack stack;
    pork_pie(stack);
  }
}
