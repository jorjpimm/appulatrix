#include "appulatrix/Logger.h"
#include "appulatrix/detail/LoggerDetails.h"
#include "appulatrix/Reader.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventManager.h"
#include "appulatrix/TypeBuilder.h"

#include <array>
#include <boost/optional/optional_io.hpp>
#include <boost/filesystem.hpp>
#include <catch/catch.hpp>
#include <iostream>

struct LoggerEventTest
{
  LoggerEventTest(int val)
  : value(val)
  {
  }
  
  bool operator==(const LoggerEventTest &ev) const
  {
    return value == ev.value;
  }

  int value;
};

struct LoggerEventTest2
{
  LoggerEventTest2(int val)
    : value(val)
  {
  }

  bool operator==(const LoggerEventTest &ev) const
  {
    return value == ev.value;
  }

  int value;
};
struct LoggerEventTest3
{
  LoggerEventTest3(int val)
    : value(val)
  {
  }

  bool operator==(const LoggerEventTest &ev) const
  {
    return value == ev.value;
  }

  int value;
};

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, LoggerEventTest)
IMPLEMENT_TYPE_BUILDER(LoggerEventTest, type)
{
}

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, LoggerEventTest2)
IMPLEMENT_TYPE_BUILDER(LoggerEventTest2, type)
{
}

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, LoggerEventTest3)
IMPLEMENT_TYPE_BUILDER(LoggerEventTest3, type)
{
}

SCENARIO("Logger Test", "[logger]")
{
  boost::filesystem::path temp = boost::filesystem::unique_path();

  WHEN("Reading a closed logger")
  {
    {
      appulatrix::Logger logger(temp);
      appulatrix::StackId stack = logger.stack_manager().get_stack(0);
      appulatrix::EventProviderId provider = 0;
      LoggerEventTest *data_out = nullptr;

      auto type = logger.type_manager().get_type<LoggerEventTest>().id();
      logger.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider, 1);
      logger.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider, 3);
      logger.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider, 6);
    
      appulatrix::Reader reader(temp);
      CHECK(reader.read().size() == 3);
    }

    {
      appulatrix::Reader logger(temp);

      auto events = logger.read();
      CHECK(events.size() == 3);
      CHECK(logger.read().size() == 0);

      auto &&val1 = events[0];
      auto user1 = static_cast<const LoggerEventTest *>(val1.data.get());
      CHECK(user1->value == 1);

      auto &&val2 = events[1];
      auto user2 = static_cast<const LoggerEventTest *>(val2.data.get());
      CHECK(user2->value == 3);

      auto &&val3 = events[2];
      auto user3 = static_cast<const LoggerEventTest *>(val3.data.get());
      CHECK(user3->value == 6);
    }
  }

  WHEN("Reading an open logger")
  {
    appulatrix::Logger logger_write(temp);

    appulatrix::Reader logger_read(temp);
    auto read = logger_read.read();
    CHECK(read.empty());

    LoggerEventTest *data_out = nullptr;
    appulatrix::StackId stack = logger_write.stack_manager().get_stack(0);
    appulatrix::EventProviderId provider = 0;
    auto type = logger_write.type_manager().get_type<LoggerEventTest>().id();
    logger_write.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider, 1);
    read = logger_read.read();
    CHECK(read.size() == 1);
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(1));

    for (std::size_t i = 0; i < 128; ++i)
    {
      logger_write.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider, int(i));
    }
    read = logger_read.read();
    REQUIRE(read.size() == 127);
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(0));

    read = logger_read.read();
    REQUIRE(read.size() == 1);
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(127));
  }


  WHEN("Reading an open logger which expands")
  {
    appulatrix::Logger logger_write(temp);

    appulatrix::Reader logger_read(temp);
    auto read = logger_read.read();
    CHECK(read.empty());

    LoggerEventTest *data_out = nullptr;
    appulatrix::StackId stack = logger_write.stack_manager().get_stack(0);
    appulatrix::EventProviderId provider = 0;
    auto type = logger_write.type_manager().get_type<LoggerEventTest>().id();
    logger_write.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider, 1);
    read = logger_read.read();
    CHECK(read.size() == 1);
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(1));

    // Move the logger onto the next file
    logger_write.next_active_file();

    for (std::size_t i = 0; i < 129; ++i)
    {
      logger_write.active_event_manager().add_event<LoggerEventTest>(&data_out, type, stack, provider,  int(i));
    }
    read = logger_read.read();
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(0));
    REQUIRE(read.size() == 128);

    read = logger_read.read();
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(128));
    REQUIRE(read.size() == 1);
  }

  WHEN("Reading an open logger which expands")
  {
    appulatrix::Logger logger_write(temp);

    appulatrix::Reader logger_read(temp);
    auto read = logger_read.read();
    CHECK(read.empty());

    LoggerEventTest *data_out = nullptr;
    appulatrix::StackId stack = logger_write.stack_manager().get_stack(0);
    appulatrix::EventProviderId provider = 0;
    auto type_1 = logger_write.type_manager().get_type<LoggerEventTest>().id();
    auto type_2 = logger_write.type_manager().get_type<LoggerEventTest2>().id();
    auto type_3 = logger_write.type_manager().get_type<LoggerEventTest3>().id();
    logger_write.active_event_manager().add_event<LoggerEventTest>(&data_out, type_1, stack, provider, 1);
    read = logger_read.read();
    CHECK(read.size() == 1);
    CHECK(*static_cast<const LoggerEventTest *>(read.begin()->data.get()) == LoggerEventTest(1));

    // Move the logger onto the next file
    logger_write.next_active_file();

    for (std::size_t i = 0; i < 129; ++i)
    {
      logger_write.active_event_manager().add_event<LoggerEventTest>(&data_out, type_1, stack, provider, int(i));
    }

    // Move the logger onto the next file
    logger_write.next_active_file();

    for (std::size_t i = 0; i < 129; ++i)
    {
      LoggerEventTest2 *data_out2 = nullptr;
      logger_write.active_event_manager().add_event<LoggerEventTest2>(&data_out2, type_2, stack, provider, int(i));
    }

    // Move the logger onto the next file
    logger_write.next_active_file();

    for (std::size_t i = 0; i < 129; ++i)
    {
      LoggerEventTest3 *data_out3 = nullptr;
      logger_write.active_event_manager().add_event<LoggerEventTest3>(&data_out3, type_3, stack, provider, int(i));
    }
    
    const auto first_user = appulatrix::TypeId(appulatrix::BuiltInTypes::Indices::FirstUserType);
    const auto second_user = first_user + 1;
    const auto third_user = first_user + 2;

    read = logger_read.read();
    CHECK(read.begin()->type == first_user);
    CHECK(logger_read.types().find_type(read.begin()->type).name() == "LoggerEventTest");
    auto &&stack_0 = logger_read.get_stack(read[0].stack);
    auto &&stack_1 = logger_read.get_stack(read[1].stack);
    CHECK(strlen(stack_0.stack_string.get()) > 0);
    CHECK(strlen(stack_1.stack_string.get()) > 0);
    REQUIRE(read.size() == 128);

    read = logger_read.read();
    CHECK(read.begin()->type == first_user);
    REQUIRE(read.size() == 1);

    read = logger_read.read();
    CHECK(read.begin()->type == second_user);
    CHECK(logger_read.types().find_type(read.begin()->type).name() == "LoggerEventTest2");
    REQUIRE(read.size() == 128);

    read = logger_read.read();
    CHECK(read.begin()->type == second_user);
    REQUIRE(read.size() == 1);

    read = logger_read.read();
    CHECK(read.begin()->type == third_user);
    CHECK(logger_read.types().find_type(read.begin()->type).name() == "LoggerEventTest3");
    REQUIRE(read.size() == 128);

    read = logger_read.read();
    CHECK(read.begin()->type == third_user);
    REQUIRE(read.size() == 1);
  }
}

SCENARIO("Logger Detail Test", "[logger]")
{
  GIVEN("a header object")
  {
    gsl::span<char> empty;
    appulatrix::detail::Header hdr(empty);
  
    WHEN("default constructed")
    {
      THEN("active file is at 0")
      {
        CHECK(hdr.active_file() == 0);
      }
    }
    
    WHEN("next_active_file is called constructed")
    {
      for (std::size_t i = 1; i < 10; ++i)
      {
        hdr.next_active_file();
      
        THEN("Active file increases")
        {
          CHECK(hdr.active_file() == i);
        }
      }
    }
  }
}
