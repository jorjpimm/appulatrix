#include "appulatrix/Logger.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventProvider.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/TypeManager.h"

#include <boost/filesystem.hpp>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>

struct TestAllocation
{
  TestAllocation(const void *val, std::uint64_t s)
  : value(val)
  , size(s)
  {
  }

  const void *value;
  std::uint64_t size;
};

struct TestDeallocation
{
  TestDeallocation(const void *val)
  : value(val)
  {
  }

  const void *value;
};

#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestAllocation)
IMPLEMENT_TYPE_BUILDER(TestAllocation, type)
{
  type
    .add("value", &TestAllocation::value)
    .add("size", &TestAllocation::size);
}


#define EXPORT_DUMMY
DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestDeallocation)
IMPLEMENT_TYPE_BUILDER(TestDeallocation, type)
{
  type
    .add("value", &TestDeallocation::value);
}

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Please specify a log file name" << std::endl;
    return EXIT_FAILURE;
  }
  boost::filesystem::path temp = boost::filesystem::absolute(argv[1]);
  std::cout << "Logging to " << temp << std::endl;

  appulatrix::Logger logger(temp);
  appulatrix::EventProvider provider = appulatrix::EventProvider::create_top_level("test");

  std::mt19937 generator;
  std::uniform_int_distribution<int> distribution(0, 9);

  std::vector<std::shared_ptr<int>> allocations;
  while (true)
  {
    std::this_thread::sleep_for(std::chrono::seconds(2));
    if (distribution(generator) > 5)
    {
      auto i = std::make_shared<int>();
      allocations.push_back(i);
      provider.emit_event<TestAllocation>(provider.get_stack(0), i.get(), sizeof(*i));
    }
    else if (allocations.size())
    {
      auto i = allocations.back();
      allocations.pop_back();
      provider.emit_event<TestDeallocation>(provider.get_stack(0), i.get());
    }
  }

  return EXIT_SUCCESS;
}
