#include <array>
#include <chrono>
#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>
#include <thread>
#include <vector>

#include "appulatrix/EventProvider.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/TypeManager.h"

#include "AppLogger.h"

#define EXPORT_DUMMY

class TestA
{
public:
  TestA()
  {
  }
};

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestA)
IMPLEMENT_TYPE_BUILDER(TestA, type)
{
}

int main(int argc, char *argv[])
{
  std::cout << "main beginning" << &AppLogger::instance() << std::endl;

  auto provider = appulatrix::EventProvider::create_top_level("permanent_emitter");

  std::mutex output_mutex;

  auto fn = [&]()
  {
    std::ostringstream str;
    str << "thread_" << std::this_thread::get_id();
    auto thr_provider = provider.create_child(str.str().c_str());

    {
      std::lock_guard<std::mutex> l(output_mutex);
      std::cout << "starting thread " << str.str() << std::endl;
    }
    
    while(true)
    {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      appulatrix::emit_event<TestA>(thr_provider);

      std::this_thread::sleep_for(std::chrono::seconds(1));
      appulatrix::emit_event<TestA>(thr_provider);
      std::this_thread::sleep_for(std::chrono::seconds(1));

      std::lock_guard<std::mutex> l(output_mutex);
      std::cout << "blorp" << std::endl;
    }
  };

  {
    std::lock_guard<std::mutex> l(output_mutex);
    std::cout << "starting threads" << std::endl;
  }
  std::vector<std::thread> threads;
  for (std::size_t i = 0; i < 8; ++i)
  {
    threads.emplace_back(fn);
  }
  
  threads.front().join();

  return EXIT_SUCCESS;
}
