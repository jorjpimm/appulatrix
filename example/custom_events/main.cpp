#include <array>
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

#include "appulatrix/EventProvider.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/TypeManager.h"

#define EXPORT_DUMMY

class TestA
{
public:
  TestA()
  {
  }
};

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestA)
IMPLEMENT_TYPE_BUILDER(TestA, type)
{
}

class TestB
{
public:
  TestB()
  {
  }
};

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestB)
IMPLEMENT_TYPE_BUILDER(TestB, type)
{
}

class TestC
{
public:
  TestC()
  {
  }
};

DECLARE_TYPE_BUILDER(EXPORT_DUMMY, TestC)
IMPLEMENT_TYPE_BUILDER(TestC, type)
{
}

int main(int argc, char *argv[])
{
  auto provider = appulatrix::EventProvider::create_top_level("test_emitter_1");

  auto b = appulatrix::emit_scoped_event<TestA>(provider);

  appulatrix::emit_event<TestA>(provider);
  appulatrix::emit_event<TestB>(provider);
  appulatrix::emit_event<TestC>(provider);

  return EXIT_SUCCESS;
}
