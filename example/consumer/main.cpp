#include "appulatrix/Logger.h"
#include "appulatrix/Reader.h"
#include "appulatrix/BuiltInTypes.h"
#include "appulatrix/EventManager.h"
#include "appulatrix/TypeBuilder.h"
#include "appulatrix/TypeManager.h"

#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
#include <chrono>
#include <iostream>
#include <thread>
#include <unordered_map>

class AllocationState
{
public:
  using Pointer = std::uint64_t;
  void allocate(Pointer ptr, std::size_t size)
  {
    allocations.emplace(std::make_pair(ptr, Allocation{ ptr, size }));
    byte_count += size;
  }

  void release(Pointer ptr)
  {
    auto val = allocations.find(ptr);
    if (val != allocations.end())
    {
      byte_count -= val->second.size;
      allocations.erase(ptr);
    }
  }

  void dump(std::ostream &str)
  {
    str << "Currently have " << byte_count << " bytes allocated" << std::endl;
  }

private:
  std::size_t byte_count = 0;

  struct Allocation
  {
    Pointer pointer;
    std::size_t size;
  };
  std::unordered_map<Pointer, Allocation> allocations;
};

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Please specify a log file name" << std::endl;
    return EXIT_FAILURE;
  }
  boost::filesystem::path temp = boost::filesystem::absolute(argv[1]);
  std::cout << "Reading from " << temp << std::endl;

  appulatrix::Logger logger(temp);
  AllocationState state;
  
  appulatrix::Reader reader(temp);
  while (true)
  {
    auto read = reader.read();
    if (read.empty())
    {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      continue;
    }
    std::cout << "Processing " << read.size() << " events" << std::endl;
    for (const auto &ev : read)
    {
      auto &type = reader.types().find_type(ev.type);
      if (type.name() == "TestAllocation")
      {
        state.allocate(
          boost::get<AllocationState::Pointer>(*type.get_field_value(reader.types(), "value", ev.data.get())),
          boost::get<std::uint64_t>(*type.get_field_value(reader.types(), "size", ev.data.get()))
        );
      }
      else if (type.name() == "TestDeallocation")
      {
        state.release(
          boost::get<AllocationState::Pointer>(*type.get_field_value(reader.types(), "value", ev.data.get()))
        );
      }
    }

    state.dump(std::cout);
  }

  return EXIT_SUCCESS;
}
