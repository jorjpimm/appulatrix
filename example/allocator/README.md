
Allocator Example
=================

Simple example does some allocations, and then exits.

Running
-------

This example does not depend on appulatrix itself, it needs to be run with the injector.

From the root appulatrix directory run (with built appulatrix at build/):
```
> APPULATRIX_PATH=build/bin
> tools/run_with_appulatrix/run.py ${APPULATRIX_PATH}/allocator
```