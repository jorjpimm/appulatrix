#include <array>
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

#include "AppLogger.h"

std::vector<float> g_data(4000);

void leaker()
{
  auto leak = new char[42];
  (void)leak;
}

void complex()
{
  std::this_thread::sleep_for(std::chrono::milliseconds(2));

  auto c = std::make_shared<std::array<char, 123>>();

  leaker();
}

int main(int argc, char *argv[])
{
  std::size_t count = 100;
  std::cout << "main beginning" << &AppLogger::instance() << std::endl;
  for (std::size_t i = 0; i < count; ++i)
  {
    auto a = std::make_shared<std::array<char, 56>>();

    std::this_thread::sleep_for(std::chrono::milliseconds(2));

    auto b = std::make_shared<std::array<char, 84>>();

    complex();

    std::this_thread::sleep_for(std::chrono::milliseconds(2));
  }
  std::cout << "Finished" << std::endl;
  return EXIT_SUCCESS;
}
