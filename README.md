
https://jorjpimm.gitlab.io/appulatrix/index.html

Configuring on Windows
----------------------

```
# Configuring for MSVC 2017 (with 2014 built libraries.)
$ cmake \
  -DBOOST_INCLUDEDIR=c:/local/boost_1_61_0 \
  -DBOOST_LIBRARYDIR=c:/local/boost_1_61_0/lib64-msvc-14.0/ \
  -DQt5Core_DIR=C:/Qt/5.7/msvc2015_64/lib/cmake/Qt5Core \ -DQt5Widgets_DIR=C:/Qt/5.7/msvc2015_64/lib/cmake/Qt5Widgets \
  -DBoost_COMPILER="-vc140" \
  -G "Visual Studio 15 2017 Win64" ..
```
