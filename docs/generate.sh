#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../

docker build -t doxygen_build -f Dockerfile.doxygen .

docker run --rm -it -v ${DIR}:/src/ doxygen_build /src/docs/build_doxygen.sh